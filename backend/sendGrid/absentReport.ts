import sgMail from '@sendgrid/mail';
import Knex from "knex";
import dotenv from 'dotenv';
import moment from 'moment';
import 'moment-timezone';

dotenv.config();

const knexConfig = require("../knexfile");
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);


function minusDays(days: number): string {
    let today = new Date();
    today.setDate(today.getDate() - days);
    const localToday = moment.tz(today, "Hongkong").format()
    return localToday;
}


const weekAttendance = (date: string) =>
    new Promise(async (resolve) => {
        try {
            const results = await knex.select(
                'cohorts.name',
                'students.username',
                'students.last_name',
                'students.first_name',
                'lessons.description',
                'lessons.date_time',
                'attendance.type'
            ).from('students_attendance')
                .leftJoin('lessons', 'lessons.id', 'students_attendance.lesson_id')
                .leftJoin('students', 'students.id', 'students_attendance.student_id')
                .leftJoin('attendance', 'attendance.id', 'students_attendance.attendance_id')
                .leftJoin('cohorts', 'cohorts.id', 'lessons.cohort_id')
                .where('lessons.date_time', '>', date)
                .andWhere('lessons.date_time', '<', moment.tz(new Date(), "Hongkong").format())
                .andWhere('attendance.type', 'absent')

            let absentStudent = [];
            for (let result of results) {
                result.date_time = moment.tz(result.date_time, "Hongkong").format()
                absentStudent.push(result)
            }

            let transform = {};
            for (let student of absentStudent) {
                if (!transform[student.username]) {  //cohort
                    transform[student.username] = {
                        'last_name': student.last_name,
                        'first_name': student.first_name,
                        'lesson': [
                            {
                                "cohort": student.name,
                                "name": student.description,
                                "dateTime": student.date_time
                            }
                        ]
                    }
                } else {
                    transform[student.username].lesson.push(
                        {
                            "cohort": student.name,
                            "name": student.description,
                            "dateTime": student.date_time
                        }
                    )
                }
            }

            for (let student in transform) {
                transform[student]['newLesson'] = {}
                for (let lesson of transform[student]['lesson']) {
                    if (!transform[student]['newLesson'][lesson.cohort]) {
                        transform[student]['newLesson'][lesson.cohort] = [
                            {
                                "lessonName": lesson.name,
                                "dateTime": lesson.dateTime
                            }
                        ]
                    } else {
                        transform[student]['newLesson'][lesson.cohort].push(
                            {
                                "lessonName": lesson.name,
                                "dateTime": lesson.dateTime
                            }
                        )
                    }
                }
                delete transform[student]['lesson']
            }

            resolve(transform)
        } catch (e) {
            console.error(e);
        } finally {
            await knex.destroy();
        }

    })


const html = () =>
    new Promise(async (resolve) => {
        try {
            console.log("SendGrid: Weekly absent report")
            const results: any = (await weekAttendance(minusDays(5)))
            let html = [];
            for (let result in results) {
                html.push(`<div style="font-weight:bold;">${results[result].first_name} ${results[result].last_name}</div>`)
                for (let cohort in results[result]["newLesson"]) {
                    html.push(`<div style="text-decoration: underline;"> ${cohort}</div><ol>`)
                    for (let lesson of results[result]["newLesson"][cohort]) {
                        html.push(`<li>  ${lesson.lessonName} - ${moment.tz(lesson.dateTime, "Hongkong").format('MMMM Do YYYY,h:mm:ss a')}</li>`)
                    }
                    html.push('</ol>')
                }
                html.push('<hr>');
            }

            const combinedHTML = html.join('')


            resolve(combinedHTML)
        } catch (e) {
            console.error(e)
        }
    })



html().then((sendGridHTML: string) => {
    try {
        sgMail.setApiKey(process.env.SENDGRID_API_KEY ? process.env.SENDGRID_API_KEY : '');

        const msg = {
            to: process.env.SENDGRID_EMAIL as string,
            from: process.env.SENDGRID_EMAIL as string,
            subject: `Report - ${moment.tz((new Date()), "Hongkong").format('MMMM Do YYYY')}`,
            html: (sendGridHTML == '' ? '<strong>No Absence this week</strong>' : sendGridHTML),
        };
        sgMail.send(msg);


        console.log(`Email sent to ${process.env.SENDGRID_EMAIL}`)

    } catch (e) {
        console.error(e)
    }
})






