import * as Knex from "knex";


export async function seed(knex: Knex): Promise<any> {
    // Deletes ALL existing entries

    const trx = await knex.transaction();

    try{
        const instructors = await trx.raw(/*SQL*/`select * from instructors`);
        const instructorData = instructors.rows[0]

        await trx("instructors").del()

        await trx("instructors").insert([
            {
                username:instructorData["username"],
                first_name:"Sir",
                last_name:"King",
                phone_number:"67777777",
                nickname:"KINGSIR",
                gitlab_id:instructorData["gitlab_id"]

        }
        ]).returning("id");

        console.log("[info] transaction commit");
        await trx.commit();

    }catch(err){
        console.error(err);
        console.log('[info] transaction rollback');
        await trx.rollback();
    }finally{
        await trx.destroy();
    }
    
       
};
