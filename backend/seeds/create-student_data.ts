import * as Knex from "knex";

export async function seed(knex: Knex): Promise<any> {
    // Deletes ALL existing entries
    const trx = await knex.transaction();

    try {
        const students = await trx.raw(/*SQL*/`select * from students`);
        const studentIds = students.rows;

        await trx("payment").del();
        await trx("students_cohort").del();
        await trx("students_products").del();
        await trx("products_cohorts").del();
        await trx("payment_types").del();
        await trx("attendance").del();
        await trx("cohorts").del();
        await trx("products").del();
        await trx("students").del()

        //insert Students
        const newStudentID: number[] = await trx("students").insert([
            {
                username: studentIds[0]["username"],
                first_name: "Jerry",
                last_name: "Chan",
                address: "Flat 999, King State, NT",
                phone_number: "68999999",
                nickname: "Jerry",
                stripe_customer_id: "cus_HMnRvnRAzLQ6im",
                gitlab_id: studentIds[0]["gitlab_id"]
            },
            {
                username: studentIds[1]["username"],
                first_name: "Kin",
                last_name: "Ko",
                address: "Flat 888, Tecky State, Kowloon",
                phone_number: "99998888",
                nickname: "Kin",
                stripe_customer_id: "cus_HJFqYZPXl7g4RE",
                gitlab_id: studentIds[1]["gitlab_id"]
            }
        ]).returning("id");

        //Insert products
        const productsId = await trx("products").insert([
            {
                name: "Micro Master",
                price: 57500.00
            },
            {
                name: "Micro Master+Data Science",
                price: (57500.00 + 9625.00)
            },

        ]).returning("id");

        //insert cohorts
        const cohortsId = await trx("cohorts").insert([
            {
                name: "hk-map-06-Jul-20",
                "start_date": new Date("2020-07-06")
            },
            {
                name: "hk-map-05-Oct-20",
                "start_date": new Date("2020-10-05")
            }
        ]).returning("id");

        //insert attendance
        await trx("attendance").insert([
            { type: "attended" },
            { type: "absent" },
            { type: "late" },
            { type: "leave" }
        ])

        //insert payment_types
        const payment_typesId = await trx("payment_types").insert([
            {
                type: "credit card"
            },
            {
                type: "cash"
            }
        ]).returning("id");

        //insert products_cohorts
        const productsCohortsId = await trx("products_cohorts").insert([
            {
                product_id: productsId[0],
                cohort_id: cohortsId[0],
                available: true
            },
            {
                product_id: productsId[0],
                cohort_id: cohortsId[1],
                available: true
            },
            {
                product_id: productsId[1],
                cohort_id: cohortsId[0],
                available: true
            },
            {
                product_id: productsId[1],
                cohort_id: cohortsId[1],
                available: true
            }

        ]).returning("id");


        //insert students_products
        const students_productsId = await trx("students_products").insert([
            {
                student_id: newStudentID[0],
                product_id: productsId[0],
                personal_price: 57500,
                total_instalment: 12
            },
            {
                student_id: newStudentID[1],
                product_id: productsId[1],
                personal_price: 67125,
                total_instalment: 12
            }
        ]).returning("id");

        //insert students_cohort
        const students_cohortId = await trx("students_cohort").insert([
            {
                students_id: newStudentID[0],
                cohort_id: cohortsId[0],
                quitted: false
            },
            {
                students_id: newStudentID[1],
                cohort_id: cohortsId[0],
                quitted: false
            }
        ]).returning("id");

        //inset payment
        await trx("payment").insert([
            {
                date: new Date("2020-05-01"),
                amount: Math.round(57500.00 / 12),
                student_id: newStudentID[0],
                payment_type_id: payment_typesId[0],
                product_id: productsId[0],
                transaction_number: "pi_1GkZsRIvBXg7JtvyPSbRtsQU"
            },
            {
                date: new Date("2020-05-01"),
                amount: Math.round(67125.00 / 12),
                student_id: newStudentID[1],
                payment_type_id: payment_typesId[0],
                product_id: productsId[1],
                transaction_number: "pi_1Gm1VXIvBXg7JtvyPd4g22tQ"
            },

        ])

        console.log(`productsCohortsId:${productsCohortsId}`)
        console.log(`students_productsId:${students_productsId}`)
        console.log(`students_cohortId:${students_cohortId}`)

        console.log("[info] transaction commit");
        await trx.commit();

    } catch (err) {
        console.error(err);
        console.log('[info] transaction rollback');
        await trx.rollback();
    } finally {
        await trx.destroy();
    }

};
