import * as Knex from "knex";

export async function seed(knex: Knex): Promise<any> {
    // Deletes ALL existing entries
    const trx = await knex.transaction();
    try {
        await trx("dashboard").del();
        await trx("payment").del();
        await trx("students_cohort").del();
        await trx("students_products").del();
        await trx("products_cohorts").del();
        await trx("payment_types").del();
        await trx("students_attendance").del();
        await trx("attendance").del();
        await trx("lessons").del();
        await trx("leave_apply").del();
        await trx("cohorts").del();
        await trx("products").del();
        await trx("students").del()
        await trx("instructors").del();


        //Insert products
        const productsId = await trx("products").insert([
            {
                name: "Micro Master",
                price: 57500.00
            },
            {
                name: "Micro Master+Data Science",
                price: (57500.00 + 9625.00)
            },

        ]).returning("id");

        //insert cohorts
        const cohortsId = await trx("cohorts").insert([
            {
                name: "hk-map-06-Jul-20",
                "start_date": new Date("2020-07-06")
            },
            {
                name: "hk-map-05-Oct-20",
                "start_date": new Date("2020-10-05")
            }
        ]).returning("id");

        //insert attendance
        await trx("attendance").insert([
            { type: "attended" },
            { type: "absent" },
            { type: "late" },
            { type: "leave" }
        ])

        //insert payment_types
        const payment_typesId = await trx("payment_types").insert([
            {
                type: "credit card"
            },
            {
                type: "cash"
            }
        ]).returning("id");

        //insert products_cohorts
        const productsCohortsId = await trx("products_cohorts").insert([
            {
                product_id: productsId[0],
                cohort_id: cohortsId[0],
                available: true
            },
            {
                product_id: productsId[0],
                cohort_id: cohortsId[1],
                available: true
            },
            {
                product_id: productsId[1],
                cohort_id: cohortsId[0],
                available: true
            },
            {
                product_id: productsId[1],
                cohort_id: cohortsId[1],
                available: true
            }

        ]).returning("id");

        await trx("dashboard").insert([
            {
                title: "CMS",
                description: "A content management system (CMS) is a software application that can be used to manage the creation and modification of digital content.",
                link: "https://cms.tecky.io/login/index.php",
                image_path: "cms.jpg",
                product_id: productsId[0]
            },
            {
                title: "Rocket Chat",
                description: "Rocket.Chat is the leading open source team chat software solution. Free, unlimited and completely customizable with on-premises and SaaS cloud hosting.",
                link: "https://chat.tecky.io/channel/general",
                image_path: "rocketchat.jpg",
                product_id: productsId[0]
            },
            {
                title: "CMS",
                description: "A content management system (CMS) is a software application that can be used to manage the creation and modification of digital content.",
                link: "https://cms.tecky.io/login/index.php",
                image_path: "cms.jpg",
                product_id: productsId[1]
            },
            {
                title: "Rocket Chat",
                description: "Rocket.Chat is the leading open source team chat software solution. Free, unlimited and completely customizable with on-premises and SaaS cloud hosting.",
                link: "https://chat.tecky.io/channel/general",
                image_path: "rocketchat.jpg",
                product_id: productsId[1]
            },

        ])


        console.log(payment_typesId);
        console.log(productsCohortsId);



        console.log("[info] transaction commit");
        await trx.commit();

    } catch (err) {
        console.error(err);
        console.log('[info] transaction rollback');
        await trx.rollback();
    } finally {
        await trx.destroy();
    }

};
