import * as Knex from "knex";

export async function seed(knex: Knex): Promise<any> {
    const trx = await knex.transaction();

    try {
        await trx("dashboard").del()

        await trx("dashboard").insert([
            {
                title:"CMS",
                description:"A content management system (CMS) is a software application that can be used to manage the creation and modification of digital content.",
                link:"https://cms.tecky.io/login/index.php",
                image:"https://doofindermedia.s3.amazonaws.com/blog/2018/09/10/082234-cms-tienda-online-1.jpg"
            },
            {
                title:"Rocket Chat",
                description:"Rocket.Chat is the leading open source team chat software solution. Free, unlimited and completely customizable with on-premises and SaaS cloud hosting.",
                link:"https://chat.tecky.io/channel/general",
                image:"https://dashboard.snapcraft.io/site_media/appmedia/2018/12/icon-256_sDZsivC.png"
            }
        ])

        console.log("[info]dashboard transaction commit");
        await trx.commit();

    } catch (err) {
        console.error(err);
        console.log('[info]dashboard transaction rollback');
        await trx.rollback();
    } finally {
        await trx.destroy();
    }
};
