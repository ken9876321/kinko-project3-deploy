import * as Knex from "knex";

export async function seed(knex: Knex): Promise<any> {
    const trx = await knex.transaction();

    try {
        await trx("coupon").del()

        await trx("coupon").insert([
            {
                name: "3個月以上超早鳥",
                code: "tecky3month1246dewq12fds",
                discount: 7500,
            },
            {
                name: "2個月早鳥",
                code: "tecky2month8964",
                discount: 4500,
            },
            {
                name: "1個月早鳥",
                code: "tecky1month8787",
                discount: 2500,
            },
        ])

        console.log("[info] transaction commit");
        await trx.commit();

    } catch (err) {
        console.error(err);
        console.log('[info] transaction rollback');
        await trx.rollback();
    } finally {
        await trx.destroy();
    }
};
