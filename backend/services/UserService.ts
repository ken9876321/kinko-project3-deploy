import Knex from 'knex';
import { User } from '../model'


export class UserService {
    constructor(private knex: Knex) { }

    getUser = async (id: number, username: string): Promise<User> => {
        const emailDomain = username.indexOf('@');
        const recognizedDomain = username.slice(emailDomain);
        if (recognizedDomain === `${process.env.INSTRUCTOR_DOMAIN}`) {
            const result: any = await this.knex('instructors').where({ id: id })

            result[0].isInstructor = true;

            return result[0];
        } else {
            const result: any = await this.knex('students').where({ id: id })

            result[0].isInstructor = false;

            return result[0];
        }
    }

    checkUserCohort = async (studentId: number) => {
        const result = await this.knex.raw(/*SQL*/`SELECT id FROM students_cohort WHERE students_id = (?)`, [studentId])
        return result.rowCount;
    }

    getUserByUsername = async (username: string): Promise<User> => {
        const emailDomain = username.indexOf('@');
        const recognizedDomain = username.slice(emailDomain);
        if (recognizedDomain === `${process.env.INSTRUCTOR_DOMAIN}`) {
            const result: any = await this.knex('instructors').where({ username: username })
            return result[0];
        } else {
            const result: any = await this.knex('students').where({ username: username })
            console.log("SQL: " + result)
            return result[0];

        }
    }

    createUser = async (username: string, gitlabID: string): Promise<User> => {
        const emailDomain = username.indexOf('@');
        const recognizedDomain = username.slice(emailDomain);
        if (recognizedDomain === process.env.INSTRUCTOR_DOMAIN!) {
            const result: any = await this.knex.insert({
                username: username,
                gitlab_id: gitlabID
            }).into("instructors").returning(['id', 'username']);

            return result[0];
        } else {
            const result: any = await this.knex.insert({
                username: username,
                gitlab_id: gitlabID
            }).into("students").returning(['id', 'username']);

            return result[0];
        }
    }

    settingForm = async (username: string, lastname: string, firstName: string, nickname: string, address: string, phoneNumber: string): Promise<User> => {
        const emailDomain = username.indexOf('@');
        const recognizedDomain = username.slice(emailDomain);
        if (recognizedDomain === `${process.env.INSTRUCTOR_DOMAIN}`) {
            const result: any = await this.knex('instructors').where({ username: username }).update({
                'last_name': lastname,
                'first_name': firstName,
                'nickname': nickname,
                'phone_number': phoneNumber
            }, [
                'id',
                'username',
                'last_name',
                'first_name',
                'nickname',
                'phone_number',

            ]);

            result[0].isInstructor = true;

            return result[0];

        } else {
            const result = await this.knex('students').where({ username: username }).update({
                'last_name': lastname,
                'first_name': firstName,
                'nickname': nickname,
                'phone_number': phoneNumber,
                'address': address
            }, [
                'id',
                'username',
                'last_name',
                'first_name',
                'nickname',
                'phone_number',
                'address'
            ])

            result[0].isInstructor = false;

            return result[0];
        }
    }
}