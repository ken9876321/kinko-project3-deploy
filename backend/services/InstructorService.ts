import Knex from 'knex';
import { Cohort, Lesson, User, Students_cohort, Attendance, AttendanceReport, LeaveApp, LeaveAppInstructors, Offer, Product, Coupon, StudentOnLeave, PaymentInfo, DealInfo, CohortInfo, StudentInfo } from '../model';
import { Customer } from './StudentService';


export class InstructorService {
    constructor(private knex: Knex) { }

    getCohort = async (): Promise<Cohort[]> => {
        const result: Cohort[] = await this.knex.select().from('cohorts')

        return result;
    }

    createLesson = async (user: User, cohortId: number, description: string, datetime: Date) => {
        const trx = await this.knex.transaction();
        try {
            const emailDomain = user.username.indexOf('@');
            const recognizedDomain = user.username.slice(emailDomain);

            if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
                return
            }

            const cohort: Cohort = (await trx('cohorts').where('id', cohortId).select())[0];

            const lesson: Lesson = (await trx('lessons').returning([
                'id'
            ]).insert({
                cohort_id: cohort.id,
                description: description,
                date_time: datetime
            }))

            const instructor: User = (await trx('instructors').where('id', user.id))[0];

            const students: Students_cohort[] = await trx.select(
                'students.id',
                'students.username',
                'students.nickname',
                'students.last_name',
                'students.first_name'
            ).from('students')
                .leftJoin('students_cohort', 'students.id', 'students_cohort.students_id').where({
                    quitted: false,
                    cohort_id: cohortId
                })


            for (let student of students) {
                await trx('students_attendance').insert({
                    student_id: student.id,
                    instructor_id: instructor.id,
                    lesson_id: lesson[0].id
                })
            }

            const index = datetime.toString().indexOf("T");
            const formatedDateTime = datetime.toString().slice(0, index);

            const studentOnLeave: StudentOnLeave[] = await trx.select(
                'students.nickname',
                'students.last_name',
                'students.first_name',
                'leave_apply.period_from',
                'leave_apply.period_to'
            ).from('leave_apply')
                .leftJoin('students', 'students.id', 'leave_apply.student_id')
                .where('leave_apply.period_from', '<=', formatedDateTime)
                .andWhere('leave_apply.period_to', '>=', formatedDateTime)
                .andWhere('leave_apply.approval', true)

            console.log("[info] transaction commit");
            await trx.commit();

            return {
                students: students,
                lessonId: lesson[0].id,
                cohortName: cohort.name,
                studentOnLeave: studentOnLeave
            };

        } catch (e) {
            console.error(e);
            console.log('[info] transaction rollback');
            await trx.rollback();
            return
        } finally {
            await trx.destroy();
        }
    }

    takeAttendance = async (user: User, studentId: number, attendance: number, lessonId: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        const student: User = (await this.knex('students').where('id', studentId))[0]

        const lesson: User = (await this.knex('lessons').where('id', lessonId))[0]

        const attendanceType: Attendance = (await this.knex('attendance').where('id', attendance))[0]

        await this.knex('students_attendance').where({
            student_id: student.id,
            lesson_id: lesson.id
        }).update({ attendance_id: attendanceType.id })


        return { msg: "Received!" }

    }

    getLessonRecord = async (user: User, cohortId: number, limit: number, offset: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        const countLesson: Lesson[] = (await this.knex('lessons').where('cohort_id', cohortId).orderBy('date_time'))

        const lesson: Lesson[] = (await this.knex('lessons').where('cohort_id', cohortId).orderBy('date_time').limit(limit).offset(offset))


        return {
            result: lesson,
            length: countLesson.length
        }

    }

    getAttendance = async (user: User, lessonId: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        const studentAttendance = await this.knex.select(
            'students_attendance.lesson_id',
            'attendance.type',
            'students.id',
            'students.username',
            'students.nickname',
            'students.last_name',
            'students.first_name'
        ).from('students_attendance')
            .leftJoin('attendance', 'attendance.id', 'students_attendance.attendance_id')
            .leftJoin('instructors', 'instructors.id', 'students_attendance.instructor_id')
            .leftJoin('students', 'students.id', 'students_attendance.student_id')
            .where({
                lesson_id: lessonId
            })

        const correspondInstructor = await this.knex.select(
            'instructors.id',
            'instructors.username',
            'instructors.nickname',
            'instructors.last_name',
            'instructors.first_name'
        ).from('students_attendance')
            .leftJoin('attendance', 'attendance.id', 'students_attendance.attendance_id')
            .leftJoin('instructors', 'instructors.id', 'students_attendance.instructor_id')
            .leftJoin('students', 'students.id', 'students_attendance.student_id')
            .where({
                lesson_id: lessonId
            })

        return {
            result: {
                students: studentAttendance,
                instructor: correspondInstructor
            }

        }
    }

    revisedLessonName = async (user: User, lessonId: number, newName: string) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        await this.knex('lessons').where('id', lessonId).update({
            description: newName
        })

        return { msg: "Revised!" }
    }

    revisedLessonDateTime = async (user: User, lessonId: number, newDateTime: string) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        await this.knex('lessons').where('id', lessonId).update({
            date_time: newDateTime
        })

        return { msg: "Revised!" }
    }

    getAttendanceReport = async (user: User, cohortId: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        const cohortAttendance = await this.knex.select(
            'students.username',
            'students.nickname',
            'students.last_name',
            'students.first_name',
            'attendance.type'
        )
            .from('cohorts')
            .leftJoin('lessons', 'lessons.cohort_id', 'cohorts.id')
            .leftJoin('students_attendance', 'students_attendance.lesson_id', 'lessons.id')
            .leftJoin('students', 'students.id', 'students_attendance.student_id')
            .leftJoin('attendance', 'attendance.id', 'students_attendance.attendance_id')
            .where({
                cohort_id: cohortId
            })


        let outcome = {};

        for (let attendance of cohortAttendance) {
            if (outcome[attendance.username] && attendance.type === "attended" && outcome[attendance.username].attended) {
                outcome[attendance.username] = {
                    ...outcome[attendance.username],
                    attended: outcome[attendance.username].attended + 1
                }
            } else if (outcome[attendance.username] && attendance.type === "late" && outcome[attendance.username].late) {
                outcome[attendance.username] = {
                    ...outcome[attendance.username],
                    late: outcome[attendance.username].late + 1
                }
            } else if (outcome[attendance.username] && attendance.type === "absent" && outcome[attendance.username].absent) {
                outcome[attendance.username] = {
                    ...outcome[attendance.username],
                    absent: outcome[attendance.username].absent + 1
                }
            } else if (outcome[attendance.username] && attendance.type === "leave" && outcome[attendance.username].leave) {
                outcome[attendance.username] = {
                    ...outcome[attendance.username],
                    leave: outcome[attendance.username].leave + 1
                }
            } else if (outcome[attendance.username] && attendance.type === "attended" && !outcome[attendance.username].attended) {
                outcome[attendance.username] = {
                    ...outcome[attendance.username],
                    attended: 1
                }
            } else if (outcome[attendance.username] && attendance.type === "late" && !outcome[attendance.username].late) {
                outcome[attendance.username] = {
                    ...outcome[attendance.username],
                    late: 1
                }
            } else if (outcome[attendance.username] && attendance.type === "absent" && !outcome[attendance.username].absent) {
                outcome[attendance.username] = {
                    ...outcome[attendance.username],
                    absent: 1
                }
            } else if (outcome[attendance.username] && attendance.type === "leave" && !outcome[attendance.username].leave) {
                outcome[attendance.username] = {
                    ...outcome[attendance.username],
                    leave: 1
                }
            } else if (!outcome[attendance.username] && attendance.type === "attended") {
                outcome[attendance.username] = {
                    username: attendance.username,
                    nickname: attendance.nickname,
                    last_name: attendance.last_name,
                    first_name: attendance.first_name,
                    attended: 1
                }
            } else if (!outcome[attendance.username] && attendance.type === "late") {
                outcome[attendance.username] = {
                    username: attendance.username,
                    nickname: attendance.nickname,
                    last_name: attendance.last_name,
                    first_name: attendance.first_name,
                    late: 1
                }
            } else if (!outcome[attendance.username] && attendance.type === "absent") {
                outcome[attendance.username] = {
                    username: attendance.username,
                    nickname: attendance.nickname,
                    last_name: attendance.last_name,
                    first_name: attendance.first_name,
                    absent: 1
                }
            } else if (!outcome[attendance.username] && attendance.type === "leave") {
                outcome[attendance.username] = {
                    username: attendance.username,
                    nickname: attendance.nickname,
                    last_name: attendance.last_name,
                    first_name: attendance.first_name,
                    leave: 1
                }
            }
        }

        let transform: any = []
        for (let [key, value] of Object.entries(outcome)) {
            key;
            transform.push(value)
        }

        const lessons: Lesson[] = await this.knex("lessons").where({ cohort_id: cohortId })

        let result: AttendanceReport[] = []

        for (let beCalulate of transform) {
            beCalulate = {
                ...beCalulate,
                attended: ((beCalulate.attended / lessons.length) * 100).toFixed(0),
                absent: ((beCalulate.absent / lessons.length) * 100).toFixed(0),
                late: ((beCalulate.late / lessons.length) * 100).toFixed(0),
                leave: ((beCalulate.leave / lessons.length) * 100).toFixed(0),
            }
            result.push(beCalulate)
        }

        return { result: result }
    }

    getAttendanceId = async (user: User) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        const attendanceId: Attendance[] = await this.knex('attendance').select();

        let transformId = {};
        for (let idType of attendanceId) {
            transformId[idType.type] = idType.id
        }

        return { result: transformId }
    }

    delLesson = async (user: User, lessonId: number) => {
        const trx = await this.knex.transaction();
        try {
            const emailDomain = user.username.indexOf('@');
            const recognizedDomain = user.username.slice(emailDomain);

            if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
                return
            }

            await trx('students_attendance').where('lesson_id', lessonId).del();

            await trx('lessons').where('id', lessonId).del();


            console.log("[info] transaction commit");
            await trx.commit();
            return { msg: "Deleted!" }
        } catch (e) {
            console.error(e);
            console.log('[info] transaction rollback');
            await trx.rollback();
            return
        } finally {
            await trx.destroy();
        }
    }

    getLeaveApp = async (user: User) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        const leaveApp: LeaveApp[] = await this.knex.select(
            'students.username',
            'students.nickname',
            'students.last_name',
            'students.first_name',
            'leave_apply.period_from',
            'leave_apply.period_to',
            'leave_apply.reason',
            'leave_apply.approval',
            'cohorts.name',
            'leave_apply.id',
            'leave_apply.created_at'
        ).from('leave_apply')
            .leftJoin('students', 'students.id', 'leave_apply.student_id')
            .leftJoin('cohorts', 'cohorts.id', 'leave_apply.cohort_id')

        const instructors: LeaveAppInstructors[] = await this.knex.select(
            'instructors.username',
            'instructors.nickname',
            'instructors.last_name',
            'instructors.first_name',
            'leave_apply.id'
        ).from('leave_apply')
            .leftJoin('instructors', 'instructors.id', 'leave_apply.instructor_id')


        return {
            result: {
                leaveAppWithStudent: leaveApp,
                leaveAppInstructors: instructors
            }
        }
    }

    approveLeave = async (user: User, leaveId: number, approval: boolean) => {
        const trx = await this.knex.transaction();
        try {
            const emailDomain = user.username.indexOf('@');
            const recognizedDomain = user.username.slice(emailDomain);

            if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
                return
            }

            const instructor: User = (await trx('instructors').where('id', user.id))[0];

            await trx('leave_apply').where('id', leaveId).update({
                instructor_id: instructor.id,
                approval: approval
            }).orderBy('created_at')


            console.log("[info] transaction commit");
            await trx.commit();
            return { msg: "Success!" }
        } catch (e) {
            console.error(e);
            console.log('[info] transaction rollback');
            await trx.rollback();
            return
        } finally {
            await trx.destroy();
        }
    }

    delLeave = async (user: User, leaveId: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        await this.knex('leave_apply').where('id', leaveId).del();

        return { msg: "Deleted!" }

    }

    getOffer = async (user: User) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        let products: Offer[] = (await this.knex.select(
            'products_cohorts.id',
            'products.name',
            'products.price',
            'products_cohorts.available'
        ).from('products_cohorts')
            .leftJoin('products', 'products.id', 'products_cohorts.product_id')
            .orderBy('products_cohorts.created_at'))

        const cohorts: Cohort[] = (await this.knex.select(
            'cohorts.name',
            'cohorts.start_date',
        ).from('products_cohorts')
            .leftJoin('cohorts', 'cohorts.id', 'products_cohorts.cohort_id')
            .orderBy('products_cohorts.created_at'))

        for (let i in cohorts) {
            products[i].cohortName = cohorts[i].name;
            products[i].startDate = cohorts[i].start_date;
        }

        return { result: products }
    }

    addOffer = async (user: User, productId: number, cohortId: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        const product: Product = (await this.knex('products').where('id', productId))[0]

        const cohort: Cohort = (await this.knex('cohorts').where('id', cohortId))[0]

        await this.knex('products_cohorts').insert({
            product_id: product.id,
            cohort_id: cohort.id,
            available: true
        })

        return { msg: "Success!" }
    }

    delOffer = async (user: User, offerId: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        await this.knex('products_cohorts').where('id', offerId).del();

        return { msg: "Deleted!" }
    }

    addCohort = async (user: User, cohortName: string, date: string) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        await this.knex('cohorts').insert({
            name: cohortName,
            start_date: date
        })

        return { msg: "Success!" }
    }

    addProduct = async (user: User, productName: string, price: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        await this.knex('products').insert({
            name: productName,
            price: price
        })

        return { msg: "Success!" }
    }

    getProductCohort = async (user: User) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        let products: Product[] = await this.knex('products')
            .select()
            .orderBy('created_at')

        const cohorts: Cohort[] = (await this.knex('cohorts')
            .select()
            .orderBy('created_at'))

        return {
            result: {
                products: products,
                cohorts: cohorts
            }
        }
    }

    getCoupon = async (user: User) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        let coupon: Coupon[] = await this.knex('coupon')
            .select()
            .orderBy('created_at')


        return { result: coupon }
    }

    addCoupon = async (user: User, couponName: string, code: string, discount: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        await this.knex('coupon').insert({
            name: couponName,
            code: code,
            discount: discount
        })

        return { msg: "Success!" }
    }

    delCoupon = async (user: User, couponId: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        await this.knex('coupon').where('id', couponId).del();

        return { msg: "Deleted!" }
    }

    postMaterials = async (title: string, description: string, link: string, image_path: string | null, product_id: number) => {
        const trx = await this.knex.transaction();
        try {
            await trx('dashboard').insert([{
                title: title,
                description: description,
                link: link,
                image_path: image_path,
                product_id: product_id
            }])


            console.log("[info]postMaterials transaction commit");
            await trx.commit();
            return true
        } catch (e) {
            console.error(e);
            console.log('[info] transaction rollback');
            await trx.rollback();
            return false
        } finally {
            await trx.destroy();
        }
    }

    deleteMaterials = async (materialId: number) => {
        const trx = await this.knex.transaction();
        try {
            await trx('dashboard').where('id', materialId).del();


            console.log("[info]deleteMaterials transaction commit");
            await trx.commit();
            return true
        } catch (e) {
            console.error(e);
            console.log('[info]deleteMaterials transaction rollback');
            await trx.rollback();
            return false
        } finally {
            await trx.destroy();
        }
    }

    updateStudent = async (studentId: number, studentData: Customer) => {
        const trx = await this.knex.transaction();
        try {
            const currentId = await trx('students').where('id', studentId).update({
                first_name: studentData.firstName,
                last_name: studentData.lastName,
                address: studentData.address,
                phone_number: studentData.phone,
                nickname: studentData.nickName,
            }).returning('id')

            await trx('students_cohort').insert([{
                students_id: currentId[0],
                cohort_id: parseInt(studentData.cohort),
                quitted: false
            }])

            console.log("[info]updateStudent transaction commit");
            await trx.commit();
            return true
        } catch (err) {
            console.error(err);
            console.log('[info]updateStudent transaction rollback');
            await trx.rollback();
            return false
        } finally {
            await trx.destroy();
        }
    }

    paymentRecord = async (amount: number, studentId: number, productId: number, instalment: number) => {
        const trx = await this.knex.transaction();
        try {
            const paymentTypeId = await trx.raw(/*SQL*/`SELECT id FROM "payment_types" WHERE "type" = 'cash'`)

            await trx('payment').insert([{
                date: new Date(),
                amount: Math.round(amount / instalment),
                student_id: studentId,
                payment_type_id: paymentTypeId.rows[0].id,
                product_id: productId,
                transaction_number: 'cash'
            }])

            console.log("[info]paymentRecord transaction commit");
            await trx.commit();
            return true;
        } catch (err) {
            console.error(err);
            console.log('[info]paymentRecord transaction rollback');
            await trx.rollback();
            return false;
        } finally {
            await trx.destroy();
        }
    }

    updateStudentProducts = async (studentId: number, productId: number, instalment: number, price: number) => {
        const trx = await this.knex.transaction();
        try {
            await trx("students_products").insert([{
                student_id: studentId,
                product_id: productId,
                personal_price: price,
                total_instalment: instalment
            }])

            console.log("[info]updateStudentProducts transaction commit");
            await trx.commit();
            return true;
        } catch (err) {
            console.error(err);
            console.log('[info]updateStudentProducts transaction rollback');
            await trx.rollback();
            return false;
        } finally {
            await trx.destroy();
        }
    }

    getStudent = async (username: string) => {
        const result = this.knex.select('id').from('students').where('username', username)
        return result
    }



    getMaterials = async (product_id: number) => {
        const materials = await this.knex.select('id', 'title', 'description', 'link', 'image_path', 'product_id').where('product_id', product_id).from('dashboard');
        return materials;
    }

    checkProductID = async (product_id: number) => {
        const product = await this.knex.raw(/*SQL*/`SELECT id FROM products_cohorts WHERE product_id = (?)`, [product_id])
        return product.rowCount
    }

    getStudentByID = async (user: User, studentId: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        const student: User[] = await this.knex('students').where('id', studentId)

        return { student: student };
    }

    getStudentByText = async (user: User, text: string) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        if (text === '') {
            return { msg: "empty" }
        }

        const student: StudentInfo[] = await this.knex('students').select();

        const studentFound = student.filter(student => {
            const name = (`${student.first_name}${student.last_name}${student.nickname}`).toLowerCase();

            return name.includes(text.toLowerCase())
        })

        return { student: studentFound };
    }

    getStudentInfoByID = async (user: User, studentId: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        const paymentInfo: PaymentInfo[] = await this.knex.select(
            'payment.date',
            'payment.amount',
            'payment_types.type',
            'products.id'
        ).from('payment')
            .leftJoin('payment_types', 'payment_types.id', 'payment.payment_type_id')
            .leftJoin('products', 'products.id', 'payment.product_id')
            .where('payment.student_id', studentId)

        const dealInfo: DealInfo[] = await this.knex.select(
            'students_products.product_id',
            'students_products.personal_price',
            'students_products.total_instalment',
            'products.name'
        ).from('students_products')
            .leftJoin('products', 'products.id', 'students_products.product_id')
            .where('students_products.student_id', studentId)

        for (let deal of dealInfo) {
            deal["payment"] = [];
            deal["balance"] = deal.personal_price;
            deal["instalmentLeft"] = deal.total_instalment
            for (let payment of paymentInfo) {
                if (deal.product_id === payment.id) {
                    deal["payment"].push(payment)
                    deal["balance"] = deal["balance"] - payment.amount
                    deal["instalmentLeft"]--
                }
            }
        }


        const cohortInfo: CohortInfo[] = await this.knex.select(
            'students_cohort.quitted',
            'cohorts.name',
            'students_cohort.id',
        ).from('students_cohort')
            .leftJoin('cohorts', 'cohorts.id', 'students_cohort.cohort_id')
            .where('students_cohort.students_id', studentId)

        return { dealInfo: dealInfo, cohortInfo: cohortInfo }
    }

    turnCohortQuit = async (user: User, id: number) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        const result = (await this.knex('students_cohort').where({
            id: id
        }))[0]

        if (result.quitted) {
            await this.knex('students_cohort').where({
                id: id,
            }).update({ quitted: false })
        } else {
            await this.knex('students_cohort').where({
                id: id,
            }).update({ quitted: true })
        }

        return { msg: "Success!" }
    }

    insertPayment = async (user: User, id: number, courseName: string, amount: number, date: string) => {
        const emailDomain = user.username.indexOf('@');
        const recognizedDomain = user.username.slice(emailDomain);

        if (recognizedDomain !== process.env.INSTRUCTOR_DOMAIN) {
            return
        }

        const paymentType = (await this.knex('payment_types').where('type', "cash"))[0]

        const course = (await this.knex('products').where('name', courseName))[0]

        await this.knex('payment').insert({
            date: date,
            amount: amount,
            student_id: id,
            payment_type_id: paymentType.id,
            product_id: course.id
        })

        return { msg: "Success!" }
    }
}