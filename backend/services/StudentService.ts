import Knex from 'knex';
import sgMail from '@sendgrid/mail';
import moment from 'moment';
import 'moment-timezone';

export interface Customer {
    instalment: number;
    nickName: string;
    firstName: string;
    lastName: string;
    address: string;
    phone: string;
    email: string;
    product: string;
    cohort: string;
    price: string;
}

interface SucceededIntent {
    id: string,
    amount: number,
    date: any,
}

interface LeaveForm {
    cohort: string;
    fromDate: string;
    toDate: string;
    reason: string;
    instructor: string
}

export class StudentService {
    constructor(private knex: Knex) { }



    getProducts = async () => {
        const result = await this.knex.raw(/*SQL*/`SELECT products.id AS "product_id", "name" AS "product_name", price from products 
        INNER JOIN "products_cohorts" ON products.id = "products_cohorts".product_id 
        WHERE "products_cohorts".available = true
        GROUP BY products.id`);
        return result.rows
    }

    getCohorts = async () => {
        const result = await this.knex.raw(/*SQL*/`SELECT cohorts.id AS "cohort_id", cohorts."name" AS "cohort_name", TO_CHAR(cohorts."start_date", 'DD Mon YYYY') AS "start_date" from cohorts 
        INNER JOIN "products_cohorts" ON cohorts.id = "products_cohorts".cohort_id 
        WHERE "products_cohorts".available = true
        GROUP BY cohorts.id
        ORDER BY cohorts.id
        `);
        return result.rows
    }

    checkCohort = async (studentID: number, cohortId: number) => {
        const result = await this.knex.raw(/*SQL*/`SELECT * FROM students_cohort WHERE students_cohort.students_id = (?)
        AND students_cohort.cohort_id = (?)
        `, [studentID, cohortId]);
        return result.rows;
    }

    getStAllAttendance = async (studentID: number, limit: number, offset: number) => {
        const countAttendance = await this.knex.raw(/*SQL*/`SELECT  students_attendance.id AS "students_attendance_id" From "students_attendance"
        INNER JOIN attendance ON students_attendance.attendance_id  = attendance.id
        INNER JOIN lessons ON students_attendance.lesson_id = lessons.id
        INNER JOIN cohorts ON lessons.cohort_id = cohorts.id
        WHERE students_attendance.student_id = (?)
        `, [studentID]);

        const length = countAttendance.rowCount;

        const attendances = await this.knex.raw(/*SQL*/`SELECT students_attendance.id AS "students_attendance_id", students_attendance.student_id, attendance.type ,lessons.description, TO_CHAR(lessons.date_time, 'YYYY-MM-DD HH:MI:SS') AS "date_time", cohorts.name AS "cohort" From "students_attendance"
        INNER JOIN attendance ON students_attendance.attendance_id  = attendance.id
        INNER JOIN lessons ON students_attendance.lesson_id = lessons.id
        INNER JOIN cohorts ON lessons.cohort_id = cohorts.id
        WHERE students_attendance.student_id = (?)
        LIMIT (?)
        OFFSET (?)
        `, [studentID, limit, offset])

        return {
            length: length,
            attendances: attendances.rows
        }


    }

    getStudentAttendance = async (studentID: number, formDate: string, toDate: string, limit: number, offset: number) => {
        const countAttendance = await this.knex.raw(/*SQL*/`SELECT students_attendance.id AS "students_attendance_id" From "students_attendance"
        INNER JOIN attendance ON students_attendance.attendance_id  = attendance.id
        INNER JOIN lessons ON students_attendance.lesson_id = lessons.id
        INNER JOIN cohorts ON lessons.cohort_id = cohorts.id
        WHERE students_attendance.student_id = (?)
        AND lessons."date_time" BETWEEN (?) AND (?)
        `, [studentID, formDate, toDate]);

        const length = countAttendance.rowCount;

        const attendances = await this.knex.raw(/*SQL*/`SELECT students_attendance.id AS "students_attendance_id", students_attendance.student_id, attendance.type ,lessons.description, TO_CHAR(lessons.date_time, 'YYYY-MM-DD HH:MI') AS "date_time", cohorts.name AS "cohort" From "students_attendance"
        INNER JOIN attendance ON students_attendance.attendance_id  = attendance.id
        INNER JOIN lessons ON students_attendance.lesson_id = lessons.id
        INNER JOIN cohorts ON lessons.cohort_id = cohorts.id
        WHERE students_attendance.student_id = (?)
        AND lessons."date_time" BETWEEN (?) AND (?)
        LIMIT (?)
        OFFSET (?)
        `, [studentID, formDate, toDate, limit, offset]);

        return {
            length: length,
            attendances: attendances.rows
        };
    }

    getLeaveApplications = async (studentID: number, limit: number, offset: number) => {
        const leaveCount = await this.knex.raw(/*SQL*/`SELECT leave_apply.id FROM leave_apply
        INNER JOIN cohorts ON leave_apply.cohort_id = cohorts.id
        WHERE leave_apply.student_id = (?)
        `, [studentID])

        const length = leaveCount.rowCount;

        const applications = await this.knex.raw(/*SQL*/`SELECT cohorts.name, TO_CHAR(leave_apply.period_from, 'YYYY-MM-DD') AS "period_from", TO_CHAR(leave_apply.period_to, 'YYYY-MM-DD') AS "period_to", leave_apply.reason, leave_apply.approval FROM leave_apply
        INNER JOIN cohorts ON leave_apply.cohort_id = cohorts.id
        WHERE leave_apply.student_id = (?)
        LIMIT (?)
        OFFSET (?)
        `, [studentID, limit, offset])

        return {
            length: length,
            applications: applications.rows
        }
    }

    getBillingStatus = async (studentId: number, fromDate: string, toDate: string) => {
        const billingStatus = await this.knex.raw(/*SQL*/`SELECT TO_CHAR(payment.date, 'YYYY-MM-DD') AS "billing_date", payment.amount, payment.transaction_number, payment_types.type FROM payment
        INNER JOIN payment_types ON payment.payment_type_id = payment_types.id
        WHERE payment.student_id = (?)
        AND payment.date BETWEEN (?) AND (?);
        `, [studentId, fromDate, toDate])

        return billingStatus.rows
    }

    paymentRecord = async (succeededIntent: SucceededIntent, studentId: number, productId: number) => {
        const trx = await this.knex.transaction();
        try {
            const paymentTypeId = await trx.raw(/*SQL*/`SELECT id FROM "payment_types" WHERE "type" = 'credit card'`)

            await trx('payment').insert([{
                date: new Date(succeededIntent.date * 1000),
                amount: (succeededIntent.amount / 100),
                student_id: studentId,
                payment_type_id: paymentTypeId.rows[0].id,
                product_id: productId,
                transaction_number: succeededIntent.id
            }])

            console.log(`[info]paymentRecord studentId:${studentId}transaction commit`);
            await trx.commit();
            return true;
        } catch (err) {
            console.error(err);
            console.log(`[info]paymentRecord studentId:${studentId}transaction rollback`);
            await trx.rollback();
            return false;
        } finally {
            await trx.destroy();
        }
    }

    updateStudent = async (studentId: number, studentData: Customer, customerId: string) => {
        const trx = await this.knex.transaction();
        try {
            const currentId = await trx('students').where('id', studentId).update({
                first_name: studentData.firstName,
                last_name: studentData.lastName,
                address: studentData.address,
                phone_number: studentData.phone,
                nickname: studentData.nickName,
                stripe_customer_id: customerId
            }).returning('id')

            await trx('students_cohort').insert([{
                students_id: currentId[0],
                cohort_id: parseInt(studentData.cohort),
                quitted: false
            }])

            try {
                const newStudent = (await trx.select(
                    'students.id',
                    'students.first_name',
                    'students.last_name',
                    'students.username',
                    'cohorts.name',
                    'cohorts.start_date'
                ).from('students')
                    .leftJoin('students_cohort', 'students_cohort.students_id', 'students.id')
                    .leftJoin('cohorts', 'students_cohort.cohort_id', 'cohorts.id')
                    .where('students.id', currentId[0]))[0]

                sgMail.setApiKey(process.env.SENDGRID_API_KEY ? process.env.SENDGRID_API_KEY : '');

                const msg = {
                    to: process.env.SENDGRID_EMAIL as string,
                    from: process.env.SENDGRID_EMAIL as string,
                    subject: `New Student of ${newStudent.name} starting on ${moment(newStudent.start_date).format('MMMM Do YYYY')}`,
                    html: `<strong>Student ID: ${newStudent.id}</strong>
            <div>username: ${newStudent.username}<div>
            <div>Last name: ${newStudent.last_name}<div>
            <div>First name: ${newStudent.first_name}<div>`,
                };
                sgMail.send(msg);


                console.log(`Email sent to ${process.env.SENDGRID_EMAIL}`)

            } catch (e) {
                console.error(e)
            }


            console.log(`[info]updateStudent studentId:${studentId}transaction commit`);
            await trx.commit();
            return true
        } catch (err) {
            console.error(err);
            console.log(`[info]updateStudent studentId:${studentId} transaction rollback`);
            await trx.rollback();
            return false
        } finally {
            await trx.destroy();
        }
    }

    updateStudentProducts = async (studentId: number, productId: number, instalment: number, price: number) => {
        const trx = await this.knex.transaction();
        try {
            await trx("students_products").insert([{
                student_id: studentId,
                product_id: productId,
                personal_price: price,
                total_instalment: instalment
            }])

            console.log(`[info]updateStudentProducts studentId:${studentId} transaction commit`);
            await trx.commit();
            return true;
        } catch (err) {
            console.error(err);
            console.log(`[info]updateStudentProducts studentId:${studentId} transaction rollback`);
            await trx.rollback();
            return false;
        } finally {
            await trx.destroy();
        }
    }

    personalStuffs = async (studentId: number) => {
        const cohortResult = await this.knex.raw(/*SQL*/`SELECT students_cohort.cohort_id, cohorts.name, TO_CHAR(cohorts.start_date, 'YYYY-MM-DD') AS "start" FROM students_cohort
        INNER JOIN cohorts ON students_cohort.cohort_id = cohorts.id
        WHERE students_cohort.students_id = (?)
        AND students_cohort.quitted = false
        `, [studentId]);

        const productResult = await this.knex.raw(/*SQL*/`SELECT students_products.product_id, students_products.total_instalment, students_products.personal_price, products.name
        FROM students_products 
        INNER JOIN products ON students_products.product_id = products.id
        WHERE students_products.student_id = (?)
        `, [studentId]);

        const instructorResult = await this.knex.raw(/*SQL*/`SELECT id, username FROM instructors`)

        return {
            cohorts: cohortResult.rows,
            products: productResult.rows,
            instructors: instructorResult.rows
        }
    }

    applyLeave = async (studentId: number, leaveForm: LeaveForm) => {
        const trx = await this.knex.transaction();
        try {
            await trx("leave_apply").insert([{
                student_id: studentId,
                cohort_id: parseInt(leaveForm.cohort),
                period_from: new Date(leaveForm.fromDate),
                period_to: new Date(leaveForm.toDate),
                reason: leaveForm.reason,
                approval: null
            }])

            console.log(`[info]applyLeave studentId:${studentId} transaction commit`);
            await trx.commit();
            return true;
        } catch (err) {
            console.error(err);
            console.log(`[info]applyLeave studentId:${studentId} transaction rollback`);
            await trx.rollback();
            return false;
        } finally {
            await trx.destroy();
        }
    }

    checkCoupon = async (code: string) => {
        const result = await this.knex.raw(/*SQL*/`SELECT discount FROM coupon
        WHERE code = (?)
        `, [code])

        return result.rows;
    }

    getMaterials = async (product_id: number) => {
        const materials = await this.knex.select('id', 'title', 'description', 'link', 'image_path', 'product_id').where('product_id', product_id).from('dashboard');
        return materials;
    }

    checkStudentProduct = async (studentId: number, product_id: number) => {
        const product = await this.knex.raw(/*SQL*/`SELECT id FROM students_products WHERE student_id =(?) AND product_id = (?)`, [studentId, product_id])
        return product.rowCount
    }

    checkProductID = async (product_id: number) => {
        const product = await this.knex.raw(/*SQL*/`SELECT id FROM products_cohorts WHERE product_id = (?)`, [product_id])
        return product.rowCount
    }

    studentEnrollByOtherMethod = async (
        studentId: number,
        address: string,
        cohort: string,
        firstName: string,
        instalment: number,
        lastName: string,
        nickName: string,
        phone: number,
        price: number,
        product: string
    ) => {
        const duplicate = (await this.knex('students_cohort').where({
            students_id: studentId,
            cohort_id: parseInt(cohort),
        }))[0]

        if (duplicate != undefined) {
            return { msg: "You have already been part of this cohort." };
        }

        const trx = await this.knex.transaction();
        try {
            const currentId = await trx('students').where('id', studentId).update({
                first_name: firstName,
                last_name: lastName,
                address: address,
                phone_number: phone,
                nickname: nickName
            }).returning('id')

            await trx('students_cohort').insert([{
                students_id: currentId[0],
                cohort_id: parseInt(cohort),
                quitted: false
            }])

            await trx("students_products").insert([{
                student_id: currentId[0],
                product_id: parseInt(product),
                personal_price: price,
                total_instalment: instalment
            }])

            try {
                const newStudent = (await trx.select(
                    'students.id',
                    'students.first_name',
                    'students.last_name',
                    'students.username',
                    'cohorts.name',
                    'cohorts.start_date'
                ).from('students')
                    .leftJoin('students_cohort', 'students_cohort.students_id', 'students.id')
                    .leftJoin('cohorts', 'students_cohort.cohort_id', 'cohorts.id')
                    .where('students.id', currentId[0]))[0]

                sgMail.setApiKey(process.env.SENDGRID_API_KEY ? process.env.SENDGRID_API_KEY : '');

                const msg = {
                    to: process.env.SENDGRID_EMAIL as string,
                    from: process.env.SENDGRID_EMAIL as string,
                    subject: `New Student of ${newStudent.name} starting on ${moment(newStudent.start_date).format('MMMM Do YYYY')}`,
                    html: `<strong>Student ID: ${newStudent.id}</strong>
            <div>username: ${newStudent.username}<div>
            <div>Last name: ${newStudent.last_name}<div>
            <div>First name: ${newStudent.first_name}<div>
            <div>*****Note: Payment other than credit card</div>`,
                };
                sgMail.send(msg);

                console.log(`[info]updateStudentProducts studentId:${studentId} transaction commit`);


                console.log(`Email sent to ${process.env.SENDGRID_EMAIL}`)

            } catch (e) {
                console.error(e)
            }
            await trx.commit();
            return { msg: "Recevied!" };
        } catch (err) {
            console.error(err);
            console.log(`[info]updateStudentProducts studentId:${studentId} transaction rollback`);
            await trx.rollback();
            return { msg: "Internal Server Error!" };
        } finally {
            await trx.destroy();
        }
    }

}