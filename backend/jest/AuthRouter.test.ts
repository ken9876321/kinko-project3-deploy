import { AuthRouter } from '../routers/AuthRouter';
import { UserService } from '../services/UserService';
jest.mock("express");
jest.mock("node-fetch");


describe('AuthRouter', () => {
    let authRouter: AuthRouter;
    let userService: UserService;
    let req: any;
    let res: any;

    beforeEach(() => {
        userService = {
            getUser: jest.fn((id, username) => [{ id: 1, username: "alexlau@tecky.io" }]),
            getUserByUsername: jest.fn((username) => [{ id: 1, username: "gordonlau@tecky.io" }]),
            createUser: jest.fn((username, gitlabID) => [[{ id: 1, username: "michaelfung@tecky.io" }]])
        } as any

        authRouter = new AuthRouter(userService)
        req = {
            body: {},
            params: {}
        };
        res = {};
        res.status = jest.fn().mockReturnValue(res);
        res.json = jest.fn().mockReturnValue(res);

    })



    it("Call loginGitlab without code", () => {
        authRouter.loginGitlab(req, res);

        expect(res.status).toHaveBeenCalledWith(401);
    })
})
