export interface Stundents {
    id: number;
    username: string;
    first_name: string | null;
    last_name: string | null;
    address: string | null;
    phone_number: string | null;
    nickname: string | null;
    stripe_customer_id: string | null;
    gitlab_id: string;
    create_at: string;
}

export interface Instructors {
    id: number;
    username: string;
    first_name: string | null;
    last_name: string | null;
    phone_number: string | null;
    nickname: string | null;
    gitlab_id: string;
    create_at: string;
}

export interface IUerservice {
    getUser(id: number, username: string): Promise<Stundents[] | Instructors[]>
    getUserByUsername(username: string): Promise<Stundents[] | Instructors[]>
    createUser(username: string, gitlabID: string): Promise<Stundents[] | Instructors[]>
}