import express, { Request, Response } from 'express';
import jwt from '../jwt';
import jwtSimple from 'jwt-simple';
import { UserService } from '../services/UserService';
import fetch from 'node-fetch'

export class AuthRouter {
    constructor(private userService: UserService) { }

    route() {
        const route = express.Router();
        route.post('/gitlab_login', this.loginGitlab);
        return route;
    }

    loginGitlab = async (req: Request, res: Response) => {
        try {
            if (!req.body.code) {
                res.status(401).json({ msg: "No code!" });
                return;
            }
            const { code } = req.body;
            const fetchResponse = await fetch(`https://gitlab.com/oauth/token`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    client_id: process.env.CLIENT_ID,
                    client_secret: process.env.CLIENT_SECRET,
                    code: code,
                    grant_type: 'authorization_code',
                    redirect_uri: process.env.GITLAB_REDIRECT_URI
                })
            });
            const result = await fetchResponse.json();



            if (result.access_token == null || result.access_token == "") {
                res.status(401).json({ message: "Access code not found." })
                return;
            }


            const openidRes = await fetch("http://gitlab.com/oauth/userinfo", {
                headers: {
                    'Authorization': `Bearer ${result.access_token}`
                }
            })
            const openidJson = await openidRes.json();

            let user = await this.userService.getUserByUsername(openidJson.email);
            console.log("user: " + user)

            if (!user) {
                user = await this.userService.createUser(openidJson.email, openidJson.sub);
                console.log("無呢個學生")
            } else {
                console.log("有呢個學生")
            }

            const payload = {
                id: user.id,
                username: user.username
            };

            const token = jwtSimple.encode(payload, jwt.jwtSecret);

            res.json({
                token: token
            })
        } catch (e) {
            res.status(500).json({ msg: "Internal error" })
            console.error(e)
        }
    }
}