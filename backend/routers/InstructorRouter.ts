import express, { Request, Response } from "express";
import { InstructorService } from '../services/InstructorService';
import path from 'path';
import multer from 'multer'
import multerS3 from 'multer-s3'
import aws from "aws-sdk";



const s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: 'ap-southeast-1'
});


const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'student-portal-image',
        metadata: (req,file,cb)=>{
            cb(null,{fieldName: file.fieldname});
        },
        key: (req,file,cb)=>{
            cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
        }
    })
})



export class InstructorRouter {
    constructor(private instructorService: InstructorService) { }

    route() {
        const router = express.Router();
        router.use(/* 放加強器 */ express.static(path.join(__dirname, '../images')))
        router.get('/cohort', this.getCohort);
        router.post('/new_lesson', this.createLesson);
        router.post('/attendance', this.takeAttendance);
        router.post('/lesson_record', this.getLessonRecord);
        router.post('/attendance_record', this.getAttendance);
        router.post('/revise_lesson_name', this.revisedLessonName);
        router.post('/revise_lesson_datetime', this.revisedLessonDateTime);
        router.post('/attendance_report', this.getAttendanceReport);
        router.post('/fetching_materials', this.getMaterials)
        router.post('/materials', upload.single('image'), this.postMaterials)
        router.post('/studentEnroll', this.studentEnroll)
        router.get('/attendance_id', this.getAttendanceId);
        router.delete('/lesson', this.delLesson);
        router.get('/leave_app', this.getLeaveApp);
        router.post('/leave_approval', this.approveLeave);
        router.delete('/leave', this.delLeave);
        router.get('/offer', this.getOffer);
        router.post('/offer', this.addOffer);
        router.delete('/offer', this.delOffer)
        router.post('/cohort', this.addCohort);
        router.post('/product', this.addProduct);
        router.get('/product_cohort', this.getProductCohort);
        router.get('/coupon', this.getCoupon);
        router.post('/coupon', this.addCoupon);
        router.delete('/coupon', this.delCoupon);
        router.delete('/materials', this.deleteMaterials);
        router.post('/student_id', this.getStudentByID);
        router.post('/student_text', this.getStudentByText);
        router.post('/student_info', this.getStudentInfoByID);
        router.post('/quit', this.turnCohortQuit);
        router.post('/payment_insert', this.insertPayment);
        return router;
    }

    getCohort = async (req: Request, res: Response) => {
        try {
            const result = await this.instructorService.getCohort()

            return res.json({ result: result });
        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }


    createLesson = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.createLesson(
                    req.user,
                    req.body.cohortId,
                    req.body.description,
                    req.body.datetime
                )

                return res.json({ result: result })
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    takeAttendance = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.takeAttendance(
                    req.user,
                    req.body.studentId,
                    req.body.checked,
                    req.body.lessonId
                )

                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    getLessonRecord = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const limit = req.body.limit;
                const offset = req.body.offset;

                const result = await this.instructorService.getLessonRecord(
                    req.user,
                    req.body.cohortId,
                    limit,
                    offset
                )

                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    getAttendance = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.getAttendance(
                    req.user,
                    req.body.lessonId
                )

                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    revisedLessonName = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.revisedLessonName(
                    req.user,
                    req.body.lessonId,
                    req.body.newName
                )

                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    revisedLessonDateTime = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.revisedLessonDateTime(
                    req.user,
                    req.body.lessonId,
                    req.body.newDateTime
                )

                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    getAttendanceReport = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.getAttendanceReport(
                    req.user,
                    req.body.cohortId
                )

                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    getAttendanceId = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.getAttendanceId(
                    req.user
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    delLesson = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.delLesson(
                    req.user,
                    req.body.lessonId
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    getLeaveApp = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.getLeaveApp(
                    req.user
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    approveLeave = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.approveLeave(
                    req.user,
                    req.body.leaveId,
                    req.body.approval
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    delLeave = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.delLeave(
                    req.user,
                    req.body.leaveId
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    getOffer = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.getOffer(
                    req.user
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    addOffer = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.addOffer(
                    req.user,
                    req.body.productId,
                    req.body.cohortId
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    delOffer = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.delOffer(
                    req.user,
                    req.body.offerId
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    addCohort = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.addCohort(
                    req.user,
                    req.body.cohortName,
                    req.body.date
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    addProduct = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.addProduct(
                    req.user,
                    req.body.productName,
                    req.body.price
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    getProductCohort = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.getProductCohort(
                    req.user
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    getCoupon = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.getCoupon(
                    req.user
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    addCoupon = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.addCoupon(
                    req.user,
                    req.body.couponName,
                    req.body.code,
                    req.body.discount
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    delCoupon = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.delCoupon(
                    req.user,
                    req.body.couponId,
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    getMaterials = async (req: Request, res: Response) => {
        try {
            if (!req.body.product_id) {
                res.status(400).json({ error: "not product_id provided" })
                return;
            }
            const product_id = Number(req.body.product_id)

            if (isNaN(product_id)) {
                res.status(400).json({ error: "course id does not exist" })
                return;
            }

            const result = await this.instructorService.getMaterials(product_id);
            res.json({ data: result })
        } catch (err) {
            console.error(err);
            res.status(500).json({ error: "Internal Server Error" })
        }
    }

    postMaterials = async (req: Request, res: Response) => {
        try {
            const fileName = (req.file) ? (req.file as any).key : null
            

            if (!req.user?.isInstructor) {
                res.status(401).json({ error: "you are not authenticated to process this action" })
                return;
            }

            console.log(fileName)


            const result = await this.instructorService.postMaterials(req.body.title, req.body.description, req.body.url, fileName, parseInt(req.body.product_id))
            return res.json({ success: result })

        } catch (e) {
            console.error(e);
            return res.status(500).json({ error: "Internal server error" })
        }
    }

    deleteMaterials = async (req: Request, res: Response) => {
        try {
            const materialId = req.body.materialId
            const success = await this.instructorService.deleteMaterials(materialId)

            res.json({ success: success })
        } catch (err) {
            res.status(500).json({ error: "Internal server error" })
        }
    }

    studentEnroll = async (req: Request, res: Response) => {
        try {
            if (req.user?.isInstructor === false) {
                res.status(401).json({ error: "you are not authenticated to process this action" })
                return
            }

            const client = req.body.customer;
            const student = await this.instructorService.getStudent(client.username)

            if (student.length === 0) {
                res.status(400).json({ error: "user not fount" })
                return
            }
            const studentId = student[0].id

            const updateStudentSuccess = await this.instructorService.updateStudent(studentId, client);
            const paymentRecordSuccess = await this.instructorService.paymentRecord(Number(client.price), studentId, parseInt(client.product), client.instalment);
            const studentPrductSuccess = await this.instructorService.updateStudentProducts(studentId, parseInt(client.product), client.instalment, Number(client.price));

            if (!updateStudentSuccess || !paymentRecordSuccess || !studentPrductSuccess) {
                console.log((`insert data success?:
              updateStudentSuccess:${updateStudentSuccess}
              paymentRecordSuccess:${paymentRecordSuccess}
              studentPrductSuccess:${studentPrductSuccess}
              `))

                res.status(401).json({ error: "[internal server error]: enrollment failed, please contact the customer support " })
                return;
            }

            res.json({ succeed: "your enrollment is completed" })
        } catch (err) {
            console.error(err);
            res.status(500).json({ error: "[internal server error]: enrollment failed, please contact the customer support " })
        }

    }

    getStudentByID = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.getStudentByID(
                    req.user,
                    req.body.studentId
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    getStudentByText = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.getStudentByText(
                    req.user,
                    req.body.text
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    getStudentInfoByID = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.getStudentInfoByID(
                    req.user,
                    req.body.studentId
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    turnCohortQuit = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.turnCohortQuit(
                    req.user,
                    req.body.id
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }

    insertPayment = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const result = await this.instructorService.insertPayment(
                    req.user,
                    req.body.studentId,
                    req.body.courseName,
                    req.body.amount,
                    req.body.date
                )
                return res.json(result)
            } else {
                return res.json({ msg: "Please login again!" })
            }

        } catch (e) {
            console.error(e);
            return res.status(500).json({ msg: "Internal server error" })
        }
    }
}