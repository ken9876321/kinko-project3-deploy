import express, { Request, Response } from "express";
import { UserService } from '../services/UserService'

export class UserRouter {

    constructor(private userService: UserService) { }

    route() {
        const router = express.Router();
        router.get('/current', this.getCurrentUser);
        router.post('/setting', this.settingForm);
        return router;
    }

    private getCurrentUser = async (req: Request, res: Response) => {
        try {
            if (req.user?.isInstructor === false) {
                let user = Object.assign({}, req.user)
                const result = await this.userService.checkUserCohort(user.id)
                user["cohortsLength"] = result
                res.json(user);
                return;
            }
            return res.json(req.user);
        } catch (err) {
            console.error(err);
            return res.status(500).json({ error: "Unsuccess! Please contact your instructor." });
        }
    }

    settingForm = async (req: Request, res: Response) => {
        try {
            if (req.user == null) {
                return res.status(401).json({ msg: "Unauthorized" });
            }

            const result = await this.userService.settingForm(
                req.user.username,
                req.body.lastName,
                req.body.firstName,
                req.body.nickname,
                req.body.address,
                req.body.phoneNumber
            )

            return res.json({ result: result, msg: "Success!" });

        } catch (e) {
            console.log(e)
            return res.status(500).json({ msg: "Unsuccess! Please contact your instructor." });
        }

    }
}