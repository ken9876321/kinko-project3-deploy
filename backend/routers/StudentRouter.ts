import express, { Request, Response } from 'express';
import { StudentService } from '../services/StudentService';
import Stripe from 'stripe';



export class StudentRouter {
    // connect to stripe API
    private stripe = new Stripe(`${process.env.STRIPE_SECRET}`, { apiVersion: '2020-03-02', })

    constructor(private studentService: StudentService) { }

    private amountCounter = (price: string, instalment: string): number => {
        const result = Math.round(parseInt(price) / parseInt(instalment));
        return result * 100;
    }

    router() {
        const router = express.Router();
        router.get('/products_cohorts', this.getProductsCohorts);
        router.get('/payment_method', this.getCurrentPaymentMethod)
        router.get('/personalStuffs', this.getPersonalStuffs)
        router.post('/fetching_materials', this.getMaterials)
        router.post('/discounts', this.getDiscounts)
        router.post('/attendances_checking', this.studentAttendance);
        router.post('/all_attendances_checking', this.studentAllAttendance);
        router.post('/billing_status_checking', this.getBillingStatus);
        router.post('/payment', this.handlePayment);
        router.post('/enrollment', this.studentEnroll);
        router.post('/enrollment_others', this.studentEnrollByOtherMethod);
        router.post('/leave', this.applyLeave)
        router.post('/leaves_checking', this.getLeave)
        router.put('/payment_method', this.changePaymentMethod)
        router.delete('/payment_method', this.deletePaymentMethod)

        return router;
    }

    getProductsCohorts = async (req: Request, res: Response) => {
        try {
            const products = await this.studentService.getProducts();
            const cohorts = await this.studentService.getCohorts();

            res.json({
                result: {
                    products: products,
                    cohorts: cohorts
                }
            })
        }
        catch (err) {
            console.error(err);
            res.status(404).json({ error: err })
        }
    }

    getBillingStatus = async (req: Request, res: Response) => {
        try {
            const fromDate = req.body.formDate;
            const toDate = req.body.toDate;


            const result = await this.studentService.getBillingStatus(req.user!.id, fromDate, toDate)

            res.json({ data: result })
        } catch (err) {
            console.error(err);
            res.status(500).json({ error: "Internal Server Error" })
        }
    }

    studentAttendance = async (req: Request, res: Response) => {
        try {
            const fromDate = req.body.formDate;
            const toDate = req.body.toDate;
            const limit = req.body.limit;
            const offset = req.body.offset;

            const result = await this.studentService.getStudentAttendance(req.user!.id, fromDate, toDate, limit, offset)

            res.json({ data: result })
        } catch (err) {
            res.status(500).json({ error: "internal Server ERROR" })
        }

    }

    studentAllAttendance = async (req: Request, res: Response) => {
        try {
            const limit = req.body.limit;
            const offset = req.body.offset;
            const result = await this.studentService.getStAllAttendance(req.user!.id, limit, offset)

            res.json({ data: result })
        } catch (err) {
            res.status(500).json({ error: "internal Server ERROR" })
        }
    }

    getLeave = async (req: Request, res: Response) => {
        try {
            const limit = req.body.limit;
            const offset = req.body.offset;
            const result = await this.studentService.getLeaveApplications(req.user!.id, limit, offset)

            res.json({ data: result })

        } catch (err) {
            res.status(500).json({ error: "internal Server ERROR" })
        }
    }

    handlePayment = async (req: Request, res: Response) => {
        try {
            if (!req.user?.id) {
                res.json({ error: "you are not authenticated to do it." })
                return
            }
            const client = req.body.customer;
            const checking = await this.studentService.checkCohort(req.user?.id, parseInt(client.cohort))

            if (checking.length != 0) {
                res.json({ error: "you can not apply for the same cohort of the course repeatedly" })
                return;
            }

            const customer = await this.stripe.customers.create({
                email: client.email,
                name: client.name,
                address: { line1: client.address }
            })



            const paymentIntent = await this.stripe.paymentIntents.create({
                customer: customer.id,
                setup_future_usage: 'off_session',
                amount: this.amountCounter(client.price, client.instalment),
                receipt_email: client.email,
                currency: 'HKD',
                payment_method_options: { card: { request_three_d_secure: 'any' } }
            });

            // console.log(paymentIntent);
            res.json({
                clientSecret: paymentIntent.client_secret,
                customerId: paymentIntent.customer
            });
        } catch (err) {
            console.error(err);
            // const paymentIntentRetrieved = await this.stripe.paymentIntents.retrieve(err.raw.payment_intent.id);
            // console.log('PI retrieved: ', paymentIntentRetrieved.id);
            res.status(500).json({ error: "fail to create payment, please confirm your input" })
        }
    }

    studentEnroll = async (req: Request, res: Response) => {
        try {
            const client = req.body.customer;
            const succeededPaymentIntent = req.body.payload;
            const customerId = req.body.customerId;


            const updateStudentSuccess = await this.studentService.updateStudent(req.user!.id, client, customerId);
            const paymentRecordSuccess = await this.studentService.paymentRecord(succeededPaymentIntent, req.user!.id, parseInt(client.product));
            const studentPrductSuccess = await this.studentService.updateStudentProducts(req.user!.id, parseInt(client.product), client.instalment, Number(client.price));

            if (!updateStudentSuccess || !paymentRecordSuccess || !studentPrductSuccess) {
                console.log((`insert data success?:
              updateStudentSuccess:${updateStudentSuccess}
              paymentRecordSuccess:${paymentRecordSuccess}
              studentPrductSuccess:${studentPrductSuccess}
              `))
                res.status(401).json({ error: "[server error]: enrollment failed, please contact the customer support " })
                return;
            }

            res.json({ succeed: "your enrollment is completed" })
        } catch (err) {
            console.error(err);
            res.status(500).json({ error: "[server error]: Your payment succedded but the enrollment failed, please contact the customer support " })
        }
    }

    studentEnrollByOtherMethod = async (req: Request, res: Response) => {
        try {
            const { cohort, address, firstName, instalment, lastName, nickName, phone, price, product } = req.body.formData;

            const result = await this.studentService.studentEnrollByOtherMethod
                (
                    req.user!.id,
                    address,
                    cohort,
                    firstName,
                    instalment,
                    lastName,
                    nickName,
                    phone,
                    price,
                    product
                );

            res.json(result)
        } catch (err) {
            console.error(err);
            res.status(500).json({ error: "[server error]: Please contact the customer support " })
        }
    }

    getCurrentPaymentMethod = async (req: Request, res: Response) => {
        try {
            const customerId = req.user?.stripe_customer_id;
            if (customerId === null || customerId === undefined) {
                res.status(404).json({ error: "did not enroll any course by credit card" });
                return;
            }
            //get the oldPaymentMethods List of the customer
            const oldPaymentMethods = (await this.stripe.paymentMethods.list({ customer: customerId!, type: 'card' })).data

            const cardInfo = oldPaymentMethods.map((oldPaymentMethod) => ({
                brand: oldPaymentMethod.card?.brand,
                lastFour: oldPaymentMethod.card?.last4,
                billingEmail: oldPaymentMethod.billing_details.email
            }))

            res.json({ data: cardInfo })
        } catch (err) {
            console.error(err)
            res.status(500).json({ error: err })
        }


    }

    changePaymentMethod = async (req: Request, res: Response) => {
        try {
            const customerId = req.user?.stripe_customer_id
            const paymentMethod = req.body.paymentMethod.paymentMethod;

            const result = (await this.stripe.setupIntents.create({
                usage: "off_session",
                customer: customerId!,
                payment_method: paymentMethod.id,
                payment_method_options: { card: { request_three_d_secure: "any" } }
            })).id

            const confirmResult = await this.stripe.setupIntents.confirm(result!, { payment_method_options: { card: { request_three_d_secure: "any" } } })
            res.json({ clientSecret: confirmResult.client_secret })

        } catch (err) {
            res.status(500).json({ error: err })
        }
    }

    deletePaymentMethod = async (req: Request, res: Response) => {
        try {
            const customerId = req.user?.stripe_customer_id;
            if (customerId === null || customerId === undefined) {
                res.status(404).json({ error: "did not enroll any course by credit card" })
                return;
            }
            const oldPaymentMethods = (await this.stripe.paymentMethods.list({ customer: customerId, type: 'card' })).data

            //Delete the last PaymentMethod from the  customer
            await this.stripe.paymentMethods.detach(oldPaymentMethods[oldPaymentMethods.length - 1].id)


            //set the oldPaymentMethods List of the customer
            const newPaymentMethods = (await this.stripe.paymentMethods.list({ customer: customerId, type: 'card' })).data

            console.log(newPaymentMethods);
            res.json({ success: "success to detach the oldPaymentMethod" })
        } catch (err) {
            res.json({ error: "fail to detach the oldPaymentMethod" })
        }
    }

    getPersonalStuffs = async (req: Request, res: Response) => {
        try {
            if (req.user?.id === undefined || req.user?.id === null) {
                res.status(401).json({ error: "you should login first" })
                return;
            }

            const studentId = req.user?.id
            const result = await this.studentService.personalStuffs(studentId);

            res.json(result);
        } catch (err) {
            console.error(err);
            res.status(500).json({ error: "Internal Server Error" })
        }
    }

    getMaterials = async (req: Request, res: Response) => {
        try {
            if (!req.body.product_id) {
                res.status(400).json({ error: "not product_id provided" })
                return;
            }
            const product_id = Number(req.body.product_id)

            if (isNaN(product_id)) {
                res.status(400).json({ error: "course id does not exist" })
                return;
            }

            const checkStudentProductId = await this.studentService.checkStudentProduct(req.user!.id, product_id)
            if (checkStudentProductId === 0) {
                res.status(400).json({ error: "student dit not enroll this course" })
                return;
            }

            const product = await this.studentService.checkProductID(product_id)

            if (product === 0) {
                res.status(400).json({ error: "course does not exist" })
                return;
            }

            const result = await this.studentService.getMaterials(product_id);
            res.json({ data: result })
        } catch (err) {
            console.error(err);
            res.status(500).json({ error: "Internal Server Error" })
        }
    }

    applyLeave = async (req: Request, res: Response) => {
        try {
            const studentId = req.user?.id
            const leave = req.body.leave;

            const result = await this.studentService.applyLeave(studentId!, leave);

            res.json({ success: result })
        } catch (err) {
            res.status(500).json({ error: "Internal Server Error" })
        }
    }

    getDiscounts = async (req: Request, res: Response) => {
        try {
            const code = req.body.code
            const result = await this.studentService.checkCoupon(code);
            if (result.length === 0) {
                res.json({ error: "the coupon code is invalid" });
                return;
            }

            res.json({ data: result[0] })
        } catch (err) {
            res.status(500).json({ error: "something wrong with the server, please try again later!" })
        }
    }
}