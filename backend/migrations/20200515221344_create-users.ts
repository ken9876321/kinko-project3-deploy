import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable('students', table => {
        table.increments();
        table.string("sid").unique()
        table.string("username").notNullable();
        table.string("first_name")
        table.string("last_name")
        table.text("address");
        table.string("phone_number");
        table.string("nickname");
        table.string("stripe_customer_id");
        table.string("gitlab_id").notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable('instructors', table => {
        table.increments();
        table.string("tid").unique();
        table.string("username").notNullable();
        table.string("first_name");
        table.string("last_name")
        table.string("phone_number");
        table.string("nickname");
        table.string("gitlab_id").notNullable();
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("students");
    await knex.schema.dropTable("instructors");
}

