import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable('payment_types', table=>{
        table.increments();
        table.string("type").notNullable();
    })

    await knex.schema.createTable('payment', table=>{
        table.increments();
        table.date("date").notNullable();
        table.decimal("amount").notNullable();
        table.integer("student_id").notNullable();
        table.foreign("student_id").references("students.id");
        table.integer("payment_type_id").notNullable();
        table.foreign("payment_type_id").references('payment_types.id');
        table.integer("product_id").notNullable();
        table.foreign("product_id").references('products.id');
        table.string("transaction_number");
        table.timestamps(false, true);
    })

}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("payment");
    await knex.schema.dropTable("payment_types");
}

