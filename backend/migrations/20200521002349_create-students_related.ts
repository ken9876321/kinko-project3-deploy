import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable('students_products', table => {
        table.increments();
        table.integer('student_id').notNullable()
        table.foreign('student_id').references('students.id');
        table.integer('product_id').notNullable();
        table.foreign('product_id').references('products.id');
        table.decimal('personal_price').notNullable()
        table.integer('total_instalment').notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable('students_cohort', table => {
        table.increments();
        table.integer('students_id').notNullable();
        table.foreign('students_id').references('students.id');
        table.integer('cohort_id').notNullable();
        table.foreign('cohort_id').references('cohorts.id');
        table.boolean('quitted').notNullable()
        table.timestamps(false, true);
    })

    await knex.schema.createTable('leave_apply', table => {
        table.increments();
        table.integer('student_id').notNullable()
        table.foreign('student_id').references('students.id');
        table.integer('cohort_id').notNullable();
        table.foreign('cohort_id').references('cohorts.id');
        table.integer('instructor_id');
        table.foreign('instructor_id').references('instructors.id')
        table.date('period_from').notNullable()
        table.date('period_to').notNullable();
        table.text('reason').notNullable();
        table.boolean('approval').nullable();

        table.timestamps(false, true);
    })

}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("leave_apply");
    await knex.schema.dropTable("students_cohort");
    await knex.schema.dropTable("students_products");
}

