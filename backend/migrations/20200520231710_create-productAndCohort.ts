import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable('products', table=>{
        table.increments();
        table.string("name").notNullable();
        table.decimal("price").notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable('cohorts', table=>{
        table.increments();
        table.string("name").notNullable();
        table.date("start_date").notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable('products_cohorts', table=>{
        table.increments();
        table.integer("product_id").notNullable();
        table.foreign("product_id").references("products.id")
        table.integer("cohort_id").notNullable();
        table.foreign("cohort_id").references("cohorts.id")
        table.boolean('available').notNullable();
        table.timestamps(false, true);
    })

}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("products_cohorts");
    await knex.schema.dropTable("cohorts");
    await knex.schema.dropTable("products");
}

