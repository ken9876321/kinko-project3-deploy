import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable('dashboard', table => {
        table.increments();
        table.string('title').notNullable()
        table.text('description').notNullable()
        table.text('link').notNullable()
        table.text('image_path').nullable()
        table.integer('product_id').notNullable();
        table.foreign('product_id').references('products.id')
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('dashboard')
}

