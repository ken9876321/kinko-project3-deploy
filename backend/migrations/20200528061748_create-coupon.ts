import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable('coupon', table => {
        table.increments();
        table.string('name').notNullable()
        table.string('code').notNullable().unique();;
        table.decimal('discount').notNullable()
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('coupon');
}

