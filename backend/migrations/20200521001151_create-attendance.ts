import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable('attendance', table => {
        table.increments();
        table.string("type").notNullable();
    })

    await knex.schema.createTable('lessons', table => {
        table.increments();
        table.integer('cohort_id').notNullable();
        table.foreign('cohort_id').references('cohorts.id');
        table.string("description");
        table.dateTime("date_time").notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable('students_attendance', table => {
        table.increments();
        table.integer('student_id').notNullable();
        table.foreign('student_id').references('students.id');
        table.integer('instructor_id').notNullable();
        table.foreign('instructor_id').references('instructors.id');
        table.integer('lesson_id').notNullable();
        table.foreign('lesson_id').references('lessons.id');
        table.integer('attendance_id')
        table.foreign('attendance_id').references('attendance.id')
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("students_attendance");
    await knex.schema.dropTable("lessons");
    await knex.schema.dropTable("attendance");
}

