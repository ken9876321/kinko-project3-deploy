declare global {
    namespace Express {
        interface Request {
            user?: User
        }
    }
}

export interface User {
    id: number;
    username: string;
    last_name: string | null;
    first_name: string | null;
    nickname: string | null;
    phone_number: string | null;
    address: string | null;
    stripe_customer_id: string | null;
    gitlab_id: string | null;
    isInstructor: boolean;
}

export interface Cohort {
    id: number;
    name: string;
    start_date: string;
}

export interface Lesson {
    id: number;
    cohort_id: number;
    description: string;
    date_time: string;
}

export interface Students_cohort {
    id: number;
    student_id: number;
    cohort_id: number;
    quitted: boolean;
}

export interface Attendance {
    id: number;
    type: string;
}

export interface AttendanceReport {
    username: string;
    nickname: string;
    last_name: string;
    first_name: string;
    attended: number;
    absent: number;
    late: number;
    leave: number;
}

export interface LeaveApp {
    username: string;
    nickname: string;
    last_name: string;
    first_name: string;
    period_from: string;
    period_to: string;
    reason: string;
    approval: boolean;
    name: string;
    id: number;
    created_at: string;
}

export interface LeaveAppInstructors {
    username: string;
    nickname: string;
    last_name: string;
    first_name: string;
    id: number;
}

export interface Offer {
    id: number;
    name: string;
    price: number;
    available: boolean;
    cohortName: string;
    startDate: string;
}

export interface Product {
    id: number;
    name: string;
    price: string;
}

export interface Coupon {
    id: number;
    name: string;
    code: string;
    discount: number;
}

export interface StudentOnLeave {
    nickname: string;
    last_name: string;
    first_name: string;
    period_from: string;
    period_to: string;
}

export interface PaymentInfo {
    date: string;
    amount: number;
    type: string;
    id: number;
}

export interface DealInfo {
    product_id: number;
    personal_price: number;
    total_instalment: number;
    name: string;
}

export interface CohortInfo {
    name: string;
    quitted: boolean;
    id: number;
}

export interface StudentInfo {
    id: number;
    username: string;
    first_name: string;
    last_name: string;
    address: string;
    phone_number: string;
    nickname: string;
    stripe_customer_id: string;
    gitlab_id: string;
}