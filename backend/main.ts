import Knex from "knex";
import express from "express";
import bodyParser from 'body-parser';
import cors from 'cors';
import { UserService } from "./services/UserService";
import { AuthRouter } from "./routers/AuthRouter";
import { UserRouter } from "./routers/UserRouter";
import { isLoggedIn } from "./guard";
import { StudentService } from "./services/StudentService";
import { StudentRouter } from "./routers/StudentRouter";
import { InstructorService } from "./services/InstructorService";
import { InstructorRouter } from "./routers/InstructorRouter"


const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);

const userService = new UserService(knex);
const authRouter = new AuthRouter(userService);
const userRouter = new UserRouter(userService);

// kinKo
const studentService = new StudentService(knex);
const studentRouter = new StudentRouter(studentService);

//Jerry
const instructorService = new InstructorService(knex);
const instructorRouter = new InstructorRouter(instructorService);



const isLoggedInGuard = isLoggedIn(userService)

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cors({
    origin: [
        'http://localhost:3000',
        `${process.env.CORS_DOMAIN}`,
    ]
}));

app.use("/images", express.static("images"))
app.use('/auth', authRouter.route());
app.use('/user', isLoggedInGuard, userRouter.route());
app.use('/student', isLoggedInGuard, studentRouter.router());
app.use('/instructor', isLoggedInGuard, instructorRouter.route());

const port = process.env.PORT || 8080;
app.listen(port, () => {
    console.log(`Listening to port ${port}`)
});