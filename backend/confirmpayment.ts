import Knex from "knex";
import Stripe from 'stripe';

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);
const stripe = new Stripe(`${process.env.STRIPE_SECRET}`, { apiVersion: '2020-03-02', })

type Student = {
    students_id: number;
    students_products_id: number;
    stripe_customer_id: string;
    personal_price: string;
    total_instalment: number
    cohort_id: number;
    paid_instalment: string;
}

function calculation(paid_instalment: number, total_instalment: number, price: number) {
    if (paid_instalment === 11) {
        const amount = price - (Math.round(price / total_instalment) * paid_instalment)
        return amount * 100
    } else {
        const amount = Math.round(price / total_instalment);
        return amount * 100
    }
}

async function collectPayment() {
    try {
        const students = await knex('students').join('payment', 'students.id', '=', 'payment.student_id')
            .join('students_products', 'students.id', '=', 'students_products.student_id')
            .join('students_cohort', 'students.id', '=', 'students_cohort.students_id')
            .select('students.id as students_id', 'students_products.product_id as students_products_id', 'students.stripe_customer_id', 'students_products.personal_price', 'students_products.total_instalment',
                'students_cohort.cohort_id'
            )
            .count('payment.id as paid_instalment')
            .groupBy('students_products.id', "students.id", "students_products.personal_price", "students_products.total_instalment", "students_cohort.cohort_id")
            .where('students_cohort.quitted', false)
            .whereNotNull('students.stripe_customer_id')
        console.log(students);

        const paymentType = await knex('payment_types').select().where('type', 'credit card')

        students.map(async (student: Student) => {
            try {

                if (parseInt(student.paid_instalment) === student.total_instalment) {
                    const trx = await knex.transaction();
                    try {
                        const result = await trx('students_cohort').where({
                            students_id: student.students_id,
                            cohort_id: student.cohort_id
                        }).update({ quitted: true }).returning('id')

                        if (result.length === 0) {
                            console.log("[info]target not fount, update quitted transaction rollback");
                            await trx.rollback();
                            return;
                        }

                        console.log("[info]update quitted transaction commit");
                        await trx.commit()
                        return;
                    } catch (err) {
                        console.error(err);
                        console.log('[info]update quitted transaction commit');
                        await trx.rollback();
                        return;
                    } finally {
                        await trx.destroy();
                        return;
                    }
                }

                const paymentMethods = await stripe.paymentMethods.list({
                    customer: student.stripe_customer_id,
                    type: "card"
                })

                const paymentIntent = await stripe.paymentIntents.create({
                    amount: calculation(parseInt(student.paid_instalment), student.total_instalment, parseInt(student.personal_price)),
                    currency: "hkd",
                    customer: student.stripe_customer_id,
                    payment_method: paymentMethods.data[0].id,
                    off_session: true,
                    confirm: true
                })

                if (paymentIntent.status === "succeeded") {
                    console.log(`student_id[${student.students_id}] charged students_products_id[${student.students_products_id}] successfully`)
                } else {
                    return;
                }

                const trx = await knex.transaction();
                try {
                    await trx('payment').insert([{
                        date: new Date(paymentIntent.created * 1000),
                        amount: paymentIntent.amount / 100,
                        student_id: student.students_id,
                        payment_type_id: paymentType[0].id,
                        product_id: student.students_products_id,
                        transaction_number: paymentIntent.id
                    }])

                    console.log("[info]payment transaction commit");
                    await trx.commit()
                } catch (err) {
                    console.error(err);
                    console.log('[info]payment transaction rollback');
                    await trx.rollback();
                } finally {
                    await trx.destroy();
                }
            } catch (err) {
                console.log('Error code is: ', err.code);
                const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(err.raw.payment_intent.id);
                console.log('PI retrieved: ', paymentIntentRetrieved.id);
                return;
            }
        })
    } catch (err) {
        console.error(err);
    }

}

collectPayment()