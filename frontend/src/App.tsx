import React, { useEffect } from 'react';
import { Container } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import UserPage from './UserPage'
import { LoginForm } from './LoginForm';
import { Route,  Switch, Redirect } from 'react-router-dom';
import { GitLabLogin } from './GitlabLogin';
import NavigationBar from './NavigationBar';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from './store';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { restoreLogin } from './redux/auth/thunk';
import { LoadingSpin } from './LoadingSpin';




const stripePromise = loadStripe(`${process.env.REACT_APP_STRIPE_API_KEY}`,{
  locale:'en'
});


function App() {
  const isLogin = useSelector((state: RootState) => state.auth.isAuthenticated)
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(restoreLogin())
  }, [dispatch,isLogin])

  return (
    <div className="App">
      <Container fluid>

        <NavigationBar />
        <Switch>
          <Route path="/" exact>{isLogin? <Redirect to="/welcome" />:<LoadingSpin />}</Route>
          <Route path="/login" exact><LoginForm /></Route>
          <Route path="/login/gitlab">{isLogin? <LoadingSpin />:<GitLabLogin />}</Route>

          {/* <Route path="/course_application">
            <Elements stripe={stripePromise} >
              <ApplicationForm />
            </Elements>
          </Route> */}
          
          
          {isLogin &&
            <Elements stripe={stripePromise} >
              <UserPage />
            </Elements>  
          }
          
          
          {!isLogin && <LoadingSpin /> }
          
        
        </Switch>

      </Container>
    </div>
  );
}

export default App;
