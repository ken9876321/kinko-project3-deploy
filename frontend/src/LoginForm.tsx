import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { restoreLogin } from './redux/auth/thunk';
import { RootState } from './store';
import { Redirect } from 'react-router-dom';
import gitlabLogo from './gitlab-logo-white-rgb.png'
import './LoginForm.scss';
import { resetMessage } from './redux/auth/action';
import loginImage from './homepage.svg'



export function LoginForm() {
    const isLogin = useSelector((oldstate: RootState) => oldstate.auth.isAuthenticated)
    const message = useSelector((state: RootState) => state.auth.message)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(resetMessage())
        dispatch(restoreLogin())
    }, [dispatch, isLogin, message])


    return (
        <div className="login_form">
            <div className="heading">Student Portal</div>
            <a href={`https://gitlab.com/oauth/authorize?client_id=${process.env.REACT_APP_CLIENT_ID}&redirect_uri=${process.env.REACT_APP_FRONTEND_URL}/login/gitlab&response_type=code&scope=${process.env.REACT_APP_SCOPE}`}
                rel="noopener noreferrer"
                id="gitlabLogin"
            ><img src={gitlabLogo} alt="connect to Gitlab" /></a>
            <div className="message">{message}</div>
            <img src={loginImage} alt="loginImage"/>
            {isLogin && <Redirect to="/welcome" />}
        </div>
    )
}