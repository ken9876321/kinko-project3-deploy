import React, { useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
import "./LeaveApp.scss";
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { getLeaveApp, approveLeave, delLeave } from '../redux/instructor/thunk'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faTimes, faTrash } from '@fortawesome/free-solid-svg-icons'






function LeaveApp() {
    const leaveAppWithStudents = useSelector((oldstate: RootState) => oldstate.instructor.leaveAppWithStudents)
    const leaveAppInstructors = useSelector((oldstate: RootState) => oldstate.instructor.leaveAppInstructors)
    const [trigger, setTrigger] = useState(false)
    const dispatch = useDispatch();

    function listLeaveApp(id: number, name: string, cohort: string, from: string, to: string, reason: string, approved: boolean | null, createdTime: string) {
        const fromIndex = from.indexOf("T");
        const formatedFrom = from.slice(0, fromIndex);
        const toIndex = to.indexOf("T");
        const formatedTo = to.slice(0, toIndex);
        const createdTimeIndex = createdTime.indexOf("T");
        const formatedCreatedTime = createdTime.slice(0, createdTimeIndex);
        const hasInstructor = leaveAppInstructors?.find(instructor => instructor.id === id)
        return (
            <Card style={{ width: '100%' }} className="leave_app_card">
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">{cohort}</Card.Subtitle>
                    <Card.Text><label className="bold">Application date:</label>{` ${formatedCreatedTime}`}</Card.Text>
                    <Card.Text><label className="bold">From:</label>{` ${formatedFrom}`}</Card.Text>
                    <Card.Text><label className="bold">To:</label>{` ${formatedTo}`}</Card.Text>
                    <Card.Text><label className="bold">Reason:</label>{` ${reason}`}</Card.Text>
                    {approved === null ? <Card.Text className="leave_app_approval_progress">In the progress</Card.Text> : approved ? <Card.Text className="leave_app_approval_yes">Approved</Card.Text>
                        : hasInstructor?.username && !approved ? <Card.Text className="leave_app_approval_no">Not approved</Card.Text>
                            : <Card.Text className="leave_app_approval_progress">In the progress</Card.Text>}
                    {!hasInstructor?.username && <button className="leave_app_approval_button_yes" onClick={async (e) => {
                        await dispatch(approveLeave(id, true));
                        setTrigger(true);
                    }}><FontAwesomeIcon icon={faCheck} /></button>}
                    {!hasInstructor?.username && <button className="leave_app_approval_button_no" onClick={async (e) => {
                        await dispatch(approveLeave(id, false));
                        setTrigger(true);
                    }}><FontAwesomeIcon icon={faTimes} /></button>}
                    <button className="leave_app_trash" onClick={async (e) => {
                        await dispatch(delLeave(id));
                        setTrigger(true);
                    }}><FontAwesomeIcon icon={faTrash} /></button>
                </Card.Body>
            </Card>
        )
    }

    useEffect(() => {
        dispatch(getLeaveApp())
        setTrigger(false)
    }, [dispatch, trigger])

    return (
        <>
            {leaveAppWithStudents?.map(leaveApp => listLeaveApp(
                leaveApp.id,
                `${leaveApp.nickname}@${leaveApp.first_name} ${leaveApp.last_name}`,
                leaveApp.name,
                leaveApp.period_from,
                leaveApp.period_to,
                leaveApp.reason,
                leaveApp.approval,
                leaveApp.created_at
            ))}
        </>
    )
}

export default LeaveApp;