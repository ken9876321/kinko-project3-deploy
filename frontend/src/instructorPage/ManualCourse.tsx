import React, { useState, useEffect } from 'react'
import { Container, Row, Col, Card, Form, Button, ListGroup } from 'react-bootstrap'
import './ManualCourse.scss';
import { useDispatch, useSelector } from 'react-redux';
import { getOffer, addCohort, addProduct, delOffer, getProductCohort, addOffer, getCoupon, addCoupon, delCoupon } from '../redux/instructor/thunk';
import { RootState } from '../store';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { clearMessge } from '../redux/instructor/action';
import moment from 'moment';
import { useRouteMatch } from 'react-router-dom';
import { push } from 'connected-react-router';

function ManualCourse() {
    const [trigger, setTrigger] = useState(false)
    const [cohortName, setCohortName] = useState('')
    const [cohortDate, setCohortDate] = useState('')
    const [productName, setProductName] = useState('')
    const [productPrice, setProductPrice] = useState('')
    const [productId, setProductId] = useState('')
    const [cohortId, setCohortId] = useState('')
    const [couponName, setCouponName] = useState('')
    const [couponCode, setCouponCode] = useState('')
    const [discount, setDiscount] = useState('')
    const offers = useSelector((oldstate: RootState) => oldstate.instructor.offers);
    const message = useSelector((oldstate: RootState) => oldstate.instructor.message);
    const products = useSelector((oldstate: RootState) => oldstate.instructor.products);
    const cohorts = useSelector((oldstate: RootState) => oldstate.instructor.cohorts);
    const coupon = useSelector((oldstate: RootState) => oldstate.instructor.coupon);
    const dispatch = useDispatch();
    const router = useRouteMatch<{ path: string }>();
    const page = router.params.path

    function pageChanger() {
        return (
            <div className="page_changer">
                <button onClick={() => { dispatch(push("/course_management/existing_offer")) }}>Existing offer</button>
                <button onClick={() => { dispatch(push("/course_management/create_offer")) }}>Create offer</button>
                <button onClick={() => { dispatch(push("/course_management/coupon")) }}>Coupon</button>
                <button onClick={() => { dispatch(push("/course_management/new_cohort")) }}>New cohort</button>
                <button onClick={() => { dispatch(push("/course_management/new_product")) }}>New product</button>
            </div>
        )
    }

    function offerCard(id: number, productName: string, price: number, cohortName: string, available: boolean, startDate: string) {
        const index = startDate.indexOf("T");
        const formateddate = startDate.slice(0, index);
        return (
            <Card key={id} style={{ width: '100%' }} className="manual_course_card">
                <Card.Body>
                    <Card.Title>{productName}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">{cohortName}</Card.Subtitle>
                    <Card.Text><label className="bold">Start date:</label>{` ${formateddate}`}</Card.Text>
                    <Card.Text><label className="bold">Price:</label>{` HKD$${price}`}</Card.Text>
                    <button className="manual_course_trash" onClick={async (e) => {
                        await dispatch(delOffer(id))
                        setTrigger(true);
                    }}><FontAwesomeIcon icon={faTrash} /></button>
                </Card.Body>
            </Card>
        )
    }

    useEffect(() => {
        dispatch(getOffer())
        dispatch(getProductCohort())
        dispatch(getCoupon())
        dispatch(clearMessge())
        setTrigger(false);
    }, [dispatch, trigger, page])

    return (
        <>
            <Container className="manual_course_container">
                <div className="header"><div className="topic">Course Management</div></div>
                {pageChanger()}
                <Row>
                    <Col sm={2}>
                    </Col>
                    <Col sm={8} className="list_background">
                        {page === "existing_offer" && offers?.map(offer => offerCard(
                            offer.id,
                            offer.name,
                            offer.price,
                            offer.cohortName,
                            offer.available,
                            offer.startDate
                        ))}
                        {page === "create_offer" &&
                            <Form onSubmit={async (e: React.FormEvent<HTMLFormElement>) => {
                                e.preventDefault();
                                dispatch(addOffer((parseInt(productId)), (parseInt(cohortId))))
                            }}>
                                <Form.Group controlId="exampleForm.ControlSelect1">
                                    <Form.Label>Product</Form.Label>
                                    <Form.Control as="select" name="product" onChange={(e) => setProductId(e.target.value)}>
                                        <option>Please select</option>
                                        {products?.map(product => <option value={product.id}>{`${product.name} - HKD$${product.price}`}</option>)}
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Cohort</Form.Label>
                                    <Form.Control as="select" name="cohort" onChange={(e) => setCohortId(e.target.value)}>
                                        <option>Please select</option>
                                        {cohorts?.map(cohort => <option value={cohort.id}>{`${cohort.name} - Start: ${moment(cohort.start_date).format('Do MMM YYYY')}`}</option>)}
                                    </Form.Control>
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Add
                                </Button>
                                {message === "Success!" ? <div className="manual_course_message_ok">{message}</div>
                                    : <div className="manual_course_message_deny">{message}</div>}
                            </Form>}
                        {page === "new_cohort" &&
                            <Form onSubmit={async (e: React.FormEvent<HTMLFormElement>) => {
                                e.preventDefault();
                                await dispatch(addCohort(cohortName, cohortDate))
                            }}>
                                <Form.Group>
                                    <Form.Label>Cohort Name</Form.Label>
                                    <Form.Control type="text" onChange={(e) => setCohortName(e.target.value)} required />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Start Date</Form.Label>
                                    <Form.Control type="date" onChange={(e) => setCohortDate(e.target.value)} required />
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Add
                            </Button>
                                {message === "Success!" ? <div className="manual_course_message_ok">{message}</div>
                                    : <div className="manual_course_message_deny">{message}</div>}
                            </Form>}
                        {page === "new_product" &&
                            <Form onSubmit={async (e: React.FormEvent<HTMLFormElement>) => {
                                e.preventDefault();
                                await dispatch(addProduct(productName, (parseFloat(parseFloat(productPrice).toFixed(2)))))
                            }}>
                                <Form.Group>
                                    <Form.Label>Product Name</Form.Label>
                                    <Form.Control type="text" onChange={(e) => setProductName(e.target.value)} required />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Price</Form.Label>
                                    <Form.Control type="number" onChange={(e) => setProductPrice(e.target.value)} required />
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Add
                            </Button>
                                {message === "Success!" ? <div className="manual_course_message_ok">{message}</div>
                                    : <div className="manual_course_message_deny">{message}</div>}
                            </Form>}
                        {page === "coupon" &&
                            <Form style={{ marginBottom: '10px' }} onSubmit={async (e: React.FormEvent<HTMLFormElement>) => {
                                e.preventDefault();
                                await dispatch(addCoupon(couponName, couponCode, (parseFloat(parseFloat(discount).toFixed(2)))))
                                setCouponName('')
                                setCouponCode('')
                                setDiscount('')
                                setTrigger(true)
                            }}>
                                <Card style={{ width: '100%', marginTop: '10px', marginBottom: '10px' }}>
                                    <Card.Header style={{ fontWeight: "bold" }}>Existing coupon</Card.Header>
                                    <ListGroup variant="flush">
                                        {coupon?.map(coupon =>
                                            <ListGroup.Item key={coupon.id} style={{ position: "relative" }}>
                                                {`${coupon.name} - ${coupon.code} - HKD$${coupon.discount}`}
                                                <button className="manual_course_coupon_trash" onClick={async (e) => {
                                                    dispatch(delCoupon(coupon.id))
                                                    setTrigger(true);
                                                }}><FontAwesomeIcon icon={faTrash} /></button>
                                            </ListGroup.Item>)}
                                    </ListGroup>
                                </Card>
                                <Form.Group>
                                    <Form.Label>Coupon Name</Form.Label>
                                    <Form.Control type="text" value={couponName} onChange={(e) => setCouponName(e.target.value)} required />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Code</Form.Label>
                                    <Form.Control type="text" value={couponCode} onChange={(e) => setCouponCode(e.target.value)} required />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Discount</Form.Label>
                                    <Form.Control type="number" value={discount} onChange={(e) => setDiscount(e.target.value)} required />
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Add
                            </Button>
                            </Form>}
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default ManualCourse;