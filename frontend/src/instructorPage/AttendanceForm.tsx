import React, { useEffect, useState } from 'react';
import "./AttendanceForm.scss";
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { useFormState } from 'react-use-form-state';
import moment from 'moment';
import { createLesson, checkAttendance } from '../redux/instructor/thunk';
import { Row, Col, Card, ListGroup } from 'react-bootstrap';
import { useRouteMatch } from 'react-router-dom';


interface IProps {
    chosenId: number;
}

const AttendanceForm: React.FC<IProps> = (props) => {
    const [formState, { text }] = useFormState()
    const curDateTime = moment().format();
    const index = curDateTime.indexOf("+");
    const formatedDateTime = curDateTime.slice(0, index);
    const [chosenDateTime, setChosenDateTime] = useState(formatedDateTime);
    const students = useSelector((oldstate: RootState) => oldstate.instructor.student);
    const message = "All checked!"
    const [countingArray, setCountingArray] = useState([0])
    const [countingControl, setCountingControl] = useState(false)
    const attendanceId = useSelector((oldstate: RootState) => oldstate.instructor.attendanceTypeId);
    const studentOnLeave = useSelector((oldstate: RootState) => oldstate.instructor.studentOnLeave);
    const [submitButton, setsubmitButton] = useState(false);
    const dispatch = useDispatch();
    const router = useRouteMatch<{ lesson: string }>();
    const lesson = router.params.lesson

    function checkbox(id: number, studentName: string, index: number) {

        return (
            <>
                <form key={id} className="checkbox_form">
                    <Row>
                        <Col xs={5} sm={4}>
                            <label className="student_name" title={studentName}>{studentName}</label>
                        </Col>
                        <Col xs={7} sm={8}>
                            <input type="radio" name="attendance" id={"attended" + id} value={attendanceId?.attended} onChange={async (event) => {
                                if (lesson) {
                                    dispatch(checkAttendance(id, parseInt(event.target.value), parseInt(lesson)))
                                    const newArray = revisedArray(countingArray, index)
                                    setCountingArray(newArray)
                                    setCountingControl(counting(countingArray))
                                }
                            }} />
                            <label htmlFor={"attended" + id}>Attended</label>
                            <input type="radio" name="attendance" id={"absent" + id} value={attendanceId?.absent} onChange={async (event) => {
                                if (lesson) {
                                    dispatch(checkAttendance(id, parseInt(event.target.value), parseInt(lesson)))
                                    const newArray = revisedArray(countingArray, index)
                                    setCountingArray(newArray)
                                    setCountingControl(counting(countingArray))
                                }
                            }} />
                            <label htmlFor={"absent" + id}>Absent</label>
                            <input type="radio" name="attendance" id={"late" + id} value={attendanceId?.late} onChange={async (event) => {
                                if (lesson) {
                                    dispatch(checkAttendance(id, parseInt(event.target.value), parseInt(lesson)))
                                    const newArray = revisedArray(countingArray, index)
                                    setCountingArray(newArray)
                                    setCountingControl(counting(countingArray))
                                }
                            }} />
                            <label htmlFor={"late" + id}>Late</label>
                            <input type="radio" name="attendance" id={"leave" + id} value={attendanceId?.leave} onChange={async (event) => {
                                if (lesson) {
                                    dispatch(checkAttendance(id, parseInt(event.target.value), parseInt(lesson)))
                                    const newArray = revisedArray(countingArray, index)
                                    setCountingArray(newArray)
                                    setCountingControl(counting(countingArray))
                                }
                            }} />
                            <label htmlFor={"leave" + id}>Leave</label>
                        </Col>
                    </Row>
                </form>
            </>
        )
    }

    function counting(countingArray: number[]) {
        let count = 0;
        for (let i = 0; i < countingArray.length; i++) {
            if (countingArray[i] === 1) {
                count++
            }
        }
        if (count === countingArray.length) {
            return true;
        } else {
            return false;
        }
    }

    function revisedArray(countingArray: number[], index: number) {
        countingArray[index] = 1
        return countingArray;
    }

    useEffect(() => {
        setsubmitButton(false)
    }, [])

    useEffect(() => {
        setCountingArray(Array(students?.length).fill(0))
    }, [students])

    return (
        <>
            <div className="attendance_form_flex">
                <form className="attendance_form" onSubmit={async (event) => {
                    event.preventDefault();
                    await dispatch(createLesson(props.chosenId, formState.values.description, chosenDateTime))
                    setsubmitButton(true)
                }}>
                    <input type="datetime-local" name="dateTime" value={chosenDateTime} onChange={(e) => setChosenDateTime(e.target.value)} />
                    <input {...text('description')} placeholder="Description" required />
                    <input type="submit" value="Create a new lesson" disabled={submitButton} />
                </form>
            </div>
            <div className="attendance_message">
                {countingControl ? <div>{message}</div> : null}
            </div>

            {studentOnLeave !== null && studentOnLeave.length > 0 ?
                <Card className="attendance_onleave_list" >
                    <Card.Header style={{ fontWeight: "bold" }}>On Leave</Card.Header>
                    <ListGroup variant="flush">
                        {studentOnLeave?.map((student, index) =>
                            <ListGroup.Item key={index}>
                                {`${student.first_name} ${student.last_name} - From: ${moment(student.period_from).format('Do MMM YYYY')} - To: ${moment(student.period_to).format('Do MMM YYYY')}`}
                            </ListGroup.Item>)}
                    </ListGroup>
                </Card> : null}


            {students?.map((student, index) => checkbox(student.id, `${student.nickname}@${student.first_name} ${student.last_name}`, index))}

        </>
    )
}

export default AttendanceForm;