import React, { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import "./Attendance.scss";
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { callCohort, getAttendanceId, getLeaveApp } from '../redux/instructor/thunk'
import { push } from 'connected-react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDoubleLeft } from '@fortawesome/free-solid-svg-icons'
import AttendanceForm from './AttendanceForm';
import { clearLessonInfo } from '../redux/instructor/action';
import AttendanceRecord from './AttendanceRecord';
import AttendanceReport from './AttendanceReport';
import LeaveApp from './LeaveApp';
import { useRouteMatch } from 'react-router-dom';



function Attendance() {
    const cohorts = useSelector((oldstate: RootState) => oldstate.instructor.cohort)
    const [chosenId, setChosenId] = useState(0)
    // const [page, setPage] = useState("record")
    const cohortName = cohorts?.find(cohort => cohort.id === chosenId)?.name
    const dispatch = useDispatch();
    const instructors = useSelector((oldstate: RootState) => oldstate.instructor.leaveAppInstructors);
    const inTheProgress = instructors?.filter(hasInstructor => hasInstructor.username === null);
    const router = useRouteMatch<{ path: string, cohort: string }>();
    const page = router.params.path
    const leaveApp = router.params.cohort

    function pageChanger() {
        return (
            <div className="page_changer">
                <button onClick={() => {
                    dispatch(push(`/attendance/${cohortName}/record`))
                }}>Record</button>
                <button onClick={() => {
                    dispatch(push(`/attendance/${cohortName}/new_lesson`))
                }}>New lesson</button>
            </div>
        )
    }

    useEffect(() => {
        dispatch(callCohort())
        dispatch(clearLessonInfo())
        dispatch(getAttendanceId())
        dispatch(getLeaveApp())
    }, [dispatch, chosenId, page])

    return (
        <>
            <Container className="attendance_container">
                <div className="header">
                    <div className="topic">{chosenId === 0 ? "Cohorts" : cohortName}</div>
                    <div className="button_background">
                        {chosenId === 0 ? <button className="leave_approve" onClick={() => {
                            dispatch(push(`/attendance/leave_app`))
                        }}>Leave apply</button>
                            : <button className="leave_approve" onClick={() => {
                                dispatch(push(`/attendance/${cohortName}/report`))
                            }}>Report</button>}
                        {chosenId === 0 && inTheProgress !== undefined && inTheProgress!.length > 0 ? <div className="leave_app_alert">{inTheProgress?.length}</div> : null}
                    </div>
                </div>
                {chosenId === 0 ? null : pageChanger()}
                <Row>
                    <Col sm={2}>
                    </Col>
                    <Col sm={8} className="list_background">

                        <div className="flex">
                            {!page && leaveApp !== "leave_app" ?
                                cohorts?.map(cohort => <div className="cohort" key={cohort.id} onClick={() => {
                                    setChosenId(cohort.id)
                                    dispatch(push(`/attendance/${cohort.name}/record`))
                                }}>{cohort.name}</div>)
                                : leaveApp === "leave_app" && !page ? <LeaveApp />
                                    : page === "record" && leaveApp !== "leave_app" ? <AttendanceRecord chosenId={chosenId} />
                                        : page === "new_lesson" && leaveApp !== "leave_app" ? <AttendanceForm chosenId={chosenId} />
                                            : <AttendanceReport chosenId={chosenId} />}
                        </div>
                    </Col>
                </Row>
                {!leaveApp ? null :
                    <button className="attendance_back_button" onClick={() => {
                        setChosenId(0);
                        dispatch(push(`/attendance`))
                    }} >
                        <FontAwesomeIcon icon={faAngleDoubleLeft} /></button>}
                {!leaveApp && leaveApp === "leave_app" ?
                    <button className="attendance_back_button" onClick={() => dispatch(push(`/attendance`))} >
                        <FontAwesomeIcon icon={faAngleDoubleLeft} /></button> : null}
            </Container>
        </>
    )
}

export default Attendance;