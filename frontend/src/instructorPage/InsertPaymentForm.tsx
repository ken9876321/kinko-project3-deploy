import React, { useEffect, useState, useCallback } from 'react';
import "./InsertPaymentForm.scss";
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { useFormState } from 'react-use-form-state';
import moment from 'moment';
import { getStudentByID, getStudentByText, insertPayment } from '../redux/instructor/thunk';
import { Row, Col, Container } from 'react-bootstrap';
import { useRouteMatch } from 'react-router-dom';
import { push } from 'connected-react-router';
import { clearMessge } from '../redux/instructor/action';



function InsertPaymentForm() {
    const [formState, { text, number }] = useFormState()
    const [studentId, setStudentId] = useState('')
    const [studentName, setStudentName] = useState('')
    const studentFound = useSelector((oldstate: RootState) => oldstate.instructor.studentInfo);
    const message = useSelector((oldstate: RootState) => oldstate.instructor.message);
    const curDateTime = moment().format();
    const index = curDateTime.indexOf("+");
    const formatedDateTime = curDateTime.slice(0, index);
    const [chosenDateTime, setChosenDateTime] = useState(formatedDateTime);
    const dispatch = useDispatch();
    const router = useRouteMatch<{ studentId: string }>();
    const id = router.params.studentId

    function checkInput() {
        if (studentId === '' || studentName === '') {
            dispatch(push('/payment_insert/'))
        }
    }

    const checkInputCallback = useCallback(checkInput, [studentId, studentName])

    function form() {
        if (id) {
            return (
                <form className="insert_payment_form" onSubmit={async (e) => {
                    e.preventDefault();
                    await dispatch(insertPayment(parseInt(id), formState.values.course, formState.values.amount, chosenDateTime))
                    formState.reset();
                }}>
                    <input type="datetime-local" name="date" value={chosenDateTime} onChange={(e) => setChosenDateTime(e.target.value)} />
                    <input {...text('course')} placeholder="Course Name (Please enter the exact name)" required />
                    <input {...number('amount')} placeholder="Amount" required />
                    <input type="submit" value="Submit"></input>
                </form>
            )
        }
    }

    useEffect(() => {
        dispatch(getStudentByID(parseInt(studentId)))
        checkInputCallback()
    }, [dispatch, studentId, checkInputCallback])

    useEffect(() => {
        dispatch(getStudentByText(studentName))
        checkInputCallback()
    }, [dispatch, studentName, checkInputCallback])

    useEffect(() => {
        dispatch(clearMessge())
    }, [dispatch])

    return (
        <>
            <Container>
                <Row>
                    <Col sm={2}></Col>
                    <Col sm={8}>
                        <h3 className="insert_payment_heading">Payment Insert</h3>
                        <div className="insert_payment_search">
                            <input type='text' placeholder="Search by ID" onChange={async (e) => {
                                await setStudentId(e.target.value)
                            }}></input>
                            <input type='text' placeholder="Search by name" onChange={async (e) => {
                                await setStudentName(e.target.value)
                            }}></input>
                            {studentFound?.map((student, id) =>
                                <div className="insert_payment_student" key={id} onClick={async () => {
                                    dispatch(push(`/payment_insert/${student.id}`))
                                }}>
                                    {`${student.nickname}@${student.first_name} ${student.last_name}`}</div>
                            )}

                            {form()}
                            {message ? <div className="insert_payment_message">{message}</div> : null}
                        </div>

                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default InsertPaymentForm;