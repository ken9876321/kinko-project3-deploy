import React, { useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';
import "./AttendanceReport.scss";
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { getAttendanceReport } from '../redux/instructor/thunk'
import 'moment-timezone';




interface IProps {
    chosenId: number;
}

const AttendanceReport: React.FC<IProps> = (props) => {
    const reports = useSelector((oldstate: RootState) => oldstate.instructor.attendanceReport)
    const dispatch = useDispatch()



    useEffect(() => {
        dispatch(getAttendanceReport(props.chosenId))
    }, [dispatch, props.chosenId])

    return (
        <>
            <Row className="attendance_report">
                <Col xs="auto" sm={4}>
                    <div className="attendance_report_col">Name</div>
                    {reports?.map(report => <div className="attendance_report_data" title={`${report.nickname}@${report.first_name} ${report.last_name}`}>{`${report.nickname}@${report.first_name} ${report.last_name}`}</div>)}
                </Col>
                <Col xs={2} sm={2}>
                    <div className="attendance_report_col">Attend</div>
                    {reports?.map(report => {
                        if (report.attended >= 80) {
                            return (<div className="attendance_report_attended1">{`${report.attended}%`}</div>)
                        } else if (report.attended >= 70 && report.attended < 80) {
                            return (<div className="attendance_report_attended2">{`${report.attended}%`}</div>)
                        } else if (report.attended >= 50 && report.attended < 70) {
                            return (<div className="attendance_report_attended3">{`${report.attended}%`}</div>)
                        } else {
                            return (<div className="attendance_report_attended4">{`${report.attended}%`}</div>)
                        }
                    }
                    )}
                </Col>
                <Col xs={2} sm={2}>
                    <div className="attendance_report_col">Absent</div>
                    {reports?.map(report => <div className="attendance_report_data">{`${report.absent}%`}</div>)}
                </Col>
                <Col xs={2} sm={2}>
                    <div className="attendance_report_col">Late</div>
                    {reports?.map(report => <div className="attendance_report_data">{`${report.late}%`}</div>)}
                </Col>
                <Col xs={2} sm={2}>
                    <div className="attendance_report_col">Leave</div>
                    {reports?.map(report => <div className="attendance_report_data">{`${report.leave}%`}</div>)}
                </Col>
            </Row>
        </>
    )
}

export default AttendanceReport;