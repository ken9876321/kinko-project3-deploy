import React, { useState, useEffect, useCallback } from 'react'
import { Container, Row, Col, Card, ListGroup } from 'react-bootstrap'
import './StudentInfo.scss';
import { useDispatch, useSelector } from 'react-redux';
import { RootState, ThunkDispatch } from '../store';
import moment from 'moment';
import { useRouteMatch } from 'react-router-dom';
import { push } from 'connected-react-router';
import { getStudentByID, getStudentByText, getStudentInfoById, turnCohortQuit } from '../redux/instructor/thunk';
import { confirmAlert } from 'react-confirm-alert';

function StudentInfo() {
    const [studentId, setStudentId] = useState('')
    const [studentName, setStudentName] = useState('')
    const [trigger, setTrigger] = useState(true);
    const studentFound = useSelector((oldstate: RootState) => oldstate.instructor.studentInfo);
    const deals = useSelector((oldstate: RootState) => oldstate.instructor.dealInfo);
    const cohorts = useSelector((oldstate: RootState) => oldstate.instructor.cohortInfo);
    const dispatch = useDispatch();
    const router = useRouteMatch<{ studentId: string }>();
    const id = router.params.studentId
    const student = studentFound?.find(student => student.id === parseInt(id))

    function checkInput() {
        if (studentId === '' || studentName === '') {
            dispatch(push('/student_info/'))
        }
    }

    const checkInputCallback = useCallback(checkInput, [studentId, studentName])

    function submit(action: (dispatch: ThunkDispatch, getState: () => RootState) => Promise<void>) {
        confirmAlert({
            title: 'Confirm to change',
            message: `Are you sure to change the Quit status?`,
            buttons: [
                {
                    label: 'Yes',
                    onClick: async () => {
                        await dispatch(action)
                        setTrigger(false)
                    }
                },
                {
                    label: 'No',
                    onClick: () => null
                }
            ]
        });
    };

    useEffect(() => {
        dispatch(getStudentByID(parseInt(studentId)))
        checkInputCallback()
    }, [dispatch, studentId, checkInputCallback])

    useEffect(() => {
        dispatch(getStudentByText(studentName))
        checkInputCallback()
    }, [dispatch, studentName, checkInputCallback])

    useEffect(() => {
        dispatch(getStudentInfoById(parseInt(id)))
        setTrigger(true)
    }, [dispatch, id, trigger])

    return (
        <>
            <Container>
                <Row>
                    <Col sm={2} className="info_search">
                        <div className="heading">Student Info</div>
                        <input type='text' placeholder="Search by ID" onChange={async (e) => {
                            await setStudentId(e.target.value)
                        }}></input>
                        <input type='text' placeholder="Search by name" onChange={async (e) => {
                            await setStudentName(e.target.value)
                        }}></input>

                        {studentFound?.map((student, id) =>
                            <div className="info_search_student" key={id} onClick={async () => {
                                dispatch(push(`/student_info/${student.id}`))
                            }}>
                                {`${student.nickname}@${student.first_name} ${student.last_name}`}</div>
                        )}
                    </Col>
                    <Col sm={3}>
                        {id !== undefined &&
                            <Card className="info_student_info">
                                <Card.Header><label className="column">ID:</label>{` ${student?.id}`}</Card.Header>
                                <ListGroup variant="flush">
                                    <ListGroup.Item><label className="column">Username:</label>{` ${student?.username}`}</ListGroup.Item>
                                    <ListGroup.Item><label className="column">Last name:</label>{` ${student?.last_name}`}</ListGroup.Item>
                                    <ListGroup.Item><label className="column">First name:</label>{` ${student?.first_name}`}</ListGroup.Item>
                                    <ListGroup.Item><label className="column">Nickname:</label>{` ${student?.nickname}`}</ListGroup.Item>
                                    <ListGroup.Item><label className="column">Address:</label>{` ${student?.address}`}</ListGroup.Item>
                                    <ListGroup.Item><label className="column">Phone No.:</label>{` ${student?.phone_number}`}</ListGroup.Item>
                                    <ListGroup.Item><label className="column">Gitlab ID:</label>{` ${student?.gitlab_id}`}</ListGroup.Item>
                                    <ListGroup.Item><label className="column">Stripe ID:</label>{` ${student?.stripe_customer_id}`}</ListGroup.Item>
                                </ListGroup>
                            </Card>
                        }
                    </Col>
                    <Col sm={3}>
                        {id !== undefined &&
                            <Card className="info_student_info">
                                <Card.Header><label className="column">Cohort</label></Card.Header>
                                <ListGroup variant="flush">
                                    {cohorts?.map(cohort =>
                                        <ListGroup.Item>{`${cohort.name} `}{cohort.quitted ?
                                            <label className="quitted" onClick={() => {
                                                submit(turnCohortQuit(cohort.id))
                                            }}>Quitted</label>
                                            : <label className="not_quitted" onClick={() => {
                                                submit(turnCohortQuit(cohort.id))
                                            }}>Remain</label>}
                                        </ListGroup.Item>)}
                                </ListGroup>
                            </Card>
                        }
                    </Col>
                    <Col sm={4}>
                        {id !== undefined ?
                            deals?.map(deal =>
                                <Card className="info_student_info">
                                    <Card.Body>
                                        <Card.Title>{deal.name}</Card.Title>
                                        <Card.Subtitle className="price">{`HKD$${deal.personal_price}`}</Card.Subtitle>
                                        <Card.Text><label className="column">Total instalment:</label>{` ${deal.total_instalment}`}</Card.Text>
                                        <Card.Text><label className="column">Balance:</label>{` HKD$${deal.balance}`}</Card.Text>
                                        <Card.Text><label className="column">Instalment left:</label>{` ${deal.instalmentLeft}`}</Card.Text>
                                        <Card.Text><label className="column">Transactions:</label></Card.Text>
                                        {deal.payment.map(payment =>
                                            <ListGroup.Item>{`${moment.tz(payment.date, "Hongkong").format('Do MMMM YYYY')} - HKD$${payment.amount} (${payment.type})`}</ListGroup.Item>
                                        )}

                                    </Card.Body>
                                </Card>
                            )
                            : null
                        }
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default StudentInfo;