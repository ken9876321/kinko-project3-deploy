import React, { useEffect, useState, useCallback } from 'react';
import { Row, Col } from 'react-bootstrap';
import "./AttendanceRecord.scss";
import { useSelector, useDispatch } from 'react-redux';
import { RootState, ThunkDispatch } from '../store';
import { getAttendance, checkAttendance, revisedLessonName, revisedLessonDateTime, delLesson } from '../redux/instructor/thunk'
import moment from 'moment';
import 'moment-timezone';
import { clearMessge, setLessonRecord, setMessge } from '../redux/instructor/action';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import ReactPaginate from 'react-paginate';


interface IProps {
    chosenId: number;
}

const AttendanceRecord: React.FC<IProps> = (props) => {
    const lessons = useSelector((oldstate: RootState) => oldstate.instructor.lessonRecord)
    const students = useSelector((oldstate: RootState) => oldstate.instructor.studentAttandance)
    const message = useSelector((oldstate: RootState) => oldstate.instructor.message)
    const attendanceId = useSelector((oldstate: RootState) => oldstate.instructor.attendanceTypeId);
    const token = useSelector((oldstate: RootState) => oldstate.auth.token);
    const [chosenDateTime, setChosenDateTime] = useState('');
    const [page, setPage] = useState(true)
    const dispatch = useDispatch();
    const [lesson, setLesson] = useState("")
    const [totalPage, setTotalPage] = useState(0);
    const perPage = 10

    function findLessonName() {
        if (students && lessons) {
            if (students[0]) {
                const lesson = lessons.find(lesson => lesson.id === students[0].lesson_id)
                if (lesson?.description) {
                    setLesson(lesson.description)
                }
            }
        }
    }

    function findLessonDateTime() {
        if (students && lessons) {
            if (students[0]) {
                const lesson = lessons.find(lesson => lesson.id === students[0].lesson_id)
                if (lesson?.date_time) {
                    const localtime = moment.tz(lesson.date_time, "Hongkong").format()
                    const index = localtime.indexOf("+");
                    const formatedDateTime = localtime.slice(0, index);
                    setChosenDateTime(formatedDateTime)
                }
            }
        }
    }

    const getlessonRecord = useCallback(async (cohortId: number, limit: number, offset: number) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/lesson_record`, {
            method: "post",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                cohortId: cohortId,
                limit: limit,
                offset: offset
            })
        })

        const json = await res.json();
        if (json.result) {
            dispatch(setLessonRecord(json.result))
            setTotalPage(Math.ceil(json.length / limit))
        } else {
            dispatch(setMessge(json.msg))
            return
        }
    }, [dispatch, token])

    const handlePageClick = useCallback((data) => {
        let selected = data.selected;
        let offset = Math.ceil(selected * perPage)
        getlessonRecord(props.chosenId, perPage, offset)
    }, [props.chosenId, perPage, getlessonRecord])

    const findLessonNameCallback = useCallback(findLessonName, [students])
    const findLessonDateTimeCallback = useCallback(findLessonDateTime, [students])

    function submit(lessonName: string, action: (dispatch: ThunkDispatch, getState: () => RootState) => Promise<void>) {
        confirmAlert({
            title: 'Confirm to delete',
            message: `Are you sure to delete ${lessonName}?`,
            buttons: [
                {
                    label: 'Yes',
                    onClick: async () => {
                        await dispatch(action)
                        await getlessonRecord(props.chosenId, perPage, 0)
                    }
                },
                {
                    label: 'No',
                    onClick: () => null
                }
            ]
        });
    };

    // function lessonListTableVer(){
    //     if(page){
    //         return(
    //             <Table striped bordered hover size="sm">
    //             <thead>
    //                 <tr>
    //                     <th>Lesson</th>
    //                     <th>Date Time</th>
    //                     <th>Delete Option</th>
    //                 </tr>
    //             </thead>

    //             <tbody>
    //                 {lessons?.map((lesson, index) =>
    //                     <tr key={index}>
    //                         <td>{<div className="attendance_record_desc" title={lesson.description} onClick={async () => {
    //                             await dispatch(getAttendance(lesson.id))
    //                             setPage(false)
    //                             dispatch(clearMessge())
    //                         }}>
    //                             {lesson.description}</div>}</td>
    //                         <td>{<div className="attendance_record_datetime" onClick={async () => {
    //                             await dispatch(getAttendance(lesson.id))
    //                             setPage(false)
    //                             dispatch(clearMessge())
    //                         }}>{moment(lesson.date_time).format('MMMM Do YYYY, h:mm:ss a')}</div>}</td>
    //                         <td>{<button className="attendance_record_delbutton" onClick={async () => {
    //                             submit(`${lesson.description}`, delLesson(lesson.id))

    //                         }}><FontAwesomeIcon icon={faTimes} /></button>}</td>
    //                     </tr>
    //                 )}
    //             </tbody>
    //         </Table>
    //         )
    //     }
    // }

    function lessonList() {
        if (page) {
            return (
                <Row className="attendance_record">
                    <Col sm={1}>
                    </Col>
                    <Col xs={4} sm={4}>
                        {lessons?.map(lesson => <div className="attendance_record_desc" title={lesson.description} onClick={async () => {
                            await dispatch(getAttendance(lesson.id))
                            setPage(false)
                            dispatch(clearMessge())
                        }}>
                            {lesson.description}</div>)}
                    </Col>
                    <Col xs={7} sm={6}>
                        {lessons?.map(lesson => <div className="attendance_record_datetime" onClick={async () => {
                            await dispatch(getAttendance(lesson.id))
                            setPage(false)
                            dispatch(clearMessge())
                        }}>{moment(lesson.date_time).format('MMMM Do YYYY, h:mm:ss a')}</div>)}
                    </Col>
                    <Col xs={1} sm={1}>
                        {lessons?.map(lesson => <button className="attendance_record_delbutton" onClick={async () => {
                            submit(`${lesson.description}`, delLesson(lesson.id))

                        }}><FontAwesomeIcon icon={faTimes} /></button>)}
                    </Col>

                </Row>
            )
        }
    }

    useEffect(() => {
        getlessonRecord(props.chosenId, perPage, 0)
        findLessonNameCallback()
        findLessonDateTimeCallback()
    }, [dispatch, props.chosenId, findLessonNameCallback, findLessonDateTimeCallback, perPage, getlessonRecord, page])

    return (
        <>
            {lessonList()}
            <div className="record_lesson_heading">
                {!page && <button onClick={() => setPage(true)}>{"Back"}</button>}
                {!page && <input value={lesson} onChange={(e) => {
                    setLesson(e.target.value);
                    if (students) {
                        dispatch(revisedLessonName(students[0].lesson_id, e.target.value))
                    }
                }} />}
                {!page && <input type="datetime-local" name="dateTime" value={chosenDateTime} onChange={async (e) => {
                    setChosenDateTime(e.target.value)
                    if (students) {
                        dispatch(revisedLessonDateTime(students[0].lesson_id, e.target.value))
                    }
                }

                } />}
                {!page && message && <div className="record_message">{message}</div>}
            </div>

            {!page && students?.map(student => {
                return (
                    <form className="attendance_checkbox_form">
                        <Row>
                            <Col xs={12} sm={3}>
                                <label className="student_name" title={`${student.nickname}@${student.first_name} ${student.last_name}`}>{`${student.nickname}@${student.first_name} ${student.last_name}`}</label>
                            </Col>
                            <Col xs={3} sm={2}>
                                {student.type === "attended" ? <label className="record_attended">{student.type}</label>
                                    : student.type === "absent" ? <label className="record_absent">{student.type}</label>
                                        : student.type === "late" ? <label className="record_late">{student.type}</label>
                                            : <label className="record_leave">{student.type}</label>}
                            </Col>
                            <Col xs={7} sm={7}>

                                <input type="radio" name="attendance" id={"attended" + student.id} value={attendanceId?.attended} onChange={async (event) => {
                                    dispatch(checkAttendance(student.id, parseInt(event.target.value), student.lesson_id))
                                }} />
                                <label htmlFor={"attended" + student.id}>Attended</label>
                                <input type="radio" name="attendance" id={"absent" + student.id} value={attendanceId?.absent} onChange={async (event) => {
                                    dispatch(checkAttendance(student.id, parseInt(event.target.value), student.lesson_id))
                                }} />
                                <label htmlFor={"absent" + student.id}>Absent</label>
                                <input type="radio" name="attendance" id={"late" + student.id} value={attendanceId?.late} onChange={async (event) => {
                                    dispatch(checkAttendance(student.id, parseInt(event.target.value), student.lesson_id))
                                }} />
                                <label htmlFor={"late" + student.id}>Late</label>
                                <input type="radio" name="attendance" id={"leave" + student.id} value={attendanceId?.leave} onChange={async (event) => {
                                    dispatch(checkAttendance(student.id, parseInt(event.target.value), student.lesson_id))
                                }} />
                                <label htmlFor={"leave" + student.id}>Leave</label>
                            </Col>
                        </Row>
                    </form>
                )
            })}

            {page && <div id="instructorRecordPaginate">
                <ReactPaginate
                    previousLabel={'previous'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    pageCount={totalPage}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageClick}
                    containerClassName={'pagination'}
                    activeClassName={'active'}
                />
            </div>}
        </>
    )
}

export default AttendanceRecord;