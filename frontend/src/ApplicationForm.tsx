import React, { useState, useCallback, useEffect } from 'react';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { useFormState } from 'react-use-form-state';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from './store';
import { Container, Row, Col, Form } from 'react-bootstrap';
import './ApplicationForm.css'
import { fetchProductsCohorts, enrollByOtherMethod, goToHomePage } from './redux/student/thunk';
import { clearMessage } from './redux/student/action';




// import { getClientSecret } from './Redux/client/action';

export default function ApplicationForm() {
    const [creditCard, setCreditCard] = useState(true);
    const [succeeded, setSucceeded] = useState(false);
    const [error, setError] = useState('');
    const [processing, setProcessing] = useState(false);
    const [disabled, setDisabled] = useState(true);
    const [price, setPrice] = useState(0);
    const [date, setDate] = useState(0);
    const [cardDisabled, setCardDisabled] = useState(false);
    const [enrollmentSucceeded, setEnrollmentSucceeded] = useState(false);
    const [discount, setDiscount] = useState(0);
    const [couponMessage, setCouponMessage] = useState('');
    const [formState, { text, email, select, number }] = useFormState({
        instalment: 12
    })
    const stripe = useStripe();
    const elements = useElements();
    const card = elements?.getElement(CardElement);
    const dispatch = useDispatch();
    const productsInfo = useSelector((state: RootState) => state.student.productsInfo);
    const cohortsInfo = useSelector((state: RootState) => state.student.cohortsInfo);
    const token = useSelector((oldstate: RootState) => oldstate.auth.token)
    const message = useSelector((state: RootState) => state.student.message);

    useEffect(() => {
        dispatch(fetchProductsCohorts());
    }, [dispatch, token])

    useEffect(() => {
        const currentProduct = productsInfo.find((product) => product.product_id === price)
        if (currentProduct !== undefined) {
            formState.setField('price', (discount === 0) ? currentProduct.price : currentProduct.price - discount);
            return;
        }
    }, [price, productsInfo, discount, formState])

    const displayPrice = useCallback(() => {
        const currentProduct = productsInfo.find((product) => product.product_id === price)
        const originalPrice = currentProduct?.price
        if (originalPrice) {
            if (discount === 0) {
                return <h5>${originalPrice}</h5>;
            } else {
                return (<h5><del>${originalPrice}</del><span style={{ color: "red" }}>${originalPrice - discount}</span></h5>)
            }
        }
    }, [discount, price, productsInfo])

    const fetchCoupon = useCallback(async (code: string) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/discounts`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    code: code
                }),
        })

        const result = await res.json();
        if (result.error) {
            setCouponMessage(result.error)
            setDiscount(0);
            return;
        }

        const discount = Number(result.data.discount);
        setDiscount(discount)
        setCouponMessage(`You got the discount ${discount} HKD`)
    }, [token])

    //********handle the payment process*******////
    const CreatePaymentIntent = useCallback(async function () {
        const formData = formState.values;

        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/payment`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    customer: formData
                }),
        })

        const data = await res.json()

        if (data.error) {
            setError(`Payment failed: ${data.error}`);
            setProcessing(false);
            setSucceeded(false);
            setCardDisabled(false);
            return
        }
        const payload = await stripe?.confirmCardPayment(data.clientSecret, {
            payment_method: {
                card: card!,
                billing_details: {
                    name: formState.values.nickName,
                    email: formState.values.email,
                    address: formState.values.address
                }
            }
        })

        if (payload?.error) {
            setError(`Payment failed ${payload.error.message}`);
            setProcessing(false);
            setSucceeded(false);
            setCardDisabled(false);
            return;
        }
        card?.clear();
        formState.reset()
        setPrice(0);
        setDate(0);
        setError('');
        setProcessing(false);
        setSucceeded(true);
        setCardDisabled(false);

        const enroll = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/enrollment`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    customer: formData,
                    payload: {
                        id: payload?.paymentIntent?.id,
                        amount: payload?.paymentIntent?.amount,
                        date: payload?.paymentIntent?.created
                    },
                    customerId: data.customerId
                }),
        })

        const enrollResult = await enroll.json();

        if (enrollResult.error) {
            setEnrollmentSucceeded(false);
            setSucceeded(false);
            setError(enrollResult.error);
            return;
        }
        setEnrollmentSucceeded(true);
        dispatch(goToHomePage());
        // window.location.href = `${process.env.REACT_APP_FRONTEND_URL}`
    }, [dispatch, card, formState, stripe, token])

    // handle the credit card element`s change
    const handleChange = async (event: any) => {
        if (event.error) {
            setDisabled(true);
            setError(event.error.message);
            return;
        }
        setDisabled(event.empty)
        setError("");

    }

    const cardStyle = {
        disabled: cardDisabled,
        style: {
            base: {
                color: "#32325d",
                fontFamily: 'Arial, sans-serif',
                fontSmoothing: "antialiased",
                fontSize: "16px",
                "::placeholder": {
                    color: "#32325d"
                }
            },
            invalid: {
                color: "#fa755a",
                iconColor: "#fa755a"
            }
        }
    };

    function paymentMethod() {
        if (creditCard) {
            return (
                <>
                    {/* stipe element! */}
                    <Form.Label>Credit Card</Form.Label>
                    <CardElement id="card-element" options={cardStyle} onChange={handleChange} />

                    <button id="paymentButton" type="submit" disabled={disabled}>
                        <span id="button-text">
                            {processing ? (<div className="spinner" id="spinner"></div>) :
                                (`Pay ${formState.values.price === "" ? "for you application" : Math.round(formState.values.price / formState.values.instalment)}`)
                            }
                        </span>
                    </button>
                    {error && (
                        <div className="card-error" role="alert">
                            {error};
                        </div>
                    )}
                    <p className={succeeded ? "result-message" : "result-message hidden"}>
                        Payment succeeded,
                                {enrollmentSucceeded ? "your enrollment is completed, you can exit the page" : "please wait the enrollment to completed....."}
                    </p>
                </>
            )
        } else {
            return (
                <>
                    <button id="paymentButton" type="submit" >Submit</button>

                    <p className={message ? "result-message" : "result-message hidden"}>
                        {message}
                    </p>
                </>
            )
        }
    }

    useEffect(() => {
        dispatch(clearMessage())
    }, [dispatch])


    return (
        <div id="ApplicationPage">
            <Container>
                <h1>Payment of Tuition Fee</h1>
                <Row>
                    <Col>
                        <div className="registrationInfo">
                            <div >
                                <h5>Start Date:</h5>
                                <div>{function () {
                                    const startDate = (cohortsInfo.find((cohort) => cohort.cohort_id === date))?.start_date;
                                    if (startDate == null || undefined) {
                                        return "* please select the cohort"
                                    }
                                    return startDate
                                }()}</div>
                            </div>
                            <div>
                                <h5>price:</h5>
                                {formState.values.price === "" ? <b>* please select a course</b> : (displayPrice())}
                            </div>
                            <em>Please complete the form to register for the enrollment of MicroMaster in the course</em>
                            <div className="TermsNCons">
                                <h5>Terms and Conditions:</h5>
                                <ol>
                                    <li>Upon confirmation of the payment, the first instalment is immediately charged from your credit card.</li>
                                    <li>If you wish you withdraw during the course, tuition fee is going to be refunded <b>on a pro-rata basis.</b> </li>
                                    <li>The first instalment is non-refundable under any circumstance. The minimum charge of the prorated tuition equals to the amount paid in the first instalment.</li>
                                    <li>If you wish to defer your study, please make you request formally for us to amend your instalment schedule</li>
                                </ol>
                            </div>
                            <form id="couponForm" onSubmit={(event) => {
                                event.preventDefault();
                                const code = formState.values.code
                                fetchCoupon(code)
                            }}>
                                <Form.Label>coupon code:</Form.Label>
                                <input {...text('code')} required />
                                <button>use</button>
                            </form>
                            <b style={{ color: "black" }}>{couponMessage}</b>
                        </div>
                    </Col>

                    <Col sm={8} >
                        <form id="applicationForm" onSubmit={async (event) => {
                            event.preventDefault();
                            if (creditCard) {
                                setProcessing(true)
                                setDisabled(true);
                                setCardDisabled(true);
                                CreatePaymentIntent();
                            } else {
                                await dispatch(enrollByOtherMethod(formState.values))
                                formState.reset();
                            }

                        }}>
                            <Form.Label>Nick Name</Form.Label>
                            <input {...text('nickName')} placeholder="Nick Name" required />
                            <Form.Label>First Name</Form.Label>
                            <input {...text('firstName')} placeholder="First Name" required />
                            <Form.Label>Last Name</Form.Label>
                            <input {...text('lastName')} placeholder="Last Name" required />
                            <Form.Label>Address</Form.Label>
                            <input {...text('address')} placeholder="Address" required />
                            <Form.Label>Phone</Form.Label>
                            <input {...number('phone')} placeholder="Phone" required  />
                            <Form.Label>Email</Form.Label>
                            <input {...email('email')} placeholder="Email" required />

                            {/* select the product */}
                            <Form.Label>Course</Form.Label>
                            <select {...select({
                                name: 'product', onChange: (e) => {
                                    setPrice(parseInt(e.target.value));
                                }
                            })} required>
                                <option value="" disabled={true}>---please select a course---</option>
                                {productsInfo.map((product) => <option key={product.product_id} value={product.product_id}>{product.product_name}</option>)}
                            </select>

                            {/* select the cohort of the product */}
                            <Form.Label>Cohort</Form.Label>
                            <select {...select({
                                name: 'cohort', onChange: (e) => {
                                    const currentCohortId = parseInt(e.target.value);
                                    setDate(currentCohortId);
                                }
                            })} required>
                                <option value="" disabled={true}>--please select a cohort---</option>
                                {cohortsInfo.map((cohort) => <option key={cohort.cohort_id} value={cohort.cohort_id} data-item={cohort.start_date}>{`${cohort.start_date.slice(3)} cohort (${cohort.cohort_name})`}</option>)}
                            </select>

                            {/* finally display the price */}
                            <Form.Label>Price</Form.Label>
                            <input {...number({ name: 'price' })} disabled={true} required />

                            {/* the instalment */}
                            <Form.Label>Instalment</Form.Label>
                            <select {...select('instalment')} required>
                                <option value={1}>full payment</option>
                                <option value={6}>6-month Instalment</option>
                                <option value={12}>12-month Instalment</option>
                            </select>

                            <Form.Label>Payment method</Form.Label>
                            <select onChange={(e) => {
                                e.target.value === '1' ? setCreditCard(true) : setCreditCard(false)
                            }} required>
                                <option value={1}>Credit Card</option>
                                <option value={2}>Others</option>
                            </select>

                            {paymentMethod()}


                        </form>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}