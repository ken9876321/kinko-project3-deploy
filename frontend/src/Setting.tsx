import React, { useEffect, useCallback } from 'react';
import { useFormState } from 'react-use-form-state';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from './store';
import { restoreLogin, settingForm } from './redux/auth/thunk';
import './Setting.scss';
import { resetMessage } from './redux/auth/action';


function Setting() {
    const [formState, { text }] = useFormState();
    const settingData = useSelector((state: RootState) => state.auth.user)
    const isInstructor = useSelector((state: RootState) => state.auth.user?.isInstructor)
    const message = useSelector((state: RootState) => state.auth.message)
    const dispatch = useDispatch();

    function setNameField() {
        formState.setField('lastName', settingData?.last_name);
        formState.setField('firstName', settingData?.first_name);
        formState.setField('nickname', settingData?.nickname);
        formState.setField('address', settingData?.address);
        formState.setField('phoneNumber', settingData?.phone_number);
    }

    function twoForm() {
        if (isInstructor) {
            return (
                <form className="form" onSubmit={async (event) => {
                    event.preventDefault();
                    await dispatch(settingForm(formState.values.lastName, formState.values.firstName, formState.values.nickname, formState.values.address, formState.values.phoneNumber))
                }}>
                    <label>Last name: <input {...text('lastName')} /></label>
                    <label>First name: <input {...text('firstName')} /></label>
                    <label>Nickname: <input {...text('nickname')} /></label>
                    <label>Contact No.: <input {...text('phoneNumber')} /></label>
                    <input type="submit" value="Submit" />
                    <div className="message">{message}</div>
                </form>
            )
        } else {
            return (
                <form className="form" onSubmit={async (event) => {
                    event.preventDefault();
                    await dispatch(settingForm(formState.values.lastName, formState.values.firstName, formState.values.nickname, formState.values.address, formState.values.phoneNumber))
                }}>
                    <label>Last name: <input {...text('lastName')} /></label>
                    <label>First name:  <input {...text('firstName')} /></label>
                    <label>Nickname:  <input {...text('nickname')} /></label>
                    <label>Address: <input {...text('address')} /></label>
                    <label>Contact No.:<input {...text('phoneNumber')} /></label>
                    <input type="submit" value="Submit" />
                    <div className="message">{message}</div>
                </form>
            )
        }
    }

    const unchangeSetNameField = useCallback(setNameField, [])


    useEffect(() => {
        unchangeSetNameField()
    }, [unchangeSetNameField])



    useEffect(() => {
        dispatch(restoreLogin())
        dispatch(resetMessage())
    }, [dispatch])

    return (
        <>
            <div className="back">
                {twoForm()}
            </div>
        </>
    )
}

export default Setting;