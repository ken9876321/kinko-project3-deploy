import { createStore, combineReducers, applyMiddleware, compose, AnyAction } from 'redux'
import { createBrowserHistory } from 'history';
import { RouterState, connectRouter, routerMiddleware } from 'connected-react-router';
import { authReducer, AuthState } from './redux/auth/reducer';
import thunk, { ThunkDispatch as OldThunkDispatch } from 'redux-thunk'
import { StudentReducer, StudentState } from './redux/student/reducer';
import { InstructorState, InstructorReducer } from './redux/instructor/reducer';



declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

export type RootAction = AnyAction;

export type ThunkDispatch = OldThunkDispatch<RootState, null, RootAction>;

export const history = createBrowserHistory();

export interface RootState {
    router: RouterState;
    auth: AuthState;
    student: StudentState;
    instructor: InstructorState;
}

const reducer = combineReducers<RootState>({
    router: connectRouter(history),
    auth: authReducer,
    student: StudentReducer,
    instructor: InstructorReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
    reducer,
    composeEnhancers(
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history)))
);