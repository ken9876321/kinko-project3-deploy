import React from 'react';
import { Container, Button } from 'react-bootstrap';
import './StudentPayment.scss'
import { BillingStatus } from './BillingStatus';
import { NavLink, useRouteMatch, Redirect } from 'react-router-dom';
import CreditCardForm from './CreditCardForm';

export function StudentPayment() {
    const route = useRouteMatch<{path: string}>();
    const param = route.params.path;
    
    return (
        <Container className="studentPayment">
            <div className="subNav">
                <p id="SubTile">Payment Information</p>

                <div className="subNavButton">
                    <NavLink className="subNavLink" to="/payment"><Button variant="primary">Change Payment Method</Button></NavLink>
                    <NavLink className="subNavLink" to="/payment/billing_status"><Button variant="info">Billing Status</Button></NavLink>
                    {/* <Button variant="primary" >Change Payment Method</Button>
                    <Button variant="info" >Billing Status</Button> */}
                </div>
            </div>
            
            
            {param ? param === "billing_status"  ? <BillingStatus /> : <Redirect to="/payment" /> : <CreditCardForm />}
            

        </Container>
    )
}