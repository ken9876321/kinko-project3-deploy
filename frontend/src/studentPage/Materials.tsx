import React, { useEffect, useState, useCallback } from 'react';
import { Row, Card, Col, Spinner, Button, Modal, Form, Table } from 'react-bootstrap';
import './Materials.scss'
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { fetchDashboardMaterials, fetchPersonalStuffs } from '../redux/student/thunk';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt, faPlusSquare, faArrowAltCircleLeft } from '@fortawesome/free-solid-svg-icons'
import { useFormState } from 'react-use-form-state';
import { deleteMaterials, fetchInstructorMaterials, getProductCohort } from '../redux/instructor/thunk';
import { push } from 'connected-react-router';
import { useRouteMatch } from 'react-router-dom';


export function Materials() {
    const materials = useSelector((state: RootState) => state.student.materials)
    const token = useSelector((state: RootState) => state.auth.token)
    const fetchingData = useSelector((state: RootState) => state.auth.fetchingData)
    const user = useSelector((state: RootState) => state.auth.user)
    const dispatch = useDispatch();
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [formState, { text, url }] = useFormState()
    const [submitting, setSubmitting] = useState(false)
    const [message, setMessage] = useState('');
    const [showDelete, setShowDelete] = useState(false);
    const [photo, setPhoto] = useState<undefined | File>(undefined);
    const router = useRouteMatch<{ path: string }>()
    const activeCourses = useSelector((state: RootState) => state.instructor.products)
    const personalCourses = useSelector((state: RootState) => state.student.personalProducts)
    const id = router.params.path;



    useEffect(() => {
        if (user?.isInstructor) {
            dispatch(getProductCohort())
            if (id) {
                dispatch(fetchInstructorMaterials(id))
            }
        } else {
            dispatch(fetchPersonalStuffs())
            if (id) {
                dispatch(fetchDashboardMaterials(id))
            }
        }
    }, [dispatch, id, user])

    const addMaterials = useCallback(async (formData) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/materials`, {
            method: "post",
            headers: {
                'Authorization': `Bearer ${token}`
            },
            body: formData
        })
        const result = await res.json()

        if (!result.success) {
            setSubmitting(false)
            setMessage("adding failed, please try again later");
            return
        }
        dispatch(fetchInstructorMaterials(id))
        setSubmitting(false)
        setMessage("succeed");
    }, [token, dispatch, id])

    const noImage = "https://www.ira-sme.net/wp-content/themes/consultix/images/no-image-found-360x260.png";
    return (
        <div className="materialPage">
            {!id && ((user?.isInstructor) ? <h5>All Course</h5> : <h5>You course</h5>)}

            {!fetchingData && id && user?.isInstructor && <div className="addMaterialBtn">
                <Button variant="secondary" onClick={handleShow}>
                    <FontAwesomeIcon icon={faPlusSquare} /> Add material
                      </Button>
                <Button variant="secondary" onClick={() => {
                    setShowDelete(!showDelete);
                }}>
                    <FontAwesomeIcon icon={faTrashAlt} />
                    Delete material
                </Button>
            </div>}

            {!fetchingData && id && <Button id="previousBtn" onClick={() => { dispatch(push('/dashboard')) }}><FontAwesomeIcon icon={faArrowAltCircleLeft} /></Button>}


            {!fetchingData && !id && ((user?.isInstructor) ?
                <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>Course Id</th>
                            <th>Course Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {activeCourses?.map((activeCourse, index) =>
                            <tr key={index} className="courseList" onClick={(event) => {
                                dispatch(push(`/dashboard/${activeCourse.id}`))
                            }}>
                                <td>{activeCourse.id}</td>
                                <td>{activeCourse.name}</td>
                            </tr>
                        )}
                    </tbody>
                </Table>
                : <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>Course</th>
                            <th>Course Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {personalCourses.map((personalCourse, index) =>
                            <tr key={index} className="courseList" onClick={(event) => {
                                dispatch(push(`/dashboard/${personalCourse.product_id}`))
                            }}>
                                <td>{personalCourse.product_id}</td>
                                <td>{personalCourse.name}</td>
                            </tr>
                        )}
                    </tbody>
                </Table>)
            }

            {!fetchingData && id && user?.isInstructor &&
                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add new materials</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form id="addMaterialForm" onSubmit={async (event: React.FormEvent<HTMLFormElement>) => {
                            event.preventDefault()
                            const newFormData = new FormData()
                            newFormData.append('title', formState.values.title)
                            newFormData.append('url', formState.values.url)
                            newFormData.append('description', formState.values.description)
                            newFormData.append('product_id', id)
                            if (photo != null) {
                                newFormData.append('image', photo)
                            }

                            addMaterials(newFormData)
                            formState.reset();
                        }}>
                            <Form.Group controlId="materialTitle">
                                <Form.Label>Title</Form.Label>
                                <Form.Control  {...text('title')} placeholder="title" required />
                            </Form.Group>

                            <Form.Group controlId="materialUrl">
                                <Form.Label>URL</Form.Label>
                                <Form.Control  {...url('url')} placeholder="URL" required />
                            </Form.Group>

                            <Form.Group >
                                <Form.File id="materialImage" label="Image Upload" accept="image/*" onChange={(event: React.ChangeEvent<HTMLInputElement>) => setPhoto(event.currentTarget.files?.[0])} />
                            </Form.Group>

                            <Form.Group controlId="materialDescription">
                                <Form.Label>description</Form.Label>
                                <Form.Control as="textarea" {...text('description')} rows={3} required />
                            </Form.Group>

                            <Button disabled={submitting} type="submit" variant="dark">submit</Button>
                            {message}
                        </Form>
                    </Modal.Body>
                </Modal>}

            {!fetchingData && id && <Row className="infoCardGroup">
                {materials && materials.map((material, i) =>
                    <Col key={i} xs={10} lg={5} className="infoCard">
                        <a href={material.link} target="_blank" rel="noopener noreferrer">
                            <Card className="card">
                                <Card.Img variant="top" className="cardImage" src={material.image_path ? `${process.env.REACT_APP_AWS_IMAGE_URL}/${material.image_path}` : noImage} />
                                <Card.Body>
                                    <Card.Title>{material.title}</Card.Title>
                                    <Card.Text>
                                        {material.description}
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </a>
                        {showDelete &&
                            <div data-id={material.id} onClick={(event) => {
                                const currentCardId = parseInt(event.currentTarget.dataset.id!);
                                dispatch(deleteMaterials(currentCardId, id));

                            }}><FontAwesomeIcon icon={faTrashAlt} /></div>}
                    </Col>
                )}


            </Row>}
            {fetchingData && ([1, 2, 3].map((current) => <Spinner key={current} animation="grow" variant="dark" />))}
            {!fetchingData && id && materials && materials.length === 0 && <div>not materials yet</div>}
        </div>
    )
}