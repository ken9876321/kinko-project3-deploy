import React, { useEffect } from 'react';
import { Button, Table } from 'react-bootstrap';
import { useFormState } from 'react-use-form-state';
import './BillingStatus.scss'
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { fetchBillingStatus } from '../redux/student/thunk';

export function BillingStatus() {
    const [formState, { date }] = useFormState()
    const dispatch = useDispatch();
    const billingStatus = useSelector((state: RootState) => state.student.billingInfo)

    useEffect(()=>{
        dispatch(fetchBillingStatus("1970-01-01", new Date().toLocaleDateString()))
    },[dispatch])

    return (
        <div className="studentBillingStatus">
            <div className="dateForm">
                <form onSubmit={(event) => {
                    event.preventDefault();
                    const formData = formState.values
                    dispatch(fetchBillingStatus(formData.fromDate, formData.toDate))
                }}>
                    <div className="dateGroup">
                        <label> From Date:
                        <input className="dateSelect" {...date('fromDate')} required />
                        </label>
                        <label >To Date:
                        <input className="dateSelect" {...date('toDate')} required />
                        </label>
                    </div>
                    <div className="btnRow"> <Button type="submit" variant="dark" className="checkDateBtn">check</Button></div>
                </form>
            </div>



            <div className="BillingsStatusTable">
                <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>Billing_Date</th>
                            <th>Payment Type</th>
                            <th>Description</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        {billingStatus.map((billingStatus, index) =>
                            <tr key={index}>
                                <td>{billingStatus.billing_date}</td>
                                <td>{billingStatus.type}</td>
                                <td>{billingStatus.transaction_number}</td>
                                <td>{billingStatus.amount}</td>
                            </tr>
                        )}
                    </tbody>
                </Table>
                {billingStatus.length === 0 && <div>no record to display</div>}
            </div>
        </div>
    )
}