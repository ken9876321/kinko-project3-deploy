import React, { useState, useEffect, useCallback } from 'react';
import { Container, Table } from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import { useDispatch, useSelector } from 'react-redux';
import { getLeaves } from '../redux/student/thunk';
import { RootState } from '../store';
import './LeaveApplicationRecord.scss'


export function LeaveApplicationRecord() {
    const [totalPage, setTotalPage] = useState(0);
    const dispatch = useDispatch()
    const leaveApplications = useSelector((state: RootState) => state.student.leaveApplications)
    const perPage = 10

    useEffect(() => {
        dispatch(getLeaves(perPage, 0, setTotalPage))
    }, [dispatch])

    const handlePageClick = useCallback((data) => {
        let selected = data.selected;
        let offset = Math.ceil(selected * perPage)
        dispatch(getLeaves(perPage, offset, setTotalPage))
    }, [dispatch])

    return (
        <Container className="leaveApplicationRecord">
            <h4 id="title">Leave Application Status</h4>

            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                        <th>cohort</th>
                        <th>period</th>
                        <th>reason</th>
                        <th>approval</th>
                    </tr>
                </thead>

                <tbody>
                    {leaveApplications.map((leaveApplication, index) =>
                        <tr key={index}>
                            <td>{leaveApplication.name}</td>
                            <td>{`${leaveApplication.period_from} to ${leaveApplication.period_to}`}</td>
                            <td>{leaveApplication.reason}</td>
                            <td>{leaveApplication.approval === null? "In the progress" : leaveApplication.approval ?  <div style={{color:"green"}}>approved</div>: <div style={{color:"red"}}>denied</div>}</td>
                        </tr>
                    )}
                </tbody>
            </Table>
            
            {leaveApplications.length === 0 && <div>沒有紀錄</div>}

          {leaveApplications.length !==0 &&  <ReactPaginate
                previousLabel={'previous'}
                nextLabel={'next'}
                breakLabel={'...'}
                breakClassName={'break-me'}
                pageCount={totalPage}
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                onPageChange={handlePageClick}
                containerClassName={'pagination'}
                activeClassName={'active'}
            />}
        </Container>
    )
}