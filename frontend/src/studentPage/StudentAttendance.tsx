import React, { useEffect, useState, useCallback } from 'react';
import { useFormState } from 'react-use-form-state';
import { Button, Table, Spinner } from 'react-bootstrap';
import './StudentAttendance.scss';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { fetchStudentAtt, fetchAllStudentAtt } from '../redux/student/thunk';
import ReactPaginate from 'react-paginate';

export function StudentAttendance() {
    const [formState, { date }] = useFormState()
    const attendances = useSelector((state: RootState) => state.student.attendance)
    const dispatch = useDispatch()
    const [totalPage, setTotalPage] = useState(0);
    const [display, setDisplay] = useState('default')
    const fetchingData = useSelector((state: RootState) => state.auth.fetchingData)
    const [fromDate, setFromDate]  = useState('')
    const [toDate, setToDate]  = useState('')

    const perPage = 10

    useEffect(() => {
        dispatch(fetchAllStudentAtt(10, 0, setTotalPage))
    },[dispatch])

    const handlePageClick = useCallback((data) => {
        let selected = data.selected;
        let offset = Math.ceil(selected * perPage)
        dispatch(fetchAllStudentAtt(perPage, offset, setTotalPage));
    }, [dispatch])

    const handlePageClickByDate = useCallback((data) => {
        let selected = data.selected;
        let offset = Math.ceil(selected * perPage)
        dispatch(fetchStudentAtt(fromDate, toDate, perPage, offset, setTotalPage))
    }, [dispatch, fromDate, toDate])


    return (
        <div className="studentAttendance">
            <div className="dateForm">
                <form onSubmit={(event) => {
                    event.preventDefault();
                    setDisplay('byDate')
                    const formData = formState.values
                    setFromDate(formData.fromDate);
                    setToDate(formData.toDate)
                    dispatch(fetchStudentAtt(formData.fromDate, formData.toDate, perPage, 0 ,setTotalPage))
                }}>
                    <div className="dateGroup">
                        <label> From Date:
                        <input className="dateSelect" {...date('fromDate')} required />
                        </label>
                        <label >To Date:
                        <input className="dateSelect" {...date('toDate')} required />
                        </label>
                    </div>

                    <div className="btnRow"><Button variant="dark" type="submit" className="checkDateBtn">check</Button></div>
                </form>

            </div>



            <div className="attendanceTable">
                <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>Date Time</th>
                            <th>Cohort</th>
                            <th>Description</th>
                            <th>Attendance</th>
                        </tr>
                    </thead>
                    <tbody>
                        {attendances.map((attendance) =>
                            <tr key={attendance.students_attendance_id}>
                                <td>{attendance.date_time}</td>
                                <td>{attendance.cohort}</td>
                                <td>{attendance.description}</td>
                                <td>{attendance.type}</td>
                            </tr>
                        )}
                    </tbody>
                </Table>
                {attendances.length === 0 && <div>沒有紀錄，請再選擇日期</div>}
            </div>

            {display === 'default' && attendances && attendances.length !== 0 &&
                <ReactPaginate
                    previousLabel={'previous'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    pageCount={totalPage}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageClick}
                    containerClassName={'pagination'}
                    activeClassName={'active'}
                />}

            {display === 'byDate' && attendances && attendances.length !== 0 &&
                <ReactPaginate
                    previousLabel={'previous'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    pageCount={totalPage}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageClickByDate}
                    containerClassName={'pagination'}
                    activeClassName={'active'}
                />}
            
            {fetchingData === true && ([1, 2, 3].map((current) => <Spinner key={current} animation="grow" variant="dark" />))}
        </div>
    )
}