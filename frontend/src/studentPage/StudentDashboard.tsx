import React from 'react';
import { Button, Container } from 'react-bootstrap';
import './StudentDashboard.scss'
import { Materials } from './Materials';
import { StudentAttendance } from './StudentAttendance';
import { useRouteMatch, NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { RootState } from '../store';



export function StudentDashboard() {
    const router = useRouteMatch<{ path: string }>()
    const user = useSelector((state: RootState) => state.auth.user)
    const path = router.path

    return (
        <div className="studentDashboard">
            <Container className="studentDashboardContainer">
                <div className="subNav">
                    <h5 id="SubTile">Dashboard Overview</h5>

                    <div className="subNavButton">
                        <NavLink className="subNavLink" to="/dashboard">
                            <Button variant="primary">Course Materials</Button>
                        </NavLink>

                        {!user?.isInstructor && <NavLink className="subNavLink" to="/studentAttendance">
                            <Button variant="info">Attendance</Button>
                        </NavLink>}

                    </div>
                </div>

            
                
                {(user?.isInstructor)? <Materials /> : (path === "/studentAttendance")? <StudentAttendance /> : <Materials /> }
    


            </Container>

        </div>
    )
}