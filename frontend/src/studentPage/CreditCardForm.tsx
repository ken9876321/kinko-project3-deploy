import React, { useState, useEffect, useCallback } from 'react';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { useSelector, useDispatch } from 'react-redux';
import './CreditCardForm.scss'
import { Col, Container, Card, ListGroup, Spinner } from 'react-bootstrap';
import { fetchPaymentMethod } from '../redux/student/thunk';
import { RootState } from '../store';

export default function CreditCardForm() {
    const [succeeded, setSucceeded] = useState(false);
    const [error, setError] = useState('');
    const [processing, setProcessing] = useState(false);
    const [disabled, setDisabled] = useState(true);
    const [cardDisabled, setCardDisabled] = useState(false);
    const currentCardInfos = useSelector((state: RootState) => state.student.paymentMethods)
    const token = useSelector((state: RootState) => state.auth.token)
    const stripe = useStripe();
    const elements = useElements();
    const card = elements?.getElement(CardElement)
    const dispatch = useDispatch();
    const fetchingData = useSelector((state:RootState) => state.auth.fetchingData);

    useEffect(() => {
        dispatch(fetchPaymentMethod());
    }, [dispatch])

    const ChangePayment = useCallback(async function () {
        /* create paymentMethod by stripe js*/
        const paymentMethod = await stripe?.createPaymentMethod({
            type: 'card',
            card: card!,
            billing_details: {
                email: currentCardInfos[0].billingEmail
            }
        });
        /*send the payment to server getting the setupIntents created in server side */
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/payment_method`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    paymentMethod
                }),
        })
        const result = await res.json()
        
        if (result.error) {
            setError(`changeCard failed : something wrong with your input`);
            setProcessing(false);
            setCardDisabled(false);
            card?.clear();
            return
        }
        /*let the client to confirm their card setup (3d secure), then attach the new paymentMethod to customer */
        const after = await stripe?.confirmCardSetup(result.clientSecret)

        if (after?.error) {
            setError(`changeCard failed ${after.error.message}`);
            setProcessing(false);
            setCardDisabled(false);
            card?.clear();
            return;
        }

        /* after add the new paymentMethod, delete the old one*/
        {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/payment_method`, {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            const result = await res.json();
            if (result.error) {
                setError('result.error')
                setProcessing(false);
                setCardDisabled(false);
                card?.clear();
                return
            }

            card?.clear();
            setCardDisabled(false);
            setProcessing(false);
            setSucceeded(true);
            dispatch(fetchPaymentMethod());
        }

    }, [card,currentCardInfos,stripe,token,dispatch])


    const cardStyle = {
        disabled: cardDisabled,
        style: {
            base: {
                color: "#32325d",
                fontFamily: 'Arial, sans-serif',
                fontSmoothing: "antialiased",
                fontSize: "16px",
                "::placeholder": {
                    color: "#32325d"
                }
            },
            invalid: {
                color: "#fa755a",
                iconColor: "#fa755a"
            }
        }
    };

    const handleChange = async (event: any) => {
        setDisabled(event.error ? true : event.empty)
        setError(event.error ? event.error.message : "")
    }

    return (
        <Container className="creditCardForm">
           
            {(currentCardInfos && currentCardInfos.length !== 0) &&
                <div>
                    <Col className="cardSets" md={12}  >
                        {currentCardInfos.map((currentCardInfo, index) =>
                            <Card key={index} className="currentCards">
                                <Card.Header>Your Current Payment Card</Card.Header>
                                <ListGroup>
                                    <ListGroup.Item className="brandAndNum">
                                        <span> {currentCardInfo.brand}</span>
                                        <span>**** **** **** {currentCardInfo.lastFour}</span>
                                    </ListGroup.Item>
                                    <ListGroup.Item className="billingEmail">Billing Email: {currentCardInfo.billingEmail}</ListGroup.Item>
                                </ListGroup>
                            </Card>
                        )}
                    </Col>

                    <Col className="changeCardForm" md={12} >
                        <form onSubmit={async (event) => {
                            event.preventDefault();
                            setDisabled(true);
                            setCardDisabled(true);
                            setProcessing(true)
                            ChangePayment();
                        }}>
                            <CardElement id="card-element" options={cardStyle} onChange={handleChange} />
                            <button disabled={disabled}>
                                <span id="button-text">
                                    {processing ? (<div className="spinner" id="spinner"></div>) :
                                        ("Change")
                                    }
                                </span>
                            </button>
                            {error && (
                                <div className="card-error" role="alert">
                                    {error};
                                </div>
                            )}
                            <p className={succeeded ? "result-message" : "result-message hidden"}>
                                succeeded to change the payment method,
                                you can exit this page.
                </p>
                        </form>
                    </Col>
                </div>
            }

            {fetchingData? ([1,2,3].map((current)=> <Spinner key={current} animation="grow" variant="dark" />)) : currentCardInfos.length === 0 && <div>You did not pay for any courses by credit card</div>}
         
        </Container>
    )
}