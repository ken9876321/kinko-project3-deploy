import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPersonalStuffs, submitLeave } from '../redux/student/thunk';
import { Container, Form, Col, Button } from 'react-bootstrap';
import { useFormState } from 'react-use-form-state';
import './LeaveApplicationForm.scss'
import { RootState } from '../store';

export function LeaveApplicationForm() {
    const dispatch = useDispatch();
    const [formState, { text, date, number }] = useFormState();
    const cohorts = useSelector((state: RootState) => state.student.personalCohorts)
    const [processing, setProcessing] = useState(false);
    const [message, setMessage] = useState("");
    

    useEffect(() => {
        dispatch(fetchPersonalStuffs())
    }, [dispatch])


    return (
        <Container className="leaveApplicationForm">
            <h4 id="title">Leave Application</h4>

           { cohorts && cohorts.length !== 0 &&
           <div>
            <Form onSubmit={(event: React.FormEvent<HTMLFormElement>) => {
                event.preventDefault()
                const formData = formState.values
                dispatch(submitLeave(formData, processing, setProcessing, message, setMessage))
            }}>
                <Form.Group controlId="cohort">
                    <Form.Label>Cohort</Form.Label>
                    <Form.Control as="select"  {...number('cohort')} required>
                        <option value="" disabled={true}>--please select a cohort---</option>
                        {cohorts.map((cohort) =>
                            <option key={cohort.cohort_id} value={cohort.cohort_id}>{cohort.name}</option>
                        )}
                    </Form.Control>
                </Form.Group>

                <Form.Row>
                    <Form.Group as={Col} controlId="formDate">
                        <Form.Label>From</Form.Label>
                        <Form.Control type="date" {...date('fromDate')} required />
                    </Form.Group>

                    <Form.Group as={Col} controlId="toDate">
                        <Form.Label>To</Form.Label>
                        <Form.Control type="date" {...date('toDate')} required />
                    </Form.Group>
                </Form.Row>


                <Form.Group controlId="teckyInstructor">
                    <Form.Label>Reason</Form.Label>
                    <Form.Control as="textarea" {...text('reason')} maxLength={300} rows={3} placeholder="max 300 word" required />
                </Form.Group>
                <Button disabled={processing} type="submit">Submit</Button>
            </Form>
            <div>{message}</div>
            </div>}
            {!cohorts? <div>you do not have any course is in progress</div> : cohorts.length === 0 && <div>you did not apply any course yet</div>}
        </Container>
    )
}