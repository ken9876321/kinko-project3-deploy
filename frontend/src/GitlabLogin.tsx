import React, { useEffect } from 'react'
import { loginGitlab } from './redux/auth/thunk';
import { useDispatch } from 'react-redux';
import './GitlabLogin.scss';
import { push } from 'connected-react-router';

export function GitLabLogin() {
    const dispatch = useDispatch();

    useEffect(() => {
        const params = new URLSearchParams(window.location.search)
        console.log(params.get('code')!);
        const code = params.get('code');
        if(code === null){
            dispatch(push('/'));
            return
        }
        dispatch(loginGitlab(params.get('code')!));
    }, [dispatch])

    return (
        <>
            <div className="loading">Loading</div>
        </>
    )
}