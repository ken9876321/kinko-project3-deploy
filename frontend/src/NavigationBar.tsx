import React, { useState } from 'react'
import { Nav, Navbar, Button } from 'react-bootstrap'
import styles from './NavigationBar.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import logo from './tecky_logo.png'
import StudentLeftBar from './StudentLeftBar';
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from './store'
import InstructorLeftBar from './InstructorLeftBar'
import { thunkLogout } from './redux/auth/thunk'


function NavigationBar() {
    const isLogin = useSelector((oldstate: RootState) => oldstate.auth.isAuthenticated)
    const nickname = useSelector((oldstate: RootState) => oldstate.auth.user?.nickname ? oldstate.auth.user.nickname : oldstate.auth.user?.username)
    const isInstructor = useSelector((oldstate: RootState) => oldstate.auth.user?.isInstructor)
    const cohortsLength = useSelector((oldstate: RootState) => oldstate.auth.user?.cohortsLength)
    const [open, setOpen] = useState(false);
    const dispatch = useDispatch()

    function differentiateLeftBar() {
        if (isInstructor) {
            return (<InstructorLeftBar open={open} setOpen={setOpen} />)
        } else {
            if (cohortsLength && cohortsLength > 0)
                return (<StudentLeftBar open={open} setOpen={setOpen} />)
        }
    }

    return (
        <div className={styles.Navigation}>
            <Navbar className={styles.navbar} expand="sm">
                {isLogin && isInstructor ? <Button
                    data-testid="leftBarButton"
                    onClick={() => setOpen(!open)}
                    className={styles.barsButton}
                >
                    <FontAwesomeIcon icon={faBars} />
                </Button> : (cohortsLength !== undefined && cohortsLength !== 0) && <Button
                    data-testid="leftBarButton"
                    onClick={() => setOpen(!open)}
                    className={styles.barsButton}
                >
                    <FontAwesomeIcon icon={faBars} />
                </Button>}
                {/* } */}
                <Navbar.Brand data-testid="navBrand" href="/"><img className={styles.logo} alt="" src={logo} /></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link className={styles.tecky_website} style={{ color: "ivory" }} href="https://tecky.io" title="https://tecky.io" target="_blank">Our website</Nav.Link>
                    </Nav>

                    <Navbar.Text>
                        {isLogin && <div className={styles.current_user}>Hi! {nickname}</div>}
                    </Navbar.Text>
                    <Nav>
                        {isLogin && <button className={styles.navButton} onClick={() => {
                            dispatch(thunkLogout());
                        }}>Logout</button>}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>


            {isLogin &&
                <div id="example-collapse-text" >
                    {differentiateLeftBar()}
                </div>}

        </div>

    )
}

export default NavigationBar;