import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import { store, history } from './store';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

test('renders learn react link', () => {
  const { getByText } = render(  <React.StrictMode>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>
  </React.StrictMode>);
  const linkElement = getByText(/website/i);
  expect(linkElement).toBeInTheDocument();
});
