import '@testing-library/dom';
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { store } from '../store';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { StudentDashboard } from '../studentPage/StudentDashboard';
import { createMemoryHistory as createHistory, createBrowserHistory} from 'history'

const mockDispatch = jest.fn();
jest.mock('react-redux', ()=>({
    useSelector: jest.fn(),
    useDispatch: () => mockDispatch
}))
jest.mock('react-router-dom')


describe('DashBoard button', () => {

   
    
    it("shold container a Course Materials button", async () => {
        const history = createHistory();
        const screen = render(<Provider store={store}>
            <ConnectedRouter history={history}>
                <StudentDashboard />
            </ConnectedRouter>
        </Provider>)
        const element = screen.getByText("Course Materials")
        expect(element).toBeInTheDocument()
        screen.unmount()
    })

    it("shold container a Attendance button", async () => {
        const dispatch = jest.fn()
        const history = createHistory();
        const screen = render(<Provider store={store}>
            <ConnectedRouter history={history}>
                <StudentDashboard />
            </ConnectedRouter>
        </Provider>)
        const element = screen.getAllByText("Attendance")
        expect(element).toBeDefined()


        screen.unmount();
    })

  
   
    

})