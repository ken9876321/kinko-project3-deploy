import '@testing-library/dom';
import React from 'react';
import {render, screen, fireEvent} from '@testing-library/react';
import NavigationBar from '../NavigationBar';
import StudentLeftBar from '../StudentLeftBar';
import { store, history } from '../store';
import { Provider } from 'react-redux';
import { AuthState, User } from '../redux/auth/reducer';
import { faChessKing } from '@fortawesome/free-solid-svg-icons';
import { useRouteMatch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';


describe('collapse button', ()=>{
    let initialState:AuthState
    let user:User;


    beforeEach(()=>{
        user = {
            id: 1,
            username: "kinko",
            last_name: "ko",
            first_name: "kin",
            nickname: "kin",
            address: "flat999,87",
            phone_number: "69998721",
            isInstructor: false,
            message: null,
            cohortsLength: 1
        }

        initialState = {
            token: "wqwewqeeqwqweqweqeq",
            user: user,
            isAuthenticated: true,
            message: null,
            fetchingLogin: false,
            fetchingData: false
        };
    })
    
    it("shold container a collapse button", async()=>{
        const screen = render(<Provider store={store}><NavigationBar /></Provider>)
        const element = screen.queryByTestId("leftBarButton")
        expect(element).toBeNull()
    })

})