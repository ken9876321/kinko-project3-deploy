import { Cohort, User, Lesson, Attendance, AttendanceReport, AttendanceTypeId, LeaveAppWithStudents, Instructor, Offer, Product, Coupon, StudentOnLeave, DealInfo, CohortInfo, StudentInfo, } from "./reducer";

export function getCohort(cohort: Cohort[]) {
    return {
        type: '@@INSTRUCTOR/COHORT' as '@@INSTRUCTOR/COHORT',
        cohort
    }
}

export function setMessge(msg: string) {
    return {
        type: '@@INSTRUCTOR/MESSAGE' as '@@INSTRUCTOR/MESSAGE',
        msg
    }
}

export function clearMessge() {
    return {
        type: '@@INSTRUCTOR/CLEAR_MESSAGE' as '@@INSTRUCTOR/CLEAR_MESSAGE',
    }
}

export function setStudent(students: User[]) {
    return {
        type: '@@INSTRUCTOR/STUDENT' as '@@INSTRUCTOR/STUDENT',
        students
    }
}

export function setStudentOnLeave(studentOnLeave: StudentOnLeave[]) {
    return {
        type: '@@INSTRUCTOR/STUDENTONLEAVE' as '@@INSTRUCTOR/STUDENTONLEAVE',
        studentOnLeave
    }
}

export function clearLessonInfo() {
    return {
        type: '@@INSTRUCTOR/CLEAR_LESSON' as '@@INSTRUCTOR/CLEAR_LESSON',
    }
}

export function setLessonRecord(lessonRecord: Lesson[]) {
    return {
        type: '@@INSTRUCTOR/SET_LESSON_RECORD' as '@@INSTRUCTOR/SET_LESSON_RECORD',
        lessonRecord
    }
}

export function setAttendance(students: Attendance[]) {
    return {
        type: '@@INSTRUCTOR/SET_ATTENDANCE' as '@@INSTRUCTOR/SET_ATTENDANCE',
        students
    }
}

export function setInstructor(instructors: User[]) {
    return {
        type: '@@INSTRUCTOR/SET_INSTRUCTOR' as '@@INSTRUCTOR/SET_INSTRUCTOR',
        instructors
    }
}

export function clearRecordInfo() {
    return {
        type: '@@INSTRUCTOR/CLEAR_RECORD' as '@@INSTRUCTOR/CLEAR_RECORD',
    }
}

export function setAttendanceReport(attendanceReport: AttendanceReport[]) {
    return {
        type: '@@INSTRUCTOR/SET_ATTENDANCE_REPORT' as '@@INSTRUCTOR/SET_ATTENDANCE_REPORT',
        attendanceReport
    }
}

export function setAttendanceId(attendanceId: AttendanceTypeId) {
    return {
        type: '@@INSTRUCTOR/SET_ATTENDANCE_ID' as '@@INSTRUCTOR/SET_ATTENDANCE_ID',
        attendanceId
    }
}

export function setLeaveAppWithStudents(leaveAppWithStudents: LeaveAppWithStudents[]) {
    return {
        type: '@@INSTRUCTOR/SET_LEAVEAPPWITHSTUDENTS' as '@@INSTRUCTOR/SET_LEAVEAPPWITHSTUDENTS',
        leaveAppWithStudents
    }
}

export function setLeaveAppInstructors(leaveAppInstructors: Instructor[]) {
    return {
        type: '@@INSTRUCTOR/SET_LEAVEAPPINSTRUCTOR' as '@@INSTRUCTOR/SET_LEAVEAPPINSTRUCTOR',
        leaveAppInstructors
    }
}

export function setOffer(offers: Offer[]) {
    return {
        type: '@@INSTRUCTOR/OFFERS' as '@@INSTRUCTOR/OFFERS',
        offers
    }
}

export function setProductCohort(products: Product[], cohorts: Cohort[]) {
    return {
        type: '@@INSTRUCTOR/PRODUCTS_COHORTS' as '@@INSTRUCTOR/PRODUCTS_COHORTS',
        products,
        cohorts
    }
}

export function setCoupon(coupon: Coupon[]) {
    return {
        type: '@@INSTRUCTOR/COUPON' as '@@INSTRUCTOR/COUPON',
        coupon
    }
}

export function setDealInfo(dealPayment: DealInfo[]) {
    return {
        type: '@@INSTRUCTOR/SETDEALINFO' as '@@INSTRUCTOR/SETDEALINFO',
        dealPayment
    }
}

export function setCohortInfo(cohortInfo: CohortInfo[]) {
    return {
        type: '@@INSTRUCTOR/SETCOHORTINFO' as '@@INSTRUCTOR/SETCOHORTINFO',
        cohortInfo
    }
}

export function setStudentInfo(studentInfo: StudentInfo[]) {
    return {
        type: '@@INSTRUCTOR/SETSTUDENTINFO' as '@@INSTRUCTOR/SETSTUDENTINFO',
        studentInfo
    }
}


export type InstructorActions =
    ReturnType<typeof clearLessonInfo> |
    ReturnType<typeof getCohort> |
    ReturnType<typeof setMessge> |
    ReturnType<typeof setStudent> |
    ReturnType<typeof setStudentOnLeave> |
    ReturnType<typeof setLessonRecord> |
    ReturnType<typeof setAttendance> |
    ReturnType<typeof setInstructor> |
    ReturnType<typeof clearRecordInfo> |
    ReturnType<typeof clearMessge> |
    ReturnType<typeof setAttendanceReport> |
    ReturnType<typeof setAttendanceId> |
    ReturnType<typeof setLeaveAppWithStudents> |
    ReturnType<typeof setLeaveAppInstructors> |
    ReturnType<typeof setOffer> |
    ReturnType<typeof setProductCohort> |
    ReturnType<typeof setCoupon> |
    ReturnType<typeof setDealInfo> |
    ReturnType<typeof setCohortInfo> |
    ReturnType<typeof setStudentInfo>;
