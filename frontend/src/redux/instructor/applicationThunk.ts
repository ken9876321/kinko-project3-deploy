import { ThunkDispatch, RootState } from "../../store";

export function studentEnroll(formData:any,
    setSucceeded:React.Dispatch<React.SetStateAction<boolean>>,
    setError:React.Dispatch<React.SetStateAction<string>>
    ) {
        
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/studentEnroll`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    customer: formData,
                    studentId: formData.studentId,
                }),
        })

        const enrollResult = await res.json();

        if (enrollResult.error) {
            setSucceeded(false);
            setError(enrollResult.error);
            return;
        }
        setSucceeded(true);
        setError(enrollResult.succeed)
    }
}