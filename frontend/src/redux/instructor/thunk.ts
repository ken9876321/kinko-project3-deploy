import { ThunkDispatch, RootState } from "../../store";
import { getCohort, setMessge, setStudent, setAttendance, setInstructor, setAttendanceReport, setAttendanceId, setLeaveAppWithStudents, setLeaveAppInstructors, setOffer, setProductCohort, setCoupon, setStudentOnLeave, setDealInfo, setCohortInfo, setStudentInfo } from "./action";
import { push } from "connected-react-router";
import { checkFetchingData } from "../auth/action";
import { loadDashboardMaterials } from "../student/action";


export function callCohort() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/cohort`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        const json = await res.json();

        if (json.result) {
            dispatch(getCohort(json.result));
        } else {
            dispatch(setMessge(json.msg))
        }
    }
}

export function createLesson(cohortId: number, description: string, datetime: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/new_lesson`, {
            method: "post",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                cohortId: cohortId,
                description: description,
                datetime: datetime
            })
        })

        const json = await res.json();

        if (json.result) {
            dispatch(setStudent(json.result.students));
            dispatch(setStudentOnLeave(json.result.studentOnLeave));
            dispatch(push(`/attendance/${json.result.cohortName}/new_lesson/${json.result.lessonId}`))

        } else {
            dispatch(setMessge(json.msg))
        }
    }
}

export function checkAttendance(studentId: number, checked: number, lessonId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/attendance`, {
            method: "post",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                studentId: studentId,
                checked: checked,
                lessonId: lessonId
            })
        })

        const json = await res.json();

        dispatch(setMessge(json.msg))
    }
}


export function getAttendance(lessonId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/attendance_record`, {
            method: "post",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                lessonId: lessonId
            })
        })

        const json = await res.json();

        if (json.result) {
            dispatch(setAttendance(json.result.students))
            dispatch(setInstructor(json.result.instructor))
        } else {
            dispatch(setMessge(json.msg))
        }
    }
}

export function revisedLessonName(lessonId: number, newName: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/revise_lesson_name`, {
            method: "post",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                lessonId: lessonId,
                newName: newName
            })
        })

        const json = await res.json();

        dispatch(setMessge(json.msg))

    }
}

export function revisedLessonDateTime(lessonId: number, newDateTime: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/revise_lesson_datetime`, {
            method: "post",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                lessonId: lessonId,
                newDateTime: newDateTime
            })
        })

        const json = await res.json();

        dispatch(setMessge(json.msg))

    }
}

export function getAttendanceReport(cohortId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/attendance_report`, {
            method: "post",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                cohortId: cohortId
            })
        })

        const json = await res.json();

        if (json.result) {
            dispatch(setAttendanceReport(json.result))
        }
    }
}

export function getAttendanceId() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/attendance_id`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        const json = await res.json();

        if (json.result) {
            dispatch(setAttendanceId(json.result))
        }
    }
}

export function delLesson(lessonId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/lesson`, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                lessonId: lessonId
            })
        })

        const json = await res.json();

        if (json.msg) {
            dispatch(setMessge(json.msg))
        }
    }
}

export function getLeaveApp() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/leave_app`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        const json = await res.json();

        if (json.result) {
            dispatch(setLeaveAppWithStudents(json.result.leaveAppWithStudent))
            dispatch(setLeaveAppInstructors(json.result.leaveAppInstructors))
        } else {
            dispatch(setMessge(json.msg))
        }
    }
}

export function approveLeave(leaveId: number, approval: boolean) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/leave_approval`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                leaveId: leaveId,
                approval: approval
            })
        })

        const json = await res.json();

        if (json.msg) {
            dispatch(setMessge(json.msg))
        }
    }
}

export function delLeave(leaveId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/leave`, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                leaveId: leaveId
            })
        })

        const json = await res.json();

        if (json.msg) {
            dispatch(setMessge(json.msg))
        }
    }
}

export function getOffer() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/offer`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        const json = await res.json();

        if (json.result) {
            dispatch(setOffer(json.result))

        } else {
            dispatch(setMessge(json.msg))
        }
    }
}

export function addOffer(productId: number, cohortId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/offer`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                productId: productId,
                cohortId: cohortId
            })
        })

        const json = await res.json();

        dispatch(setMessge(json.msg))
    }
}

export function delOffer(offerId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/offer`, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                offerId: offerId
            })
        })

        const json = await res.json();

        dispatch(setMessge(json.msg))
    }
}

export function addCohort(cohortName: string, date: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/cohort`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                cohortName: cohortName,
                date: date
            })
        })

        const json = await res.json();

        dispatch(setMessge(json.msg))
    }
}

export function addProduct(productName: string, price: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/product`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                productName: productName,
                price: price
            })
        })

        const json = await res.json();

        dispatch(setMessge(json.msg))
    }
}

export function getProductCohort() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;

        dispatch(checkFetchingData(true))
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/product_cohort`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        const json = await res.json();

        if (json.result) {
            dispatch(setProductCohort(json.result.products, json.result.cohorts))
            dispatch(checkFetchingData(false))
            return;
        }
        dispatch(checkFetchingData(false))
    }
}

export function getCoupon() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/coupon`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        const json = await res.json();

        if (json.result) {
            dispatch(setCoupon(json.result))

        } else {
            dispatch(setMessge(json.msg))
        }
    }
}

export function addCoupon(couponName: string, couponCode: string, discount: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/coupon`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                couponName: couponName,
                code: couponCode,
                discount: discount
            })
        })

        const json = await res.json();

        dispatch(setMessge(json.msg))
    }
}

export function delCoupon(couponId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/coupon`, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                couponId: couponId
            })
        })

        const json = await res.json();

        dispatch(setMessge(json.msg))
    }
}


export function deleteMaterials(materialId: number, id: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/materials`, {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                materialId: materialId
            })
        })

        const result = await res.json();

        if (result.error) {
            console.log("internal server error");
            return
        }

        dispatch(fetchInstructorMaterials(id));

    }
}

export function fetchInstructorMaterials(product_id: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;

        dispatch(checkFetchingData(true))
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/fetching_materials`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                product_id: product_id
            })
        });
        const result = await res.json();

        if (result.error) {
            console.log(result.error);
            dispatch(push(`/dashboard`))
            dispatch(checkFetchingData(false))
            return
        }

        dispatch(loadDashboardMaterials(result.data))
        dispatch(checkFetchingData(false))
    }
}

export function getStudentByID(studentId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/student_id`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                studentId: studentId
            })
        })

        const json = await res.json();

        if (json.student !== []) {
            dispatch(setStudentInfo(json.student))
        }
    }
}

export function getStudentByText(text: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/student_text`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                text: text
            })
        })

        const json = await res.json();

        if (json.student && json.student !== []) {
            dispatch(setStudentInfo(json.student))
        }

    }
}

export function getStudentInfoById(studentId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/student_info`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                studentId: studentId
            })
        })

        const json = await res.json();

        if (json.dealInfo !== [] && json.cohortInfo !== []) {
            dispatch(setDealInfo(json.dealInfo))
            dispatch(setCohortInfo(json.cohortInfo))
        }
    }
}

export function turnCohortQuit(id: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/quit`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                id: id
            })
        })

    }
}

export function insertPayment(studentId: number, courseName: string, amount: number, dateTime: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/instructor/payment_insert`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                studentId: studentId,
                courseName: courseName,
                amount: amount,
                date: dateTime
            })
        })

        const json = await res.json();

        dispatch(setMessge(json.msg))
    }
}
