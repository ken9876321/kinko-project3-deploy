import { InstructorActions } from "./action"


export interface InstructorState {
    cohort: Cohort[] | null;
    student: User[] | null;
    studentOnLeave: StudentOnLeave[] | null;
    lessonRecord: Lesson[] | null;
    studentAttandance: Attendance[] | null;
    correspondInstructor: User[] | null;
    attendanceReport: AttendanceReport[] | null;
    attendanceTypeId: AttendanceTypeId | null;
    leaveAppWithStudents: LeaveAppWithStudents[] | null;
    leaveAppInstructors: Instructor[] | null;
    offers: Offer[] | null;
    products: Product[] | null;
    cohorts: Cohort[] | null;
    coupon: Coupon[] | null;
    message: string | null;
    dealInfo: DealInfo[] | null;
    cohortInfo: CohortInfo[] | null;
    studentInfo: StudentInfo[] | null;
}

export interface Cohort {
    id: number;
    name: string;
    start_date: string | null;
}

export interface User {
    id: number;
    username: string;
    nickname: string;
    last_name: string;
    first_name: string;
}

export interface StudentOnLeave {
    nickname: string;
    last_name: string;
    first_name: string;
    period_from: string;
    period_to: string;
}

export interface Lesson {
    id: number;
    cohort_id: number;
    description: string;
    date_time: string;
}

export interface Attendance {
    id: number;
    lesson_id: number;
    type: string | null;
    username: string;
    nickname: string | null;
    last_name: string | null;
    first_name: string | null;
}

export interface AttendanceReport {
    username: string;
    nickname: string;
    last_name: string;
    first_name: string;
    attended: number;
    absent: number;
    late: number;
    leave: number;
}

export interface AttendanceTypeId {
    attended: number;
    absent: number;
    late: number;
    leave: number;
}

export interface LeaveAppWithStudents {
    username: string;
    nickname: string;
    last_name: string;
    first_name: string;
    period_from: string;
    period_to: string;
    reason: string;
    approval: boolean | null;
    name: string;     //Cohort name
    id: number;
    created_at: string;
}

export interface Instructor {
    username: string;
    nickname: string;
    last_name: string;
    first_name: string;
    id: number;
}

export interface Offer {
    id: number;
    name: string;
    price: number;
    available: boolean;
    cohortName: string;
    startDate: string;
}

export interface Product {
    id: number;
    name: string;
    price: string;
}

export interface Coupon {
    id: number;
    name: string;
    code: string;
    discount: number;
}

export interface DealInfo {
    product_id: number;
    personal_price: number;
    total_instalment: number;
    instalmentLeft: number;
    name: string;
    balance: number;
    payment: PaymentInfo[]
}

export interface PaymentInfo {
    date: string;
    amount: number;
    type: string;
    id: number;
}

export interface CohortInfo {
    quitted: boolean;
    name: string;
    id: number;
}

export interface StudentInfo {
    id: number;
    username: string;
    first_name: string;
    last_name: string;
    address: string;
    phone_number: string;
    nickname: string;
    stripe_customer_id: string;
    gitlab_id: string;
}

const initialState = {
    cohort: null,
    student: null,
    studentOnLeave: null,
    lessonRecord: null,
    studentAttandance: null,
    correspondInstructor: null,
    attendanceReport: null,
    attendanceTypeId: null,
    leaveAppWithStudents: null,
    leaveAppInstructors: null,
    offers: null,
    products: null,
    cohorts: null,
    coupon: null,
    message: null,
    dealInfo: null,
    cohortInfo: null,
    studentInfo: null
}

export const InstructorReducer = (state: InstructorState = initialState, action: InstructorActions): InstructorState => {
    switch (action.type) {
        case "@@INSTRUCTOR/COHORT":
            return {
                ...state,
                cohort: action.cohort,
            }
        case "@@INSTRUCTOR/MESSAGE":
            return {
                ...state,
                message: action.msg,
            }
        case "@@INSTRUCTOR/STUDENT":
            return {
                ...state,
                student: action.students,
            }
        case "@@INSTRUCTOR/STUDENTONLEAVE":
            return {
                ...state,
                studentOnLeave: action.studentOnLeave
            }
        case "@@INSTRUCTOR/CLEAR_LESSON":
            return {
                ...state,
                student: null,
                studentOnLeave: null
            }
        case "@@INSTRUCTOR/SET_LESSON_RECORD":
            return {
                ...state,
                lessonRecord: action.lessonRecord
            }
        case "@@INSTRUCTOR/SET_ATTENDANCE":
            return {
                ...state,
                studentAttandance: action.students
            }
        case "@@INSTRUCTOR/SET_INSTRUCTOR":
            return {
                ...state,
                correspondInstructor: action.instructors
            }
        case "@@INSTRUCTOR/CLEAR_MESSAGE":
            return {
                ...state,
                message: null
            }
        case "@@INSTRUCTOR/CLEAR_RECORD":
            return {
                ...state,
                studentAttandance: null,
                correspondInstructor: null,
            }
        case "@@INSTRUCTOR/SET_ATTENDANCE_REPORT":
            return {
                ...state,
                attendanceReport: action.attendanceReport
            }
        case "@@INSTRUCTOR/SET_ATTENDANCE_ID":
            return {
                ...state,
                attendanceTypeId: action.attendanceId
            }
        case "@@INSTRUCTOR/SET_LEAVEAPPWITHSTUDENTS":
            return {
                ...state,
                leaveAppWithStudents: action.leaveAppWithStudents
            }
        case "@@INSTRUCTOR/SET_LEAVEAPPINSTRUCTOR":
            return {
                ...state,
                leaveAppInstructors: action.leaveAppInstructors
            }
        case "@@INSTRUCTOR/OFFERS":
            return {
                ...state,
                offers: action.offers
            }
        case "@@INSTRUCTOR/PRODUCTS_COHORTS":
            return {
                ...state,
                products: action.products,
                cohorts: action.cohorts
            }
        case "@@INSTRUCTOR/COUPON":
            return {
                ...state,
                coupon: action.coupon
            }
        case "@@INSTRUCTOR/SETDEALINFO":
            return {
                ...state,
                dealInfo: action.dealPayment,
            }
        case "@@INSTRUCTOR/SETCOHORTINFO":
            return {
                ...state,
                cohortInfo: action.cohortInfo
            }
        case "@@INSTRUCTOR/SETSTUDENTINFO":
            return {
                ...state,
                studentInfo: action.studentInfo
            }
    }
    return state;
}