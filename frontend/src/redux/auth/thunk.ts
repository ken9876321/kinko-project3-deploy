import { ThunkDispatch, RootState } from "../../store";
import { push } from "connected-react-router";
import { loginFailed, loginSuccess, logout, getUser, setMessage, checkFetchingLogin } from "./action";


export function loginGitlab(code: string) {
    return async (dispatch: ThunkDispatch) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/auth/gitlab_login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                code: code
            })
        })

        const json = await res.json();

        if (json.token != null) {
            localStorage.setItem('token', json.token)
            dispatch(loginSuccess(json.token));
            dispatch(restoreLogin())
            dispatch(push('/welcome'))
        }
    }
}

export function restoreLogin() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;

        if (token == null) {
            dispatch(logout());
            dispatch(checkFetchingLogin(false));
            return;
        }

        dispatch(checkFetchingLogin(true))
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/user/current`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        const json = await res.json();

        if (res.status !== 200) {
            dispatch(loginFailed(json.msg))
            dispatch(checkFetchingLogin(false))
        } else {
            dispatch(loginSuccess(token))
            dispatch(getUser(json))
            dispatch(checkFetchingLogin(false))
        }
    }
}

export function settingForm(lastName: string, firstName: string, nickname: string, address: string, phoneNumber: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;

        if (token == null) {
            dispatch(logout());
            dispatch(checkFetchingLogin(false));
            return;
        }

        console.log("hi")

        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/user/setting`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                lastName: lastName,
                firstName: firstName,
                nickname: nickname,
                address: address,
                phoneNumber: phoneNumber
            })
        })

        const json = await res.json();

        if (json.result) {
            await dispatch(restoreLogin())
            dispatch(setMessage(json.msg));
        } else {
            dispatch(setMessage(json.msg))
        }
    }
}

export function thunkLogout() {
    return async (dispatch: ThunkDispatch) => {
        localStorage.removeItem('token')

        dispatch(logout())
    }
}


