import { AuthActions } from "./action"


export interface AuthState {
    token: string | null;
    user: User | null;
    isAuthenticated: boolean | null;
    message: string | null;
    fetchingLogin: boolean;
    fetchingData: boolean;
}

export interface User {
    id: number;
    username: string;
    last_name: string | null;
    first_name: string | null;
    nickname: string | null;
    address: string | null;
    phone_number: string | null;
    isInstructor: boolean;
    message: string | null;
    cohortsLength: number | undefined;
}

const initialState = {
    token: localStorage.getItem('token'),
    user: null,
    isAuthenticated: null,
    message: null,
    fetchingLogin: true,
    fetchingData: false
}

export const authReducer = (state: AuthState = initialState, action: AuthActions): AuthState => {
    switch (action.type) {
        case "@@AUTH/LOGIN_SUCCESS":
            return {
                ...state,
                token: action.token,
                isAuthenticated: true,
                message: null,
            }
        case "@@AUTH/LOGIN_FAILED":
            return {
                ...state,
                token: null,
                user: null,
                isAuthenticated: false,
                message: action.message
            }
        case "@@AUTH/MESSAGE_RESET":
            return {
                ...state,
                message: null
            }
        case "@@AUTH/GET_USER":
            return {
                ...state,
                user: action.user
            }
        case "@@AUTH/LOGOUT":
            return {
                ...state,
                token: null,
                user: null,
                isAuthenticated: false,
                message: null
            }
        case '@@AUTH/SET_MESSAGE':
            return {
                ...state,
                message: action.message
            }
        case '@@Student/CHECK_FETCH_LOGIN':
            return {
                ...state,
                fetchingLogin: action.fetchingLogin
            }
        case '@@Student/CHECK_FETCH_DATA':
            return {
                ...state,
                fetchingData: action.fetchingData
            }
    }
    return state;
}