import { Attendance, ProductInfo, CohortInfo, BillingInfo, PaymentMethod, PersonalCohort, PersonalProduct, TeckyInstructor, Materials, LeaveApplication, } from "./reducer";

export function loadAttendance(attendance: Attendance[]) {
    return {
        type: '@@Student/LOAD_ATTENDANCE' as '@@Student/LOAD_ATTENDANCE',
        attendance
    }
}

export function loadProductsInfo(productsInfo: ProductInfo[]) {
    return {
        type: '@@Student/LOAD_PRODUCTS_INFO' as '@@Student/LOAD_PRODUCTS_INFO',
        productsInfo
    }
}

export function loadCohortsInfo(cohortsInfo: CohortInfo[]) {
    return {
        type: '@@Student/LOAD_COHORTS_INFO' as '@@Student/LOAD_COHORTS_INFO',
        cohortsInfo
    }
}

export function loadBillingInfo(billingInfo: BillingInfo[]) {
    return {
        type: '@@Student/LOAD_BILLING_INFO' as '@@Student/LOAD_BILLING_INFO',
        billingInfo
    }
}

export function loadPaymentMethod(paymentMethods: PaymentMethod[]) {
    return {
        type: '@@Student/LOAD_PAYMENT_METHODS' as '@@Student/LOAD_PAYMENT_METHODS',
        paymentMethods
    }
}

export function loadPersonalCohort(personalCohorts: PersonalCohort[]) {
    return {
        type: '@@Student/LOAD_PERSONAL_COHORT' as '@@Student/LOAD_PERSONAL_COHORT',
        personalCohorts
    }
}

export function loadPersonalProduct(personalProducts: PersonalProduct[]) {
    return {
        type: '@@Student/LOAD_PERSONAL_PRODUCT' as '@@Student/LOAD_PERSONAL_PRODUCT',
        personalProducts
    }
}

export function loadTekcyInstructor(loadTekcyInstructors: TeckyInstructor[]) {
    return {
        type: '@@Student/LOAD_TECKY_INSTRUCTOR' as '@@Student/LOAD_TECKY_INSTRUCTOR',
        loadTekcyInstructors
    }
}

export function loadDashboardMaterials(materials: Materials[]) {
    return {
        type: '@@Student/LOAD_MATERIALS' as '@@Student/LOAD_MATERIALS',
        materials
    }
}

export function loadLeaveApplications(loadLeaveApplications: LeaveApplication[]) {
    return {
        type: '@@Student/LOAD_LEAVE_APPLICATIONS' as '@@Student/LOAD_LEAVE_APPLICATIONS',
        loadLeaveApplications
    }
}

export function setMessage(message: string) {
    return {
        type: '@@Student/SETMESSAGE' as '@@Student/SETMESSAGE',
        message
    }
}

export function clearMessage() {
    return {
        type: '@@Student/CLEARMESSAGE' as '@@Student/CLEARMESSAGE',
    }
}

export type StudentActions =
    ReturnType<typeof loadAttendance> |
    ReturnType<typeof loadProductsInfo> |
    ReturnType<typeof loadCohortsInfo> |
    ReturnType<typeof loadBillingInfo> |
    ReturnType<typeof loadPaymentMethod> |
    ReturnType<typeof loadPersonalCohort> |
    ReturnType<typeof loadPersonalProduct> |
    ReturnType<typeof loadTekcyInstructor> |
    ReturnType<typeof loadDashboardMaterials> |
    ReturnType<typeof loadLeaveApplications> |
    ReturnType<typeof setMessage> |
    ReturnType<typeof clearMessage>