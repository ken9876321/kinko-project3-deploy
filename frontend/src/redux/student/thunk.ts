import { ThunkDispatch, RootState } from "../../store";
import { loadProductsInfo, loadCohortsInfo, loadBillingInfo, loadAttendance, loadPaymentMethod, loadPersonalCohort, loadPersonalProduct, loadTekcyInstructor, loadDashboardMaterials, loadLeaveApplications, setMessage } from "./action";
import { checkFetchingData } from "../auth/action";
import { push } from "connected-react-router";
import { StateValues } from "react-use-form-state";
import { restoreLogin } from "../auth/thunk";


export function fetchProductsCohorts() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;

        dispatch(checkFetchingData(true))
        const result = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/products_cohorts`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        const Infos = await result.json();

        if (Infos.error) {
            console.log("data loading error")
            dispatch(checkFetchingData(false))
            return;
        }

        if (Infos.result === undefined) {
            console.log("loading error");
            dispatch(checkFetchingData(false))
            return;
        }
        dispatch(loadProductsInfo(Infos.result.products))
        dispatch(loadCohortsInfo(Infos.result.cohorts))
        dispatch(checkFetchingData(false))
    }
}

export function fetchBillingStatus(formDate: string, toDate: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/billing_status_checking`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    formDate: formDate,
                    toDate: toDate
                }),
        })

        const result = await res.json();
        const billingStatus = result.data
        if (result.error) {
            console.log(result.error);
            return
        }
        dispatch(loadBillingInfo(billingStatus))
    }
}

export function fetchDashboardMaterials(product_id: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;

        dispatch(checkFetchingData(true))
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student//fetching_materials`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    product_id: product_id
                }),
        });
        const result = await res.json();

        if (result.error) {
            console.log(result.error);
            dispatch(push(`/dashboard`))
            dispatch(checkFetchingData(false))
            return
        }

        dispatch(loadDashboardMaterials(result.data))
        dispatch(checkFetchingData(false))
    }
}

export function fetchAllStudentAtt(limit: number, offset: number, setTotalPage: React.Dispatch<React.SetStateAction<number>>) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/all_attendances_checking`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    limit: limit,
                    offset: offset
                }),
        })

        const result = await res.json();
        if (result.error) {
            console.log(result.error);
            return
        }
        dispatch(loadAttendance(result.data.attendances))
        setTotalPage(Math.ceil(result.data.length / limit))

    }
}


export function fetchStudentAtt(formDate: string, toDate: string,
    limit: number, offset: number, setTotalPage: React.Dispatch<React.SetStateAction<number>>
) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;

        dispatch(checkFetchingData(true))
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/attendances_checking`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    formDate: formDate,
                    toDate: toDate,
                    limit: limit,
                    offset: offset
                }),
        })

        const result = await res.json();

        if (result.error) {
            console.log(result.error);
            dispatch(checkFetchingData(false))
            return
        }
        dispatch(loadAttendance(result.data.attendances))
        setTotalPage(Math.ceil(result.data.length / limit))
        dispatch(checkFetchingData(false))

    }
}

export function fetchPaymentMethod() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {

        const token = getState().auth.token;
        dispatch(checkFetchingData(true))
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/payment_method`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        const result = await res.json();
        console.log(result)
        if (result.error) {
            console.log(result.error);
            dispatch(checkFetchingData(false))
            return
        }

        dispatch(loadPaymentMethod(result.data))
        dispatch(checkFetchingData(false))


    }

}

export function fetchPersonalStuffs() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;

        dispatch(checkFetchingData(true))
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/personalStuffs`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        const result = await res.json();
        if (result.error) {
            console.log(result.error);
            dispatch(checkFetchingData(false))
            return;
        }
        dispatch(loadPersonalCohort(result.cohorts));
        dispatch(loadPersonalProduct(result.products));
        dispatch(loadTekcyInstructor(result.instructors))

        dispatch(checkFetchingData(false))

    }
}

export function submitLeave(formData: any, processing: boolean,
    setProcessing: React.Dispatch<React.SetStateAction<boolean>>,
    message: string,
    setMessage: React.Dispatch<React.SetStateAction<string>>
) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        setProcessing(!processing);
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/leave`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    leave: formData,
                }),
        })
        const result = await res.json()
        console.log(result)
        if (result.error) {
            setProcessing(false)
            setMessage("failed to submit the leave application");
            return
        }
        setProcessing(false)
        setMessage("submission completed, you can exit the page")
    }
}

export function getLeaves(limit: number, offset: number, setTotalPage: React.Dispatch<React.SetStateAction<number>>) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/leaves_checking`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    limit: limit,
                    offset: offset
                }),
        })
        const result = await res.json()

        if (result.error) {
            console.log(result.error);
            return
        }

        dispatch(loadLeaveApplications(result.data.applications))
        setTotalPage(Math.ceil(result.data.length / limit))

    }
}


export function fetchCoupon(code: string, setDiscount: React.Dispatch<React.SetStateAction<number>>,
    setCouponMessage: React.Dispatch<React.SetStateAction<string>>
) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/discounts`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    code: code
                }),
        })

        const result = await res.json();
        if (result.error) {
            setCouponMessage(result.error)
            setDiscount(0);
            return;
        }

        const discount = Number(result.data.discount);
        setDiscount(discount)
        setCouponMessage(`You got the discount ${discount} HKD`)

    }
}

export function enrollByOtherMethod(formData: StateValues<any>) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = getState().auth.token;
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/student/enrollment_others`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(
                {
                    formData: formData
                }),
        })
        const result = await res.json()

        if(result.error){
            dispatch(setMessage(result.error));
            return
        }
        dispatch(setMessage(result.msg))

        await dispatch(restoreLogin())
        dispatch(push('/'));
    }
}

export function goToHomePage() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        await dispatch(restoreLogin())
        dispatch(push('/'));
    }
}
