import { StudentActions } from "./action"
import { Materials } from "../../studentPage/Materials"

export interface StudentState {
    attendance: Attendance[];
    productsInfo: ProductInfo[];
    cohortsInfo: CohortInfo[];
    billingInfo: BillingInfo[];
    paymentMethods: PaymentMethod[];
    personalCohorts: PersonalCohort[];
    personalProducts: PersonalProduct[];
    teckyInstructors: TeckyInstructor[];
    materials: Materials[];
    leaveApplications: LeaveApplication[];
    message: string | null;
}

export interface Attendance {
    students_attendance_id: string;
    date_time: string;
    cohort: string;
    description: string;
    type: string;
}

export interface ProductInfo {
    product_id: number;
    product_name: string;
    price: number;
}

export interface CohortInfo {
    cohort_id: number;
    cohort_name: string;
    start_date: string;
}

export interface BillingInfo {
    amount: number;
    billing_date: string;
    transaction_number: string;
    type: string;
}

export interface PaymentMethod {
    brand: string;
    lastFour: string;
    billingEmail: string;
}

export interface PersonalCohort {
    cohort_id: number;
    name: string;
    start: string;
}

export interface PersonalProduct {
    product_id: number;
    total_instalment: number;
    personal_price: string;
    name: string;
}

export interface TeckyInstructor {
    id: number;
    username: string;
}

export interface Materials {
    id: number;
    title: string;
    description: string;
    link: string;
    image_path: string | null;
    product_id: number;
}

export interface LeaveApplication {
    name: string;
    period_from: string;
    period_to: string;
    reason: string;
    approval: boolean | null
}

const initialState = {
    attendance: [],
    productsInfo: [],
    cohortsInfo: [],
    billingInfo: [],
    paymentMethods: [],
    personalCohorts: [],
    personalProducts: [],
    teckyInstructors: [],
    materials: [],
    leaveApplications: [],
    message: null
}

export const StudentReducer = (oldState: StudentState = initialState, action: StudentActions): StudentState => {
    switch (action.type) {
        case '@@Student/LOAD_ATTENDANCE':
            return {
                ...oldState,
                attendance: action.attendance
            }
        case '@@Student/LOAD_PRODUCTS_INFO':
            return {
                ...oldState,
                productsInfo: action.productsInfo
            }
        case '@@Student/LOAD_COHORTS_INFO':
            return {
                ...oldState,
                cohortsInfo: action.cohortsInfo
            }
        case '@@Student/LOAD_BILLING_INFO':
            return {
                ...oldState,
                billingInfo: action.billingInfo
            }
        case '@@Student/LOAD_PAYMENT_METHODS':
            return {
                ...oldState,
                paymentMethods: action.paymentMethods
            }
        case '@@Student/LOAD_PERSONAL_COHORT':
            return {
                ...oldState,
                personalCohorts: action.personalCohorts
            }
        case '@@Student/LOAD_PERSONAL_PRODUCT':
            return {
                ...oldState,
                personalProducts: action.personalProducts
            }
        case '@@Student/LOAD_TECKY_INSTRUCTOR':
            return {
                ...oldState,
                teckyInstructors: action.loadTekcyInstructors
            }
        case '@@Student/LOAD_MATERIALS':
            return {
                ...oldState,
                materials: action.materials
            }
        case '@@Student/LOAD_LEAVE_APPLICATIONS':
            return {
                ...oldState,
                leaveApplications: action.loadLeaveApplications
            }
        case '@@Student/SETMESSAGE':
            return {
                ...oldState,
                message: action.message
            }
        case '@@Student/CLEARMESSAGE':
            return {
                ...oldState,
                message: null
            }

    }
    return oldState
}