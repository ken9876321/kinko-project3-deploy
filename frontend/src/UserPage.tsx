import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ManualCourse from './instructorPage/ManualCourse';
import Attendance from './instructorPage/Attendance';
import Setting from './Setting';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useSelector } from 'react-redux';
import { RootState } from './store';
import './UserPage.scss';
import ApplicationForm from './ApplicationForm';
import { StudentDashboard } from './studentPage/StudentDashboard';
import { StudentPayment } from './studentPage/StudentPayment';
import { LeaveApplicationForm } from './studentPage/LeaveApplicationForm';
import { LeaveApplicationRecord } from './studentPage/LeaveApplicationRecord';
import StudentInfo from './instructorPage/StudentInfo';
import InsertPaymentForm from './instructorPage/InsertPaymentForm';
import studentWelcome from './studentWelcome.svg';
import instructorWelcome from './instructorWelcome.svg'


function UserPage() {
    const nickname = useSelector((oldstate: RootState) => oldstate.auth.user?.nickname ? oldstate.auth.user.nickname : oldstate.auth.user?.username)
    const isInstructor = useSelector((oldstate: RootState) => oldstate.auth.user?.isInstructor)
    const cohortsLength = useSelector((oldstate: RootState) => oldstate.auth.user?.cohortsLength)

    function differentiatePath() {
        if (isInstructor) {
            return (
                <Switch>
                    <Route path="/welcome">
                        <div className='welcome'>Welcome! {nickname}</div><img id="instructorWelcomeImage" src={instructorWelcome} alt="instructorWelcomeImage"/></Route>
                    <Route path="/dashboard" exact><StudentDashboard /></Route>
                    <Route path="/dashboard/:path" exact><StudentDashboard /></Route>
                    <Route path="/attendance" exact><Attendance /></Route>
                    <Route path="/attendance/:cohort/" exact><Attendance /></Route>
                    <Route path="/attendance/:cohort/:path" exact><Attendance /></Route>
                    <Route path="/attendance/:cohort/:path/:lesson"><Attendance /></Route>
                    <Route path="/course_management/:path"><ManualCourse /></Route>
                    <Route path="/student_info" exact><StudentInfo /></Route>
                    <Route path="/student_info/:studentId" exact><StudentInfo /></Route>
                    <Route path="/payment_insert" exact><InsertPaymentForm /></Route>
                    <Route path="/payment_insert/:studentId" exact><InsertPaymentForm /></Route>
                    <Route path="/setting"><Setting /></Route>
                    <Route >404 not Found</Route>
                </Switch>
            )
        } else if (cohortsLength !== undefined && cohortsLength > 0) {
            return (
                <Switch>
                    <Route path="/welcome" exact><div className='welcome'>Welcome! {nickname}</div><img id="studentWelcomeImage" src={studentWelcome} alt="studentWelcomeImage"/></Route>
                    <Route path="/dashboard" exact><StudentDashboard /></Route>
                    <Route path="/dashboard/:path" exact><StudentDashboard /></Route>
                    <Route path="/studentAttendance" exact><StudentDashboard /></Route>
                    <Route path="/payment" exact><StudentPayment /></Route>
                    <Route path="/payment/:path" exact><StudentPayment /></Route>
                    <Route path="/leave_application" exact><LeaveApplicationForm /></Route>
                    <Route path="/leave_application_record" exact><LeaveApplicationRecord /></Route>
                    <Route path="/setting" exact><Setting /></Route>
                    <Route path="/course_application" exact><ApplicationForm /></Route>
                    <Route >404 not Found</Route>
                </Switch>
            )
        }
        else if (cohortsLength !== undefined && cohortsLength <= 0) {
            return (
                <Switch>
                    <Route path="/course_application" exact><ApplicationForm /></Route>
                    <Route><Redirect to="/course_application" /></Route>
                </Switch>
            )
        }
    }


    return (
        <>
            {differentiatePath()}
        </>
    );
}

export default UserPage;