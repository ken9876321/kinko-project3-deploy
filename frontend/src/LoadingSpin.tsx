import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import './LoadingSpin.scss'
import { RootState } from './store';
import { useSelector } from 'react-redux';

export function LoadingSpin() {
    const [loading, setLoading] = useState(true);
    const fetchingLogin = useSelector((state: RootState) => state.auth.fetchingLogin)


    useEffect(() => {
        if (!fetchingLogin) {
            setLoading(false);
        }
    }, [fetchingLogin])

    return (
        <div className="superSpin">
            {(loading ?
                <Spinner className="insideSpinner" animation="border" role="status"><span className="sr-only">Loading...</span></Spinner>
                : <Redirect to="/login" />)}
        </div>

    )
}