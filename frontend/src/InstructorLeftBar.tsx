import React from "react"
import './leftBar.css'
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faKeyboard, faCog, faAngleDoubleLeft, faTasks, faHome, faUserFriends, faClipboard } from '@fortawesome/free-solid-svg-icons'
import { Button } from "react-bootstrap";

interface IProps {
  open: boolean;
  setOpen: (open: boolean) => void;
}

const InstructorLeftBar: React.FC<IProps> = (props) => {
  return (
    <div id="LeftBar" >
      <nav className="col-md-2 col-sm-4 col-4 bg-light sidebar" data-region={props.open}>
        <Button id="goBackLeft" onClick={() => {
          props.setOpen(!props.open)

        }}> <FontAwesomeIcon icon={faAngleDoubleLeft} /></Button>
        <h2>Student Portal</h2>
        <div className="sidebar-sticky">
          <ul className="nav flex-column">

            <li className="nav-item">
              <NavLink className="nav-link" to="/dashboard">
                <FontAwesomeIcon icon={faHome} />Dashboard
                </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/attendance">
                <FontAwesomeIcon icon={faTasks} />
                Attendance
                </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/course_management/existing_offer">
                <FontAwesomeIcon icon={faKeyboard} />
                Course Management
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/student_info">
                <FontAwesomeIcon icon={faUserFriends} />
                Student Info
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/payment_insert">
                <FontAwesomeIcon icon={faClipboard} />
                Payment Insert
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/setting">
                <FontAwesomeIcon icon={faCog} />
                Setting
              </NavLink>
            </li>
          </ul>

          <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Saved reports</span>
            <a className="d-flex align-items-center text-muted" href="#fgf" aria-label="Add a new report">
              <span data-feather="plus-circle"></span>
            </a>
          </h6>
        </div>
      </nav>
      <div className="leftBarOverLab" data-overlap={props.open} onClick={() => { props.setOpen(!props.open) }}></div>
    </div>
  )
}

export default InstructorLeftBar;