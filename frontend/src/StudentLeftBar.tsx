import React, { useState } from "react"
import './leftBar.css'
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faCreditCard, faGraduationCap, faCog, faAngleDoubleLeft, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { Button, Collapse } from "react-bootstrap";

interface IProps {
  open: boolean;
  setOpen: (open: boolean) => void;
}

const StudentLeftBar: React.FC<IProps> = (props) => {
  const [dashboardOpen, setDashboardOpen] = useState(false);
  const [paymentOpen, setPaymentOpen] = useState(false);

  return (
    <div id="LeftBar" >
      <nav className="col-md-2 col-sm-4 col-6 bg-light sidebar" data-region={props.open}>
        <Button id="goBackLeft" onClick={() => {
          props.setOpen(!props.open)
          setDashboardOpen(false)
          setPaymentOpen(false)
        }}> <FontAwesomeIcon icon={faAngleDoubleLeft} /></Button>
        <h2>Student Portal</h2>
        <div className="sidebar-sticky">
          <ul className="nav flex-column">

            <li className="nav-item">
              <div className="nav-collapse" onClick={() => setDashboardOpen(!dashboardOpen)}
                aria-controls="dashboardCollapse"
                aria-expanded={dashboardOpen}><FontAwesomeIcon icon={faHome} />Dashboard</div>

              <Collapse in={dashboardOpen}>
                <div id="dashboardCollapse" className="sub-nav-items">
                  <div className="sub-nav-item">
                    <NavLink className="nav-link" to="/dashboard" exact>
                      Course Materials
                    </NavLink>
                  </div>
                  <div className="sub-nav-item">
                    <NavLink className="nav-link" to="/studentAttendance" exact>
                      Attendance
                    </NavLink>
                  </div>
                </div>
              </Collapse>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/course_application">
                <FontAwesomeIcon icon={faCreditCard} />
                Course Enrollment
              </NavLink>
            </li>

            <li className="nav-item">
              <div className="nav-collapse" onClick={() => setPaymentOpen(!paymentOpen)}
                aria-controls="paymentCollapse"
                aria-expanded={paymentOpen}><FontAwesomeIcon icon={faCreditCard} />
                Payment</div>
              <Collapse in={paymentOpen}>
                <div id="paymentCollapse" className="sub-nav-items">
                  <div className="sub-nav-item">
                    <NavLink className="nav-link" to="/payment" exact>
                      Payment Method
                    </NavLink>
                  </div>
                  <div className="sub-nav-item">
                    <NavLink className="nav-link" to="/payment/billing_status" exact>
                      Billing Status
                    </NavLink>
                  </div>
                </div>
              </Collapse>
            </li>


            <li className="nav-item">
              <NavLink className="nav-link" to="/leave_application">
                <FontAwesomeIcon icon={faGraduationCap} />
                Leave Apply
                </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/leave_application_record">
                <FontAwesomeIcon icon={faSignOutAlt} />
                Leave Application Record
                </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/setting">
                <FontAwesomeIcon icon={faCog} />
                Setting
              </NavLink>
            </li>


          </ul>

          <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>presented by</span>
            <a className="d-flex align-items-center text-muted" href="#fgf" aria-label="Add a new report">
              <span data-feather="plus-circle"></span>
            </a>
          </h6>


        </div>
      </nav>
      <div className="leftBarOverLab" data-overlap={props.open} onClick={() => {
        props.setOpen(!props.open)
        setDashboardOpen(false)
        setPaymentOpen(false)
      }}></div>
    </div>
  )
}

export default StudentLeftBar;